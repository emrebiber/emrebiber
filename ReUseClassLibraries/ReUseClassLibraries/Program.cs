﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCG.ConsoleUtilities;

namespace ReUseClassLibraries
{
    class Program
    {
        static void Main(string[] args)
        {

            /* I created a userInput class library which called SGC.ConsoleUtilities.dll
             * and now i will reuse that library in this project.
             */

            UserInput ui = new UserInput();

            int age = ui.GetIntFormUser("Enter your age: ");
            int num = ui.GetIntFormUser("Enter your favorite number: ");

            Console.WriteLine($"Your age is {age} and your favorite number is {num}.");


        }
    }
}
