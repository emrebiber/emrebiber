﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contacts.Data;
using Contacts.Models;

namespace ContactsV1.Workflows
{
    public class AddContact
    {
        public void Execute()
        {
            ContactRepository repo = new ContactRepository();
            Contact contact = new Contact();

            contact.Name = UserPrompts.PromptUserForString("Please enter a name: ");
            contact.PhoneNumber = UserPrompts.PromptUserForString("Please enter a phone number: ");

            repo.AddContact(contact);

            Console.WriteLine("You have succesfully added a contact");
            UserPrompts.PromptKeyToContinue();
        }
    }
}
