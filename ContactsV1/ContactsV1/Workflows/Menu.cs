﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsV1.Workflows
{
    public class Menu
    {
        public void Execute()
        {
            string userInput;
            do
            {
                ApplicationScreens.ShowMenu();
                userInput = UserPrompts.PromptUserForString("Enter a value: ");

                ProcessUserChoice(userInput);

            } while (userInput.ToUpper() != "Q");

            Console.ReadLine();
        }

        static void ProcessUserChoice(string userInput)
        {
            switch (userInput)
            {
                case "1":
                    ListContacts listWorkflow = new ListContacts();
                    listWorkflow.Execute();
                    break;
                case "2":
                    AddContact addWorkflow = new AddContact();
                    addWorkflow.Execute();
                    break;
            }
        }
    }
}
