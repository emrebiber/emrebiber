﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contacts.Data;
using Contacts.Models;

namespace ContactsV1.Workflows
{
    public class ListContacts
    {
        public void Execute()
        {
            ContactRepository repo = new ContactRepository();
            List<Contact> contacts = repo.LoadAll();
            ApplicationScreens.ContactPrinter(contacts);
        }
    }
}
