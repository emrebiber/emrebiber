﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contacts.Models;

namespace ContactsV1
{
    static class ApplicationScreens
    {
        public static void ShowMenu()
        {
            Console.Clear();
            Console.WriteLine("****  Contacs V1  *******");
            Console.WriteLine("\n1. List Contacts");
            Console.WriteLine("2. Add New Contact");
            Console.WriteLine("3. Edit Contact");
            Console.WriteLine("4. Delete Contact");
            Console.WriteLine("5. Search");
            Console.WriteLine("\nQ to quit\n\n");
        }

        public static void ContactPrinter(List<Contact> contacts)
        {
            Console.Clear();
            Console.WriteLine("Contacts List");
            Console.WriteLine("-----------------------------------");
            if (contacts.Count == 0)
            {
                Console.WriteLine("You have no friends... so sad!");
            }
            else
            {
                foreach (var contact in contacts)
                {
                    Console.WriteLine("{0} {1}", contact.Name, contact.PhoneNumber);
                }
            }

            UserPrompts.PromptKeyToContinue();
        }
    }
}
