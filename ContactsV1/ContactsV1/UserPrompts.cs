﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactsV1
{
    static class UserPrompts
    {
        public static string PromptUserForString(string prompt)
        {
            string userValue;
            do
            {
                Console.WriteLine(prompt);
                userValue = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(userValue))
                {
                    return userValue;
                }

                Console.WriteLine("You must enter a string!\n");
            } while (true);
           
        }

        public static void PromptKeyToContinue()
        {
            Console.WriteLine("\n\nPress any key to contiue...");
            Console.ReadKey();
        }
    }
}
