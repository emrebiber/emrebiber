﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contacts.Models;

namespace Contacts.Data
{
    public class ContactRepository
    {
        private static List<Contact> _contacts = new List<Contact>();

        public ContactRepository()
        {
            if (_contacts.Count < 1)
            {
                _contacts.Add(new Contact() {Name = "Dave", PhoneNumber = "222-3333"});
                _contacts.Add(new Contact() {Name="Jenny", PhoneNumber = "867-5309"});
            }
        }

        public List<Contact> LoadAll()
        {
            return _contacts;
        }

        public void AddContact(Contact contact)
        {
            _contacts.Add(contact);
        }
    }
}
