﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using NUnit.Framework;

namespace DapperSample
{
    [TestFixture]
    public class Queries
    {
        [Test]
        public void RunsSelectQuery()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var employees = cn.Query<Employee>("SELECT * FROM Employees").ToList();

                Assert.AreEqual(10, employees.Count);
            }
        }

        [Test]
        public void RunQueryWithInlineParameters()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var employees = cn.Query<Employee>("Select * from employees where employeeid > @EmployeeID",
                    new {EmployeeID = 5}).ToList();

                Assert.AreEqual(5,employees.Count);
            }
        }

        [Test]
        public void RunQueryWithDynamicParameters()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("EmployeeID", 5);
                var employees = cn.Query<Employee>("Select * from employees where employeeid > @EmployeeID",
                    p).ToList();

                Assert.AreEqual(5, employees.Count);
            }
        }

        [Test]
        public void GetJustOneRecord()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("EmployeeID", 5);
                var employee = cn.Query<Employee>("Select * from employees where employeeid = @EmployeeID",
                    p).FirstOrDefault();

                Assert.IsNotNull(employee);
            }
        }

        [Test]
        public void NotFoundReturnsNull()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("EmployeeID", 25);
                var employee = cn.Query<Employee>("Select * from employees where employeeid = @EmployeeID",
                    p).FirstOrDefault();

                Assert.IsNull(employee);
            }
        }

        [Test]
        public void CanExecuteStoredProcedure()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var employees = cn.Query<Employee>("EmployeeGetAll", 
                    commandType: CommandType.StoredProcedure).ToList();

                Assert.AreEqual(10, employees.Count);
            }
        }

        [Test]
        public void CanRetrieveOutputParameter()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var p = new DynamicParameters();
                p.Add("RegionDescription", "Another Region");
                p.Add("RegionId", DbType.Int32, direction: ParameterDirection.Output);
                cn.Execute("RegionInsert", p, commandType: CommandType.StoredProcedure);

                int regionId = p.Get<int>("RegionId");

                Assert.AreNotEqual(0, regionId);
            }
        }

        [Test]
        public void CanBindComplexObjects()
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var customers = cn.Query<Customer>("Select * from Customers").ToList();

                foreach (var customer in customers)
                {
                    var p = new DynamicParameters();
                    p.Add("CustomerId", customer.CustomerID);
                    customer.Orders = cn.Query<Order>("Select * from Orders where CustomerId = @CustomerId",
                        p).ToList();
                }

                Assert.AreEqual(91, customers.Count);
                Assert.AreEqual(6, customers[0].Orders.Count);
            }
        }
    }
}
