﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockQuoteDIExample.Data;
using StockQuoteDIExample.Models;

namespace StockQuoteDIExample.BLL
{
    public class StockQuoteManager
    {
        private IStockRepository repo;

        public StockQuoteManager(string provider)
        {
            repo = StockRepositoryFactory.CreateStockRepository(provider);
        }

        public List<StockQuote> RetrieveQuotes(string symbols)
        {
            
            return repo.GetStockQuoteInformation(symbols);
        }
    }
}
