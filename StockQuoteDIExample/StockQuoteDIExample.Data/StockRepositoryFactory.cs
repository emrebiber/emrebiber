﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockQuoteDIExample.Data
{
    public class StockRepositoryFactory
    {
        public static IStockRepository CreateStockRepository(string provider)
        {
            switch (provider.ToUpper())
            {
                case "G":
                    return new GoogleStockRepository();
                case "Y":
                    return new YahooStockRepository();
                default:
                    return new TestStockRepository();
            }
        }
    }
}
