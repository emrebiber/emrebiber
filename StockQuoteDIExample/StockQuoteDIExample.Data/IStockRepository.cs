﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockQuoteDIExample.Models;

namespace StockQuoteDIExample.Data
{
    public interface IStockRepository
    {
        List<StockQuote> GetStockQuoteInformation(string symbols);
    }
}
