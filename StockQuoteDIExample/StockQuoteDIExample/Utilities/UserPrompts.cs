﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockQuoteDIExample.Utilities
{
    public static class UserPrompts
    {

        public static string PromptForSymbols()
        {
            Console.WriteLine("Enter stock symbols seperated by a space:");
            return Console.ReadLine();
        }

        public static string PromptForProvider()
        {
            Console.WriteLine("What provider would you like your data from?");
            Console.WriteLine("(G)oogle, (Y)ahoo, (T)est");
            return Console.ReadLine();
        }
    }
}
