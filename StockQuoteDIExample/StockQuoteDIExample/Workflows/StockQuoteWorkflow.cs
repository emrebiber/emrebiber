﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockQuoteDIExample.BLL;
using StockQuoteDIExample.Models;
using StockQuoteDIExample.Screens;
using StockQuoteDIExample.Utilities;

namespace StockQuoteDIExample.Workflows
{
    public class StockQuoteWorkflow
    {
        public void Execute()
        {
            string symbols = UserPrompts.PromptForSymbols();
            string provider = UserPrompts.PromptForProvider();
            StockQuoteManager manager = new StockQuoteManager(provider);
            List<StockQuote> quotes = manager.RetrieveQuotes(symbols);

            DisplayScreens.DisplayQuotes(quotes);

            Console.ReadLine();
        }
    }
}
