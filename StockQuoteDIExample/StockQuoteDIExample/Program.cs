﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockQuoteDIExample.Workflows;

namespace StockQuoteDIExample
{
    class Program
    {
        static void Main(string[] args)
        {
            StockQuoteWorkflow workflow = new StockQuoteWorkflow();
            workflow.Execute();
        }
    }
}
