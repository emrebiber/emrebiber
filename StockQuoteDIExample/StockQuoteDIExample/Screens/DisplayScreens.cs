﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StockQuoteDIExample.Models;

namespace StockQuoteDIExample.Screens
{
    public static class DisplayScreens
    {
        public static void DisplayQuotes(List<StockQuote> quotes)
        {
            Console.Clear();
            Console.WriteLine("Stock Data");
            Console.WriteLine("-------------------------------");
            foreach (var stockQuote in quotes)
            {
                Console.WriteLine($"{stockQuote.Symbol,-10} | {stockQuote.CompanyName,-40} | " +
                                  $"{stockQuote.StockPrice:c}");
            }
        }
    }
}
