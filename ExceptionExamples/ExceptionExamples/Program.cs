﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionExamples
{
    class Program
    {
        static void Main(string[] args)
        {
           // CauseException();
           // HandleException();
           // HandleSpecificException();
           // DisplayException();
           // CallStackExample();
           // ThrowExceptionExample();
            ThrowSpecificException();


            Console.ReadLine();
        }

        static void ThrowSpecificException()
        {
            try
            {
                throw new MySpecificException("Dave did something STUPID!!!");
            }
            catch (MySpecificException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.HelpLink);
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.StackTrace);
            }
        }

        static void ThrowExceptionExample()
        {
            try
            {
                Console.WriteLine("Starting sample");
                throw new NullReferenceException();
                Console.WriteLine("After the throw");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception thrown was {0}", ex.Message);
            }
            catch
            {
                Console.WriteLine("Empty catch block");
            }
            finally
            {
                Console.WriteLine("Finally block");
            }
            Console.WriteLine("After finally");
        }

        static void CallStackExample()
        {
            try
            {
                Method1();
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Catch clasue in CallStackExample Method");
            }
            finally
            {
                Console.WriteLine("Finally clause in CallStackExample Method");
            }
        }

        static void Method1()
        {
            try
            {
                Method2();
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Catch clasue in Method 1");
            }
            finally
            {
                Console.WriteLine("Fianlly clause in Method 1");
            }
        }

        static void Method2()
        {
            int x = 10;
            int y = 0;

            try
            {
                Console.WriteLine(x/y);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Catch clause in method 2");
            }
            finally
            {
                Console.WriteLine("Finally clause in method 2");
            }
            Console.WriteLine("This is after the finally in Ca");
        }

        static void DisplayException()
        {
            try
            {
                Divide(5, 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine("The following error occured");
                Console.WriteLine(ex);
                Console.WriteLine("Source = {0}", ex.Source);
                Console.WriteLine("Help info: {0}", ex.HelpLink);
                Console.WriteLine("Stack Trace: \n{0}", ex.StackTrace);
            }
        }

        static void Divide(int a, int b)
        {
            Console.WriteLine(a/b);
        }

        static void HandleSpecificException()
        {
            try
            {
                Console.WriteLine("Before the exception");
                int[] ints = new int[2];
                ints[50] = 10;
                ints[0] = 5;

                Console.WriteLine("After the exception");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("You tried to divide by zero");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("You tried to access an element of your array that doesn't exists");
            }
            Console.WriteLine("I kept running");
        }

        static void HandleException()
        {
            try
            {
                int x = 5;
                int y = 0;

                Console.WriteLine(x/y);
            }
            catch
            {
                Console.WriteLine("You did something wrong...");
            }
            Console.WriteLine("I kept running...");
        }

        static void CauseException()
        {
            
        }
    }
}
