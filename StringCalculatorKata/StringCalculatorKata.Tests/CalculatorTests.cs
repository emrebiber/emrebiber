﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace StringCalculatorKata.Tests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator _calc;

        [SetUp]
        public void SetupTests()
        {
            _calc = new Calculator();
        }

        [Test]
        public void EmptyStringReturnsZero()
        {
            //Act
            int result = _calc.Add("");
            //Assert
            Assert.AreEqual(0, result);
        }

        [Test]
        public void SingleNumberReturnsTheNumber()
        {
            int result = _calc.Add("5");
            Assert.AreEqual(5,result);
        }

        [TestCase("5,5", 10)]
        [TestCase("6,3", 9)]
        [TestCase("1,3", 4)]
        public void TwoNumbersReturnsTheSumOfThoseNumbers(string input, int expected)
        {
            int result = _calc.Add(input);
            Assert.AreEqual(expected, result);
        }

        [TestCase("", 0)]
        [TestCase("7,7,7", 21)]
        [TestCase("3,6", 9)]
        [TestCase("1,2,3,4,5", 15)]
        public void UnknownNumberOfArgumentsReturnsCorrectSum(string input, int expected)
        {
            int result = _calc.Add(input);
            Assert.AreEqual(expected, result);
        }
    }
}
