﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorKata
{
    public class Calculator
    {
        public int Add(string input)
        {
            // empty string return 0
            if (string.IsNullOrEmpty(input))
            {
                return 0;
            }

            // single number return the number
            string[] numbers = input.Split(',');

            //if (numbers.Length == 1)
            //{
            //    return int.Parse(numbers[0]);
            //} else if (numbers.Length == 2)
            //{
            //    int a = int.Parse(numbers[0]);
            //    int b = int.Parse(numbers[1]);

            //    return a + b;
            //}

            int sum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                // sum = sum + ...
                sum += int.Parse(numbers[i]);
            }

            return sum;
        }
    }
}
