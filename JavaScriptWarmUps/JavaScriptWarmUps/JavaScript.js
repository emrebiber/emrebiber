﻿//Exercise 1
/*
Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 

SayHi("Bob") -> "Hello Bob!"
SayHi("Alice") -> "Hello Alice!"
SayHi("X") -> "Hello X!

*/
function sayHi(name) {
  return alert("Hi " + name);
}


//Exercise 2
/*
Given two strings, a and b, return the result of putting them together in the order abba, e.g. "Hi" and "Bye" returns "HiByeByeHi". 

Abba("Hi", "Bye") -> "HiByeByeHi"
Abba("Yo", "Alice") -> "YoAliceAliceYo"
Abba("What", "Up") -> "WhatUpUpWhat"

public string Abba(string a, string b) {

}
*/
function Abba(a,b) {
    return alert(a+b+b+a);
}

//Exercise 3
/*
The web is built with HTML strings like "<i>Yay</i>" which draws Yay as italic text. In this example, the "i" tag makes <i> and </i> which surround the word "Yay". Given tag and word strings, create the HTML string with tags around the word, e.g. "<i>Yay</i>". 

MakeTags("i", "Yay") -> "<i>Yay</i>"
MakeTags("i", "Hello") -> "<i>Hello</i>"
MakeTags("cite", "Yay") -> "<cite>Yay</cite>"

public string MakeTags(string tag, string content) {

}
*/
function MakeTags(tag,content) {
    return alert("<"+tag+">"+content+"<"+tag+">");
}

// Exercise 4
/*
Given an "out" string length 4, such as "<<>>", and a word, return a new string where the word is in the middle of the out string, e.g. "<<word>>".

Hint: Substrings are your friend here 

InsertWord("<<>>", "Yay") -> "<<Yay>>"
InsertWord("<<>>", "WooHoo") -> "<<WooHoo>>"
InsertWord("[[]]", "word") -> "[[word]]"

public string InsertWord(string container, string word) {

}
*/
function InsertWord(container, word) {
 
    var output = container.substr(0, 2) + word + container.substr(2);
    return  alert(output);
}

//Exercise 5
/*
Given a string, return a new string made of 3 copies of the last 2 chars of the original string. The string length will be at least 2. 

MultipleEndings("Hello") -> "lololo"
MultipleEndings("ab") -> "ababab"
MultipleEndings("Hi") -> "HiHiHi"

public string MultipleEndings(string str) {

}
*/
function MultipleEndings(str) {
    var word = str.substr(str.length - 2);
    return alert(word + word + word);

}

//Exercise 6
/*
Given a string of even length, return the first half. So the string "WooHoo" yields "Woo". 

FirstHalf("WooHoo") -> "Woo"
FirstHalf("HelloThere") -> "Hello"
FirstHalf("abcdef") -> "abc"

public string FirstHalf(string str) {

}
*/
function FirstHalf(str) {
    var half = str.length / 2;
    return alert(str.substr(0, half));
}

//Exercise 7
/*
Given a string, return a version without the first and last char, so "Hello" yields "ell". The string length will be at least 2. 

TrimOne("Hello") -> "ell"
TrimOne("java") -> "av"
TrimOne("coding") -> "odin"

public string TrimOne(string str) {

}
*/
function TrimOne(str) {
    var trimFirstAndLast = str.substr(1, str.length - 2);
    return alert(trimFirstAndLast);
}

/*
     Given 2 strings, a and b, return a string of the form short+long+short, with the shorter string on the outside and the longer string on the inside. The strings will not be the same length, but they may be empty (length 0). 

     LongInMiddle("Hello", "hi") -> "hiHellohi"
     LongInMiddle("hi", "Hello") -> "hiHellohi"
     LongInMiddle("aaa", "b") -> "baaab"

     public string LongInMiddle(string a, string b) {

     }
*/
function LongInMiddle(a, b) {
   
    if (a.length > b.length) {
        return alert(b + a + b);
    } else {
        return alert(a + b + a);
    }
}

//Exercise 9
/*
Given a string, return a "rotated left 2" version where the first 2 chars are moved to the end. The string length will be at least 2. 

Rotateleft2("Hello") -> "lloHe"
Rotateleft2("java") -> "vaja"
Rotateleft2("Hi") -> "Hi"

public string Rotateleft2(string str) {

}
*/
function RotateLeft2(str) {
    if (str.length < 3) {
        return alert(str);
    }
    return alert(str.substr(2) + str.substr(0, 2));
}

//Exercise 10
/*
Given a string, return a "rotated right 2" version where the last 2 chars are moved to the start. The string length will be at least 2. 

RotateRight2("Hello") -> "loHel"
RotateRight2("java") -> "vaja"
RotateRight2("Hi") -> "Hi"Given a string, return a string length 1 from its front, unless front is false, in which case return a string length 1 from its back. The string will be non-empty. 
*/
function RotateRight2(str) {
    if (str.length < 3) {
        return alert(str);
    }

    return alert(str.substr(str.length - 2) + str.substr(0, str.length - 2));
}

// Exercise 11
/*
   Given a string, return a string length 1 from its front, unless front is false, 
   in which case return a string length 1 from its back. The string will be non-empty. 

   TakeOne("Hello", true) -> "H"
   TakeOne("Hello", false) -> "o"
   TakeOne("oh", true) -> "o"
*/
function TakeOne(str, fromFront) {
    if (fromFront == true) {
        return alert(str.substr(0, 1));
    } else {
        return alert(str.substr(str.length - 1));
    }
}

// Exercise 12
/*
 Given a string of even length, return a string made of the middle two chars, so the string "string" yields "ri". 
 The string length will be at least 2. 

 MiddleTwo("string") -> "ri"
 MiddleTwo("code") -> "od"
 MiddleTwo("Practice") -> "ct"

 public string MiddleTwo(string str ) {

 }
 */
function MiddleTwo(str) {
    var half = str.length / 2;
    return alert(str.substr(half - 1, 2));
}

// Exercise 13
/*
Given a string, return true if it ends in "ly". 

EndsWithLy("oddly") -> true
EndsWithLy("y") -> false
EndsWithLy("oddy") -> false

public bool EndsWithLy(string str) { 

}
*/
function EndsWithLy(str) {
    var end = str.substr(str.length - 2);

    if (end.equals("ly")) {
        return alert(true);
    }
}

//Exercise 14
/*
 * Given a string and an int n, return a string made of the first and last n chars from the string. 
   The string length will be at least n. 

    FrontAndBack("Hello", 2) -> "Helo"
    FrontAndBack("Chocolate", 3) -> "Choate"
    FrontAndBack("Chocolate", 1) -> "Ce"

    public string FrontAndBack(string str, int n) {
*/
function FrontAndBack(str,n) {
    var front = str.substr(0, n);
    var back = str.substr(str.length - n);
    return alert(front + back);
}

// Exercise 15
/*
 * Given a string and an index, return a string length 2 starting at the given index. 
   If the index is too big or too small to define a string length 2, use the first 2 chars. 
   The string length will be at least 2. 

   TakeTwoFromPosition("java", 0) -> "ja"
   TakeTwoFromPosition("java", 2) -> "va"
   TakeTwoFromPosition("java", 3) -> "ja"
   public string TakeTwoFromPosition(string str, int n) {
 * 
 * */
function TakeTwoFromPosition(str, n) {
    if (n <= str.length - 2 && n>= 0) {
        return alert(str.substr(n, 2));
    }

    return alert(str.substr(0, 2));
}

//Execise 16

/*
 * Given a string, return true if "bad" appears starting at index 0 or 1 in the string, such as with "badxxx" or "xbadxx" but not "xxbadxx". The string may be any length, including 0.

   HasBad("badxx") -> true
   HasBad("xbadxx") -> true
   HasBad("xxbadxx") -> false

   public bool HasBad(string str) {
 * 
 */
function HasBad(str) {
    var bool;
    if (str.length >= 3 && str.substr(0, 3).equals("bad")) {
        bool = true;
        return alert(bool.toString);
    }
    if (str.length >= 4 && str.substr(1, 4).equals("bad")) {
        bool = false;
        return alert(bool);
    }

    return alert(false);
}

// Exercise 17
/*
 * Given a string, return a string length 2 made of its first 2 chars. 
   If the string length is less than 2, use '@' for the missing chars. 

   AtFirst("hello") -> "he"
   AtFirst("hi") -> "hi"
   AtFirst("h") -> "h@"

   public string AtFirst(string str) {
 * 
 */
function AtFirst(str) {
    if (str.length >= 2) {
        return alert(str.substr(0, 2));
    }
    if (str.length === 1) {
        return alert(str.substr(0, 1) + "@");
    }
    if (str.length === 0) {
        return alert("@@");
    }
    return 1;
}

//Exercise 18
/*
 * Given 2 strings, a and b, return a new string made of the first char of a and the last char of b, 
   so "yo" and "java" yields "ya". If either string is length 0, use '@' for its missing char. 

   LastChars("last", "chars") -> "ls"
   LastChars("yo", "mama") -> "ya"
   LastChars("hi", "") -> "h@"
 * 
 */
function LastChars(a, b) {
    if (a.length === 0 && b.length != 0) {
        return alert("@" + b.substr(b.length - 1));
    }
    if (a.length !=0 && b.length === 0) {
        return alert(a.substr(0, 1) + "@");
    }
    return alert(a.substr(0, 1) + b.substr(b.length - 1));
}

//Exercise 19
/*
 * Given two strings, append them together (known as "concatenation") and return the result. 
    However, if the concatenation creates a double-char, then omit one of the chars, 
    so "abc" and "cat" yields "abcat". 

    ConCat("abc", "cat") -> "abcat"
    ConCat("dog", "cat") -> "dogcat"
    ConCat("abc", "") -> "abc"

    public string ConCat(string a, string b)
 * 
 */
function Concat(a, b) {
   // return alert(a + b);
    return alert(a.concat(b));
}

//Exercise 20
/*
* Given a string of any length, return a new string where the last 2 chars, 
  if present, are swapped, so "coding" yields "codign". 

   SwapLast("coding") -> "codign"
   SwapLast("cat") -> "cta"
   SwapLast("ab") -> "ba"

   public string SwapLast(string str)
* 
*/
function SwapLast(str) {
    var beforeLast = str.substr(str.length - 2,1);
    var last = str.substr(str.length - 1,1);
    var rest = str.substr(0, str.length - 2);

    return alert(rest + last + beforeLast);
}


/******************* Arrays *******************/
// Exercise 1
/*
 *  Given an array of ints, return true if 6 appears as either the first or last element in the array.
    The array will be length 1 or more. 

    FirstLast6({1, 2, 6}) -> true
    FirstLast6({6, 1, 2, 3}) -> true
    FirstLast6({13, 6, 1, 2, 3}) -> false

    public bool FirstLast6(int[] numbers)
 * 
 */
function FirstLast6(numbers)
{
    if (numbers[0] === 6 || numbers[numbers.length - 1] === 6) {
        return alert(true);

    }
    return alert(false);
}

//Exercise 2
/*
 *  Given an array of ints, return true if the array is length 1 or more, 
    and the first element and the last element are equal. 

    SameFirstLast({1, 2, 3}) -> false
    SameFirstLast({1, 2, 3, 1}) -> true
    SameFirstLast({1, 2, 1}) -> true

    public bool SameFirstLast(int[] numbers)
 * 
 */
function SameFirstLast(numbers) {
    if (numbers.length >= 1 && numbers[0] === numbers[numbers.length - 1]) {
        return alert(true);
    }
    return alert(false);
}

//Exercise 4
/*
 *  Given 2 arrays of ints, a and b, 
    return true if they have the same first element or they 
    have the same last element. Both arrays will be length 1 or more. 

    CommonEnd({1, 2, 3}, {7, 3}) -> true
    CommonEnd({1, 2, 3}, {7, 3, 2}) -> false
    CommonEnd({1, 2, 3}, {1, 3}) -> true

    public bool commonEnd(int[] a, int[] b)
 * 
 */
function CommonEnd(a,b) {
    if (a[0] === b[0] || a[a.length - 1] === b[b.length - 1]) {
        return alert(true);
    }
    return alert(false);
}

//Exercise 5
/*
 *  Given an array of ints, return the sum of all the elements. 

    Sum({1, 2, 3}) -> 6
    Sum({5, 11, 2}) -> 18
    Sum({7, 0, 0}) -> 7

    public int Sum(int[] numbers)
 * 
 */
function Sum(numbers) {
    return alert(numbers.reduce(function(a, b) { return a + b; }));
}

// Exercise 6
/*
 *  Given an array of ints, return an array 
    with the elements "rotated left" so {1, 2, 3} yields {2, 3, 1}. 

    RotateLeft({1, 2, 3}) -> {2, 3, 1}
    RotateLeft({5, 11, 9}) -> {11, 9, 5}
    RotateLeft({7, 0, 0}) -> {0, 0, 7}

    public int[] RotateLeft(int[] numbers)
 * 
 */
function RotateLeft(numbers) {
    var number = numbers.splice(0, 1);
    return alert(number + );

}






























