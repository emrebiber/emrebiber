﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIODirectoryApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowWindowsDirectoryInfo();
            //DisplayImageFiles();
            //ModifyAppDirectory();
            ShowDriveInfo();

            Console.ReadLine();
        }

        static void ShowDriveInfo()
        {
            DriveInfo[] myDrives = DriveInfo.GetDrives();

            foreach (var driveInfo in myDrives)
            {
                Console.WriteLine($"Name: {driveInfo.Name}");
                Console.WriteLine($"Type: {driveInfo.DriveType}");
                if (driveInfo.IsReady)
                {
                    Console.WriteLine($"Free Space: {driveInfo.TotalFreeSpace}");
                    Console.WriteLine($"Format: {driveInfo.DriveFormat}");
                    Console.WriteLine($"Label: {driveInfo.VolumeLabel}");
                }
            }
        }

        static void ModifyAppDirectory()
        {
            DirectoryInfo dir = new DirectoryInfo(".");

            dir.CreateSubdirectory("MyFolder");

            DirectoryInfo myDataFolder = dir.CreateSubdirectory(@"MyFolder\Data");

            Console.WriteLine("New Folder is: {0}", myDataFolder);
        }

        static void DisplayImageFiles()
        {
            DirectoryInfo dir =  new DirectoryInfo(@"C\Windows\Web\Wallpaper");
            FileInfo[] imageFiles = dir.GetFiles("*.jpg", SearchOption.AllDirectories);

            Console.WriteLine("Found {0} .jpg files", imageFiles.Length);

            foreach (var f in imageFiles)
            {
                Console.WriteLine("********************************");
                Console.WriteLine($"File name: {f.Name}");
                Console.WriteLine($"File name: {f.Length}");
                Console.WriteLine($"File name: {f.CreationTime}");
                Console.WriteLine($"File name: {f.Attributes}");
                Console.WriteLine("********************************\n");
            }
        }

        static void ShowWindowsDirectoryInfo()
        {
            DirectoryInfo dir = new DirectoryInfo(@"C:\Windows");
            Console.WriteLine("**** Directory Information *****");
            Console.WriteLine("Full Name {0}", dir.FullName);
            Console.WriteLine("Name {0}", dir.Name);
            Console.WriteLine("Parent {0}", dir.Parent);
            Console.WriteLine("Creation {0}", dir.CreationTime);
            Console.WriteLine("Attributes {0}", dir.Attributes);
            Console.WriteLine("Root {0}", dir.Root);

        }
    }
}
