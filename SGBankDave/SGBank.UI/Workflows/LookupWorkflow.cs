﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.UI.Utilities;
using SGBank.Models;

namespace SGBank.UI.Workflows
{
    class LookupWorkflow
    {
        private Account _currentAccount;

        public void Execute()
        {
            int accountNumber = UserPrompts.GetIntFromUser("Please provide an account number");
            DisplayAccountInformation(accountNumber);
        }

        private void DisplayAccountInformation(int accountNumber)
        {
            var manager = new AccountManager();
            var result = manager.GetAccount(accountNumber);
            Console.Clear();
            if (result.Success)
            {
                _currentAccount = result.Data;
                AccountScreens.PrintAccountDetailsView(_currentAccount);
                DisplayLookupMenu();
            }
        }

        private void DisplayLookupMenu()
        {
            do
            {
                Console.WriteLine("\n1. Deposit");
                Console.WriteLine("2. Withdraw");
                Console.WriteLine("3. Transfer");
                Console.WriteLine("\n(Q) to return to main menu");

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");
                if (input.Substring(0, 1).ToUpper() == "Q")
                    break;

                ProcessChoice(input);

            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    DepositWorkflow dep = new DepositWorkflow();
                    dep.Execute(_currentAccount);
                    break;
            }
        }
    }
}
