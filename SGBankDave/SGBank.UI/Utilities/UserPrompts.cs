﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.UI.Utilities
{
    public static class UserPrompts
    {
        public static string GetStringFromUser(string message)
        {
            // Should wrap in a loop to verify that a string was recieved from user.
            //TODO:  Add Validation 
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        public static int GetIntFromUser(string message)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(message);
                string input = Console.ReadLine();
                int value;
                if (int.TryParse(input, out value))
                    return value;

                Console.WriteLine("That was not a valid number.");
                PressKeyForContinue();
            } while (true);
        }

        public static decimal GetDecimalFromUser(string message)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(message);
                string input = Console.ReadLine();
                decimal value;
                if (decimal.TryParse(input, out value))
                    return value;

                Console.WriteLine("That was not a valid decimal value.");
                PressKeyForContinue();
            } while (true);
        }

        public static void PressKeyForContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
