﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;

namespace SGBank.UI.Utilities
{
    public static class AccountScreens
    {
        public static void PrintAccountDetailsView(Account account)
        {
            Console.WriteLine("Account Information");
            Console.WriteLine("=================================");
            Console.WriteLine($"Account Number: {account.AccountNumber}");
            Console.WriteLine($"Name: {account.Name}");
            Console.WriteLine($"Account Balance: {account.Balance:c}");
        }

        public static void DepositDetails(DepositReciept reciept)
        {
            Console.Clear();
            Console.WriteLine("Deposited {0:c} to account {1}", reciept.DepositAmount, reciept.AccountNumber);
            Console.WriteLine("New Balance is {0:c}", reciept.NewBalance);
            UserPrompts.PressKeyForContinue();
        }

        public static void WorkflowErrorScreen(string message)
        {
            Console.Clear();
            Console.WriteLine($"An error occured.  {message}");
            UserPrompts.PressKeyForContinue();
        }
    }
}
