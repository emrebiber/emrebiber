﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace jQueryValidationExample.Models
{
    public class RsvpResponse
    {
        //[Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Please enter your email")]
        //[RegularExpression(@"^\S+@\S+$", ErrorMessage = "The email address is not in correct format")]
        public string Email { get; set; }

       // [Required(ErrorMessage = "Please enter your phone number")]
        public string Phone { get; set; }

       // [Required(ErrorMessage = "Please enter your favorite Game")]
        public string FavoriteGame { get; set; }

       // [Required(ErrorMessage = "Please specify if you will attend")]
        public bool? WillAttend { get; set; }
    }
}
