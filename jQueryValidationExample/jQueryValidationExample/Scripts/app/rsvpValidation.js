﻿$(document)
    .ready(function() {
        $('#rsvpForm')
            .validate({
                rules: {
                    Name: {
                        required: true
                    },
                    Email: {
                        required: true,
                        email: true
                    },
                    Phone: {
                        required: true,
                        phone: true
                    },
                    FavoriteGame: {
                        required: true,
                        minlength: 2
                    },
                    WillAttend: {
                        required: true
                    }
                },
                messages: {
                    Name: "Enter your name",
                    Email: {
                        required: "Type your email!!!",
                        email: "That's not the right way to type your email!!!"
                    },
                    FavoriteGame: {
                        required: "Tell us your favorite game",
                        minlength: $.validator.format("I dont know any game with less than {0} characters...")
                    }
                }

            });
    });