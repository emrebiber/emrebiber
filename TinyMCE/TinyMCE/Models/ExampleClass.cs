﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TinyMCE.Models
{
    public class ExampleClass
    {
        [AllowHtml]
        public string HtmlContent { get; set; }
   
        public string Summary { get; set; }

    }
}