﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphismDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape[] myShapes = new Shape[5];
            myShapes[0] = new Shape();
            myShapes[1] = new Square();
            myShapes[2] = new Triangle();
            myShapes[3] = new Rectangle();
            myShapes[4] = new Circle() {Radius = 10};

            foreach (Shape shape in myShapes)
            {
                Console.Write(shape.Draw());
                if (shape is Circle)
                    Console.Write(" Radius is {0}", ((Circle)shape).Radius);

                Console.Write("\n");
            }

            //Square mySquare = new Square();
            //Console.WriteLine(mySquare.Draw());

            Console.ReadLine();
        }
    }
}
