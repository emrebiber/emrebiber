﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphismDemo
{
    public class Square : Rectangle
    {
        public override string Draw()
        {
            return "I have 4 sides too, but I'm uniform.  Drawing a square!";
        }
    }
}
