﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolymorphismDemo
{
    public class Rectangle : Shape
    {
        public override string Draw()
        {
            return "I have 4 sides (Rec)";
        }
    }
}
