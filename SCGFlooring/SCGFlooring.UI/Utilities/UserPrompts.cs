﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.BLL;

namespace SCGFlooring.UI.Utilities
{
    public class UserPrompts
    {
        public const string _seperatorBar = "-------------------------------------------------------------------------------";

        public static DateTime GetValidDateTimeFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                DateTime date = new DateTime();

                if (DateTime.TryParse(input, out date))
                {
                    return date;
                }
                else
                {
                    Console.WriteLine("Invalid date. Try Again!");
                    PressAnyKeyToContinue();

                }

            } while (true);
        }

        public static int GetIntegerFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                int output;

                if (int.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("You must enter integer value.");
                    PressAnyKeyToContinue();
                }

            } while (true);
        }

        public static decimal GetDecimalFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                decimal output;

                if (decimal.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("You must enter decimal value.");
                    PressAnyKeyToContinue();
                }
            } while (true);

        }

        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.Write("you must enter valid text.");
                    PressAnyKeyToContinue();
                }
                else
                {
                    return input;
                }
            } while (true);

        }

        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + "(Y/N)? ");
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N.");
                    PressAnyKeyToContinue();
                }
                else
                {
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N.");
                        PressAnyKeyToContinue();
                    }

                    return input;
                }
            }

        }

        public static void PressAnyKeyToContinue()
        {
            Console.WriteLine("Press any key to coninue...");
            Console.ReadKey();

        }

        public static string GetStateChoiceFromUser(string prompt)
        {

            do
            {
                string input = GetStringFromUser("Enter State Name: ").ToUpper();
                var manager = new StateManager();
                bool validStateInput = manager.IsInputAValidSate(input);

                if (!validStateInput)
                {
                    Console.WriteLine("This is not valid state.");
                    Console.WriteLine("Available sates are OH, PA, MI, IN, AR"); 
                    PressAnyKeyToContinue();
                }
                else
                    return input;
                    
            } while (true);

        }

        public static string GetProductChoiceFromUser(string prompt)
        {

            do
            {
                string input = GetStringFromUser("Enter Product Type: ");
                var manager = new ProductManager();
                bool validProductInput = manager.IsInputAValidProductType(input);

                if (!validProductInput)
                {
                    Console.WriteLine("This is not valid product type. Try again...");
                    Console.WriteLine("Currently Carpet, Wood, Laminate, Tile, Bamboo are available in stock.");
                    PressAnyKeyToContinue();
                }
                    
                else
                    return input;

            } while (true);

        }


    }
}