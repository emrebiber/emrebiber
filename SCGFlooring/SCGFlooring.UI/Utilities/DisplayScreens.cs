﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Models;

namespace SCGFlooring.UI.Utilities
{
    public class DisplayScreens
    {
        public const string orderLineFormat = "{0, -10} {1, -15} {2, -15} {3, -10} {4, -10}";
        public const string AddOrderLineFormat = "{0, -10} {1, -15} {2, -15} {3, -10} {4, -10} {5, -10}";


        public static void DisplayOrderList(List<Order> orders, DateTime date)
        {
            
            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine("Your order summary for: {0}", date.ToString("d"));
            Console.WriteLine();
            PrintOrderListHeader();

            foreach (var print in orders)
            {
               Console.WriteLine(orderLineFormat, print.OrderNumber,print.OrderDate.ToString("d"), print.CustomerName, print.StateName, print.ProductType);
                  
            }
        }

        public static void PrintOrderListHeader()
        {
            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine(orderLineFormat, "Order#", "OrderDate", "CustomerName", "State", "Product");
            Console.WriteLine(UserPrompts._seperatorBar);
        }

        public static void PrintAddOrderListHeader()
        {
            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine(AddOrderLineFormat, "Order#", "OrderDate", "CustomerName", "State", "Product", "Area");
            Console.WriteLine(UserPrompts._seperatorBar);
        }

        

        

       
    }
}
