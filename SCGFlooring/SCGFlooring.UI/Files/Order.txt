﻿OrderNumber,OrderDate,CustomerName,StateName,TaxRate,ProductType,OrderArea,CostPerSquareFoot,LaborCostPerSquareFoot,MaterialCost,LaborCost,OrderTax,OrderTotalCost
2,8/26/2016 12:00:00 AM,Lyle Lovett,Texas,0.0625,Mexican Ceramic Tile,50.00,4.25,3.75, 212.50,187.50,25.00,425.00

3,8/26/2016 12:00:00 AM,Detective Boyle,IN,6.00,Wood,68.25,5.15,4.75, 351.4875,324.1875,40.540500,716.215500

4,08/28/2016,Dave Balzer,Ohio,0.0575,Shag Carpet,22.5,1.85,2.50,41.63,56.25,5.63,103.51

5,08/28/2016,Richard Linklater,Texas,0.0625,Mexican Ceramic Tile,50.00,4.25,3.75,212.50,187.50,25.00,425.00
19,08/28/201
6,Ronnie Van Zandt,Alabama,0.04,Wood Laminate,12.25,2.25,3.50,27.56,42.88,2.82,73.26