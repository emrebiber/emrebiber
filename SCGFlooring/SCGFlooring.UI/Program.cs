﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.UI.Utilities;

namespace SCGFlooring.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SCG Flooring Program";

            MainMenu.Execute();
        }
    }
}
