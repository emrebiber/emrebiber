﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.BLL;
using SCGFlooring.Models;
using SCGFlooring.UI.Utilities;

namespace SCGFlooring.UI.Workflow
{
    public class AddOrderWorkflow
    {
        public void Execute()
        {
            Order newOrder = new Order();

           
            newOrder.OrderDate = UserPrompts.GetValidDateTimeFromUser("Enter Order Date MM/DD/YYYY: ");
            newOrder.CustomerName = UserPrompts.GetStringFromUser("Enter Customer Name: ");
            newOrder.StateName = UserPrompts.GetStateChoiceFromUser("Enter State: ");
            newOrder.ProductType = UserPrompts.GetProductChoiceFromUser("Enter Product Type: ");
            newOrder.OrderArea = UserPrompts.GetDecimalFromUser("Enter Area: ");

            var manager = new OrderManager();
            var order = manager.CreateNewOrder(newOrder);

            Console.WriteLine();
            DisplayScreens.PrintAddOrderListHeader();
            Console.WriteLine(DisplayScreens.AddOrderLineFormat, newOrder.OrderNumber, newOrder.OrderDate.ToString("d"), newOrder.CustomerName, newOrder.StateName, newOrder.ProductType, newOrder.OrderArea);
            Console.WriteLine();

            if (UserPrompts.GetYesNoAnswerFromUser("Add the following information") == "Y")
            {
                
                var response = manager.AddOrder(newOrder);

                if (response.Success)
                {
                    Console.WriteLine("New order added succesfully...");
                }
                else
                {
                    Console.WriteLine(response.Message);
                }

                Console.WriteLine();
                UserPrompts.PressAnyKeyToContinue();
            }
            else
            {
                Console.WriteLine("Add order cancelled!");
                UserPrompts.PressAnyKeyToContinue();
            }

                       


        }
    }
}
