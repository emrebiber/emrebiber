﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.BLL;
using SCGFlooring.Models;
using SCGFlooring.UI.Utilities;

namespace SCGFlooring.UI.Workflow
{
    public class EditOrderWorkflow
    {
        public void Execute()
        {
            DateTime date = UserPrompts.GetValidDateTimeFromUser("Enter date MM/DD/YYYY for order you want to see: ");
            var manager = new OrderManager();
            var orders = manager.ListAllOrders(date);

            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine("Orders belong to date: {0}", date.ToString("d"));
            Console.WriteLine();
            DisplayScreens.PrintOrderListHeader();

            foreach (var print in orders)
            {
                Console.WriteLine(DisplayScreens.orderLineFormat, print.OrderNumber, print.OrderDate.ToString("d"),
                    print.CustomerName, print.StateName, print.ProductType, print.OrderArea);
            }
            Console.WriteLine();
            UserPrompts.PressAnyKeyToContinue();

            int orderNumber = UserPrompts.GetIntegerFromUser("Enter the order number you want to edit: ");
            var order = orders.FirstOrDefault(o => o.OrderNumber == orderNumber);

            string input = UserPrompts.GetStringFromUser("Are you sure to edit your records Y/N: ");

            //if (input == "Y")
            //{

            //    //order.OrderDate = UserPrompts.GetValidDateTimeFromUser("Enter new order date: ");
            //    order.CustomerName = UserPrompts.GetStringFromUser("Enter your new customer name: ");
            //    order.StateName = UserPrompts.GetStateChoiceFromUser("Enter your new state: ");
            //    order.ProductType = UserPrompts.GetProductChoiceFromUser("Enter new product type: ");

            //    manager.EditOrder(order);
            //}
            //else
            //{
            //    Console.WriteLine("Edit cancelled. Your records remain same.");
            //}

            Console.WriteLine("Want to edit customer name? Press Y to edit or Any key to keep it same");
            string customerNameInput = Console.ReadLine();
            if (customerNameInput.ToUpper() == "Y")
            {

                order.CustomerName = UserPrompts.GetStringFromUser("Enter new name: ");
            }

            Console.Clear();
            Console.WriteLine("Want to edit state ? Press Y to edit or any key to continue");
            string stateInput = Console.ReadLine();
            if (stateInput.ToUpper() == "Y")
            {
                order.StateName = UserPrompts.GetStateChoiceFromUser("Enter new state: ");
            }

            Console.Clear();
            Console.WriteLine("Want to edit product type? Press Y to edit or Any key to keep it same");
            string productInput = Console.ReadLine();
            if (productInput.ToUpper() == "Y")
            {
                order.ProductType = UserPrompts.GetProductChoiceFromUser("Enter new product type: ");
            }

            Console.Clear();
            Console.WriteLine("Want to edit area? Press Y to edit or Any key to keep it same");
            string areaInput = Console.ReadLine();
            if (areaInput.ToUpper() == "Y")
            {
                order.OrderArea = UserPrompts.GetDecimalFromUser("Enter new area: ");
            }

            manager.EditOrder(order);

            Console.WriteLine();
            Console.WriteLine("You've succesfully edited your records.");
            UserPrompts.PressAnyKeyToContinue();


            

        }

    }
}
