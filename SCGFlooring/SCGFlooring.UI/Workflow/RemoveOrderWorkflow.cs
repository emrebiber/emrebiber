﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.BLL;
using SCGFlooring.UI.Utilities;

namespace SCGFlooring.UI.Workflow
{
    public class RemoveOrderWorkflow
    {
        public void Execute()
        {
            DateTime date = UserPrompts.GetValidDateTimeFromUser("Enter date MM/DD/YYYY for order you want to display: ");
            OrderManager manager = new OrderManager();
            var orders = manager.ListAllOrders(date);

            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine("Orders belong to date: {0}", date.ToString("d"));
            Console.WriteLine();
            DisplayScreens.PrintOrderListHeader();

            foreach (var print in orders)
            {
                Console.WriteLine(DisplayScreens.orderLineFormat, print.OrderNumber, print.OrderDate.ToString("d"), print.CustomerName, print.StateName, print.ProductType);
            }

            Console.WriteLine();
            UserPrompts.PressAnyKeyToContinue();

            int orderNumber = UserPrompts.GetIntegerFromUser("Enter the order number you want to delete: ");
            var remove = orders.FirstOrDefault(o => o.OrderNumber == orderNumber);

            string input = UserPrompts.GetStringFromUser("Are you sure delete your records Y/N: ");
            
            if (input == "Y")
            {
                var response = manager.RemoveSpecificOrder(remove);
                Console.WriteLine(response.Message);
            }
            
            Console.WriteLine();
            Console.WriteLine("You have succesfully deleted...");
            UserPrompts.PressAnyKeyToContinue();
        }
    }
}
