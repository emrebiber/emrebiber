﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.BLL;
using SCGFlooring.UI.Utilities;

namespace SCGFlooring.UI.Workflow
{
    public class DisplayOrderWorkflow
    {
        public void Execute()
        {
            DateTime date = UserPrompts.GetValidDateTimeFromUser("Enter date MM/DD/YYYY: ");
            var manager = new OrderManager();
            var result = manager.ListOrders(date);

            if (result.Success)
            {
                DisplayScreens.DisplayOrderList(result.Data, date);
            }
            else
            {
                
                Console.WriteLine(result.Message);
            }

            Console.WriteLine();
            UserPrompts.PressAnyKeyToContinue();

        }
    }
}
