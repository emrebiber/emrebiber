﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.UI.Utilities;
using SCGFlooring.UI.Workflow;

namespace SCGFlooring.UI
{
    public class MainMenu
    {
        public static void Execute()
        {
            bool keepRunning = true;

            while (keepRunning)
            {
                DisplayMenu();
                keepRunning = ProcessOrderChoice();
            }
        }

        public static void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("SCG Flooring Program");
            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine("1. Display Orders");
            Console.WriteLine("2. Add Order");
            Console.WriteLine("3. Edit Order");
            Console.WriteLine("4. Remove Order");
            Console.WriteLine();
            Console.WriteLine("Q - Quit");
            Console.WriteLine();
            Console.WriteLine(UserPrompts._seperatorBar);
            Console.WriteLine();
            Console.WriteLine("Please make a choice: ");
        }

        public static bool ProcessOrderChoice()
        {
            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    DisplayOrderWorkflow displayOrder = new DisplayOrderWorkflow();
                    displayOrder.Execute();
                    break;
                case "2":
                    AddOrderWorkflow addOrder = new AddOrderWorkflow();
                    addOrder.Execute();
                    break;
                case "3":
                    EditOrderWorkflow editOrder = new EditOrderWorkflow();
                    editOrder.Execute();
                    break;
                case "4":
                    RemoveOrderWorkflow removeOrder = new RemoveOrderWorkflow();
                    removeOrder.Execute();
                    break;
                case "Q":
                    return false;
                default:
                    Console.WriteLine("That is not a valid choice.");
                    UserPrompts.PressAnyKeyToContinue();
                    break;
            }

            return true;
        }
    }
}
