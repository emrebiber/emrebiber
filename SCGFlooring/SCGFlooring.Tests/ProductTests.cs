﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SCGFlooring.Data.Repositories.MockRepo;

namespace SCGFlooring.Tests
{
    [TestFixture]
    public class ProductTests
    {
        [Test]
        public void CanListProducts()
        {
            var repo = new MockProductRepository();
            var products = repo.ListProducts();
            Assert.AreEqual(3, products.Count);
        }

        [TestCase("Tile", 4.25, 3.75)]
        public void CanGetSpecificItem(string productType, decimal costper, decimal laborper)
        {
            var repo = new MockProductRepository();
            var product = repo.GetProduct("Tile");
            Assert.AreEqual(product.ProductType,productType);
            Assert.AreEqual(product.CostPerSquareFoot, costper);
            Assert.AreEqual(product.LaborCostPerSquareFoot,laborper);
        }



    }
}
