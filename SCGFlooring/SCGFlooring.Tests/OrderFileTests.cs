﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SCGFlooring.Data.Repositories;

namespace SCGFlooring.Tests
{
    [TestFixture]
    public class OrderFileTests
    {
        [Test]
        public void CreateDirectoryAndListOrderFilesInDirectory()
        {
            var repo = new FileOrderRepository();
            var files = repo.CreateDirectoryAndListOrderFilesInDirectory();
            Assert.AreEqual(12, files.Count);
        }

        [Test]
        public void CanListOrders()
        {
            var repo = new FileOrderRepository();
            var firstOrders = repo.ListOrderByDate(DateTime.Parse("01/01/2016"));
            var secondOrders = repo.ListOrderByDate(DateTime.Parse("03/03/2011"));

            Assert.AreEqual(4, firstOrders.Count);
            Assert.AreEqual(1, secondOrders.Count);
        }

        [Test]
        public void ProperNameForFile()
        {
            var repo = new FileOrderRepository();
            DateTime dt1 = new DateTime(2016,01,01);
            DateTime dt2 = new DateTime(2011, 05, 05);
            DateTime dt3 = new DateTime(1999, 12, 18);
            DateTime dt4 = new DateTime(2222, 11, 29);


            string file1 = repo.ProperNameForFile(dt1);
            string file2 = repo.ProperNameForFile(dt2);
            string file3 = repo.ProperNameForFile(dt3);
            string file4 = repo.ProperNameForFile(dt4);

            Assert.AreEqual(file1, "Orders_01012016.txt");
            Assert.AreEqual(file2, "Orders_05052011.txt");
            Assert.AreEqual(file3, "Orders_12181999.txt");
            Assert.AreEqual(file4, "Orders_11292222.txt");


        }


    }
}
