﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SCGFlooring.BLL;
using SCGFlooring.Data.Repositories;
using SCGFlooring.Models;

namespace SCGFlooring.Tests
{
    [TestFixture]
    public class OrderTests
    {
        [Test]
        public void CanLoadOrder()
        {
            var repo = new MockOrderRepository();
            var order = repo.ListOrders();

            Assert.AreEqual(5, order.Count);
        }

        [TestCase(1, "Dave Balzer", 33.5)]
        public void CanGetSpecificOrder(int orderNumber, string customerName, decimal orderArea)
        {
            var repo = new MockOrderRepository();
            var order = repo.ListOneSpecificOrder(DateTime.Parse("01/05/2016"), orderNumber);
            Assert.AreEqual(orderNumber, order.OrderNumber);
            Assert.AreEqual(customerName, order.CustomerName);
            Assert.AreEqual(orderArea, order.OrderArea);

        }

        [Test]
        public void CanAddOrder()
        {
            MockOrderRepository repo = new MockOrderRepository();
            Order newOrder = new Order();
            

            newOrder.OrderNumber = 1;
            newOrder.CustomerName = "Emre";
            newOrder.StateName = "Arkansas";
            newOrder.MaterialCost = 25.5M;

            repo.AddOrder(newOrder);

        }


        [Test]
        public void CanDeleteOrder()
        {
            var repo = new MockOrderRepository();
            var order = repo.ListOrders();

            order.RemoveAt(2);

            Assert.AreEqual(5, order.Count);

            Order check = order[0];

            Assert.AreEqual("Dave Balzer", check.CustomerName);
            Assert.AreEqual(1,check.OrderNumber);
            Assert.AreEqual("Ohio", check.StateName);

        }

        [Test]
        public void CanEditOrder()
        {
            var repo = new MockOrderRepository();
            var order = repo.ListOrders();

            Order editedOrder = order[3];

            editedOrder.CustomerName = "Irem Orbay2";
            editedOrder.StateName = "New Jersey";

            repo.EditOrder(editedOrder);

            Assert.AreEqual(5, order.Count);
            Assert.AreEqual(editedOrder.CustomerName, "Irem Orbay2");
            Assert.AreEqual(editedOrder.StateName, "New Jersey");

        }

        

       

       
    }
}
