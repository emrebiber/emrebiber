﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using NUnit.Framework;
using SCGFlooring.Data.Repositories.MockRepo;

namespace SCGFlooring.Tests
{
    [TestFixture]
    public class StateTests
    {
        [Test]
        public void CanListState()
        {
            var repo = new MockStateRepository();
            var states = repo.ListStates();
            Assert.AreEqual(5,states.Count);
        }

        [TestCase("AR", "Arkansas", 5.01)]
        public void CanGetSpecificState(string abb, string name, decimal tax)
        {
            var repo = new MockStateRepository();
            var state = repo.GetState("AR");
            Assert.AreEqual(state.StateAbbreviation, abb);
            Assert.AreEqual(state.StateName, name);
            Assert.AreEqual(state.TaxRate, tax);
        }
    }
}
