﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data;
using SCGFlooring.Data.Factories;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Data.Repositories;
using SCGFlooring.Models;

namespace SCGFlooring.BLL
{
    public class OrderManager
    {
        private IOrderRepository _orderRepository;

        public OrderManager()
        {
            _orderRepository = OrderRepositoryFactory.GetOrderRepository();
        }

        public Response<List<Order>> ListOrders(DateTime orderDate)
        {
            var response = new Response<List<Order>>();
            var order = _orderRepository.ListOrderByDate(orderDate);


            try
            {
                if (order.Count == 0)
                {
                    response.Success = false;
                    response.Message = "There is no record related to date...";
                }
                else
                {
                    response.Success = true;
                    response.Data = order;
                }
            }
            catch (Exception ex)
            {

                response.Success = false;
                response.Message = "There was an error. Please try again!";
                ErrorLogs.LogErrors(ex.Message);
            }

            return response;

        }

        public List<Order> ListAllOrders(DateTime date)
        {
            return _orderRepository.ListOrderByDate(date);
        }

        public Order ListOneSpecificOrder(DateTime date, int number)
        {
            var orders = ListAllOrders(date);
            var order = orders.FirstOrDefault(o => o.OrderNumber == number);

            return order;
        }

        public Response<Order> AddOrder(Order orders)
        {
            var response = new Response<Order>();

            try
            {
                _orderRepository.AddOrder(orders);
                response.Success = true;
                response.Data = orders;

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Order did not save succesfully... ";
                ErrorLogs.LogErrors(ex.Message);

            }
            return response;
        }

        public int GenerateNextOrderNumber(DateTime orderDate)
        {
            int orderNumber;
            var orders = _orderRepository.ListOrderByDate(orderDate);

            if (orders.Count == 0)
                orderNumber = 1;
            else
            {
                orderNumber = orders.Max(o => o.OrderNumber) + 1;
            }

            return orderNumber;

        }

        public Order CreateNewOrder(Order order)
        {
            
            order.OrderNumber = GenerateNextOrderNumber(order.OrderDate);
            var orderCost = CalculateOrderCosts(order);

            return orderCost;

        }

        private Order CalculateOrderCosts(Order order)
        {
            ProductManager productManager = new ProductManager();
            StateManager stateManager  = new StateManager();

            var product = productManager.GetProduct(order.ProductType);

            order.CostPerSquareFoot = product.CostPerSquareFoot;
            order.LaborCostPerSquareFoot = product.LaborCostPerSquareFoot;

            var state = stateManager.GetState(order.StateName);

            order.StateName = state.StateAbbreviation;
            order.TaxRate = state.TaxRate;

            order.LaborCost = order.LaborCostPerSquareFoot*order.OrderArea;
            order.MaterialCost = order.CostPerSquareFoot*order.OrderArea;
            order.OrderTax = (order.LaborCost + order.MaterialCost)*order.TaxRate;
            order.OrderTotalCost = order.MaterialCost + order.LaborCost + order.TaxRate;

            return order;


        }

        public Response<Order> RemoveSpecificOrder(Order order)
        {
            var response = new Response<Order>();

            try
            {
                _orderRepository.RemoveOrder(order);
                response.Success = true;
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Order did not removed correctly.";
                ErrorLogs.LogErrors(ex.Message);
            }

            return response;
        }

        public void EditOrder (Order order)
        {

            var repo = new FileOrderRepository();
            repo.EditOrder(order);

        }







    }
}
