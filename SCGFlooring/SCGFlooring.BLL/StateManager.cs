﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Factories;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.BLL
{
    public class StateManager
    {
        private IStateRepository _stateRepository;

        public StateManager()
        {
            _stateRepository = StateRepositoryFactory.GetStateRepository();
        }

        public bool IsInputAValidSate(string stateInput)
        {
            bool result = false;
            var states = _stateRepository.ListStates();

            foreach (var state in states)
            {
                if (stateInput == state.StateAbbreviation)
                {
                    result = true;

                }
                else if (stateInput == state.StateName)
                {
                    result = true;

                }

            }

            return result;
        }

        public State GetState(string stateName)
        {
            State matchedState = new State();
            var states = _stateRepository.ListStates();

            foreach (var state in states)
            {
                if (stateName == state.StateName)
                {
                    matchedState = state;
                }
                if (stateName == state.StateAbbreviation)
                {
                    matchedState = state;
                }

            }

            return matchedState;
        }
    }
}
