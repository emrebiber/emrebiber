﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Factories;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.BLL
{
    public class ProductManager
    {
        private IProductRepository _productRepository;

        public ProductManager()
        {
            _productRepository = ProductRepositoryFactory.GetProductRepository();
        }

        public bool IsInputAValidProductType(string productInput)
        {
            bool result = false;
            var products = _productRepository.ListProducts();

            foreach (var prod in products)
            {
                if (productInput == prod.ProductType)
                {
                    result = true;
                }

            }

            return result;
        }

        public Product GetProduct(string prodcutType)
        {
            Product matchedProduct = new Product();
            var products = _productRepository.ListProducts();

            foreach (var product in products)
            {
                if (prodcutType == product.ProductType)
                {
                    matchedProduct = product;
                }
            }

            return matchedProduct;
        }


    }
}
