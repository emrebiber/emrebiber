﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Interfaces
{
    public interface IProductRepository
    {
        List<Product> ListProducts();
        Product GetProduct(string productType);
    }
}
