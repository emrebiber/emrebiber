﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> ListOrders();
        List<Order> ListOrderByDate(DateTime orderDate);
        Order ListOneSpecificOrder(DateTime orderDate, int orderNumber);  
        void AddOrder (Order order);
        void EditOrder (Order order);
        void RemoveOrder (Order order);

    }
}
