﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Interfaces
{

    public interface IStateRepository
    {
        List<State> ListStates();
        State GetState(string stateAbbreviation);
    }
}
