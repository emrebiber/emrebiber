﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories.MockRepo
{
    public class MockStateRepository : IStateRepository
    {
        private static List<State> _states;
        

        static MockStateRepository()

        {
            _states = new List<State>()
            {
                new State() {StateAbbreviation = "OH", StateName = "Ohio", TaxRate = 5.75M},
                new State() {StateAbbreviation = "AL", StateName = "Alabama", TaxRate = 4.00M},
                new State() {StateAbbreviation = "TX", StateName = "Texas", TaxRate = 6.25M},
                new State() {StateAbbreviation = "AR", StateName = "Arkansas", TaxRate = 5.01M},
                new State() {StateAbbreviation = "NY", StateName = "New York", TaxRate = 7.75M}
            };
        }


        public List<State> ListStates()
        {
           return _states;
        }

        public State GetState(string stateAbbreviation)
        {
            return _states.FirstOrDefault(a => a.StateAbbreviation == stateAbbreviation);
        }
    }
}