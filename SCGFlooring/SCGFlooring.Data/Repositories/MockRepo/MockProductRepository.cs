﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories.MockRepo
{
    public class MockProductRepository : IProductRepository
    {
        private static List<Product> _products;

        static MockProductRepository()

        {
            _products = new List<Product>()
            {
                new Product() {ProductType = "Wood", CostPerSquareFoot = 2.25M, LaborCostPerSquareFoot = 3.50M},
                new Product() {ProductType = "Tile", CostPerSquareFoot = 4.25M, LaborCostPerSquareFoot = 3.75M},
                new Product() {ProductType = "Bamboo", CostPerSquareFoot = 1.85M, LaborCostPerSquareFoot = 2.50M}

            };
        }

        public List<Product> ListProducts()
        {
            return _products;
        }

        public Product GetProduct(string productType)
        {
             return _products.FirstOrDefault(a => a.ProductType == productType);
        }
    }
}

