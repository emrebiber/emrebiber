﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories
{
    public class MockOrderRepository : IOrderRepository
    {
        private static List<Order> _orders;
       
        static MockOrderRepository()
        {
            _orders = new List<Order>()
            {
                new Order()
                {
                    OrderNumber = 1,
                    OrderDate = DateTime.Parse("01/05/2016"),
                    CustomerName = "Dave Balzer",
                    StateName = "Ohio",
                    TaxRate = 0.0575M,
                    ProductType = "Shag Carpet",
                    OrderArea = 33.5M,
                    CostPerSquareFoot = 1.85M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 56.25M,
                    OrderTax = 5.63M,
                    OrderTotalCost = 103.51M
                },
                new Order()
                {
                    OrderNumber = 2,
                    OrderDate = DateTime.Parse("01/06/2016"),
                    CustomerName = "Willie Nelson",
                    StateName = "Texas",
                    TaxRate = 0.0625M,
                    ProductType = "Mexican Ceramic Tile",
                    OrderArea = 50.00M,
                    CostPerSquareFoot = 4.25M,
                    LaborCostPerSquareFoot = 3.75M,
                    MaterialCost = 212.50M,
                    LaborCost = 187.50M,
                    OrderTax = 25.00M,
                    OrderTotalCost = 425.00M
                },
                new Order()
                {
                    OrderNumber = 3,
                    OrderDate = DateTime.Parse("01/07/2016"),
                    CustomerName = "Bear Bryant",
                    StateName = "Alabama",
                    TaxRate = 0.04M,
                    ProductType = "Wood Laminate",
                    OrderArea = 12.25M,
                    CostPerSquareFoot = 2.25M,
                    LaborCostPerSquareFoot = 3.50M,
                    MaterialCost = 27.56M,
                    LaborCost = 42.88M,
                    OrderTax = 2.82M,
                    OrderTotalCost = 73.26M 
                },

                 new Order()
                {
                    OrderNumber = 4,
                    OrderDate = DateTime.Parse("01/11/2016"),
                    CustomerName = "Irem Orbay",
                    StateName = "New York",
                    TaxRate = 0.55M,
                    ProductType = "Wood Laminate",
                    OrderArea = 12.5M,
                    CostPerSquareFoot = 1.85M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 56.25M,
                    OrderTax = 5.63M,
                    OrderTotalCost = 103.51M
                },

                  new Order()
                {
                    OrderNumber = 5,
                    OrderDate = DateTime.Parse("01/12/2016"),
                    CustomerName = "Emre Biber",
                    StateName = "Arkansas",
                    TaxRate = 0.0665M,
                    ProductType = "Plastic Floor",
                    OrderArea = 10.5M,
                    CostPerSquareFoot = 1.00M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 12.25M,
                    OrderTax = 5.03M,
                    OrderTotalCost = 103.51M
                },


            };

        }

        public List<Order> ListOrders()
        {
            return _orders;
        }

        public List<Order> ListOrderByDate(DateTime orderDate)
        {
            return(from order in _orders where orderDate == order.OrderDate select order).ToList();
        }

        public Order ListOneSpecificOrder(DateTime orderDate, int orderNumber)
        {
            var orders = ListOrderByDate(orderDate);
            var order = orders.FirstOrDefault(o => o.OrderNumber == orderNumber);

            return order;
        }

        public void AddOrder(Order order)
        {
            _orders.Add(order);
        }

        public void EditOrder(Order order)
        {
            var currentOrder = ListOrderByDate(order.OrderDate);
            int index = currentOrder.FindIndex(o => o.OrderNumber == order.OrderNumber);
            currentOrder[index] = order;

        }

        public void RemoveOrder(Order order)
        {
            _orders.RemoveAt(_orders.IndexOf(order));



        }
    }
}
