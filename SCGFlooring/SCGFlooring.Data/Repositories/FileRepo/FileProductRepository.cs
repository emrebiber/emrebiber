﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories.FileRepo
{
    public class FileProductRepository : IProductRepository
    {
        private static List<Product> _products;
        private string _filePath = @"C:\Data\SCGFlooring\Products.txt";

        public List<Product> ListProducts()
        {
            _products = new List<Product>();
            using (StreamReader reader = new StreamReader(_filePath))
            {
                string[] line = reader.ReadToEnd().Split('\n');

                for (int i = 1; i < line.Length; i++)
                {
                    Product newProduct = new Product();

                    string record = line[i];
                    string[] columns = record.Split(',');
                    newProduct.ProductType = columns[0];
                    newProduct.CostPerSquareFoot = decimal.Parse(columns[1]);
                    newProduct.LaborCostPerSquareFoot = decimal.Parse(columns[2]);

                    _products.Add(newProduct);
                }

            }

            return _products;
        }

        public Product GetProduct(string productType)
        {
            throw new NotImplementedException();
        }
    }
}
