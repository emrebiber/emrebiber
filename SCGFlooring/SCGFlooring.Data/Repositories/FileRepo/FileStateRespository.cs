﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories.FileRepo
{
    public class FileStateRespository : IStateRepository
    {
        private static List<State> _states;
        private string _filePath = @"C:\Data\SCGFlooring\States.txt";


        public List<State> ListStates()
        {
            _states = new List<State>();
            using (StreamReader reader = new StreamReader(_filePath))
            {
                string[] line = reader.ReadToEnd().Split('\n');

                for (int i = 1; i < line.Length; i++)
                {
                    State newState = new State();

                    string record = line[i];
                    string[] columns = record.Split(',');
                    newState.StateAbbreviation = columns[0];
                    newState.StateName = columns[1];
                    newState.TaxRate = decimal.Parse(columns[2]);

                    _states.Add(newState);
                }

            }

            return _states;
        }

        public State GetState(string stateAbbreviation)
        {
            throw new NotImplementedException();
        }
    }
}
