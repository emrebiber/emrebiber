﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Models;

namespace SCGFlooring.Data.Repositories
{
    public class FileOrderRepository : IOrderRepository
    {
        public static List<Order> _orders;
        private List<string> _orderFile = new List<string>();
        private const string _directoryPath = @"C:\Data\SCGFlooring\";

   
        public FileOrderRepository()
        {
            // _orders = new List<Order>();
            _orderFile = CreateDirectoryAndListOrderFilesInDirectory();
        }

        public string ProperNameForFile(DateTime orderDate)
        {
            string fileName = "Orders_" + orderDate.ToString("MMddyyyy") + ".txt";

            return fileName;
        }

        public List<string> CreateDirectoryAndListOrderFilesInDirectory()
        {
            
            try
            {
                // Determine whether the directory exists.
                if (!Directory.Exists(_directoryPath))
                {
                    Directory.CreateDirectory(_directoryPath);
                }
                else
                {
                    DirectoryInfo dir = new DirectoryInfo(_directoryPath);
                    foreach (var file in dir.GetFiles("*.txt"))
                    {
                        _orderFile.Add(file.Name);
                    }
                } 
            }
            catch (Exception ex)
            {
                Console.WriteLine("The process failed: {0}", ex.ToString());
                ErrorLogs.LogErrors(ex.Message);
            }

            return _orderFile;
        }

        public List<Order> ListOrders()
        {
            return _orders;
        }

        public List<Order> ListOrderByDate(DateTime orderDate)
        {
            string fileName = ProperNameForFile(orderDate);
            List<Order> orderList = new List<Order>();
           

            if (_orderFile.Contains(fileName))
            {
                var rows = File.ReadAllLines(_directoryPath + fileName);

                for (int i = 1; i < rows.Length; i++)
                {
                    var columns = rows[i].Split(',');

                    var order = new Order();
                    order.OrderNumber = int.Parse(columns[0]);
                    order.OrderDate = DateTime.Parse(columns[1]);
                    order.CustomerName = columns[2];
                    order.StateName = columns[3];
                    order.TaxRate = decimal.Parse(columns[4]);
                    order.ProductType = columns[5];
                    order.OrderArea = decimal.Parse(columns[6]);
                    order.CostPerSquareFoot = decimal.Parse(columns[7]);
                    order.LaborCostPerSquareFoot = decimal.Parse(columns[8]);
                    order.MaterialCost = decimal.Parse(columns[9]);
                    order.LaborCost = decimal.Parse(columns[10]);
                    order.OrderTax = decimal.Parse(columns[11]);
                    order.OrderTotalCost = decimal.Parse(columns[12]);

                    orderList.Add(order);
                }    

            }

            return orderList;

        }

        public void CreateAndWriteFile(List<Order> orders,DateTime date)
        {
            string fileName = ProperNameForFile(date);

            if (orders.Count == 0)
            {
                File.Delete(_directoryPath + fileName);
            }
            else
            {
                File.Delete(_directoryPath + fileName);

                using (StreamWriter writer = File.CreateText(_directoryPath + fileName))
                {
                    writer.WriteLine("OrderNumber,OrderDate,CustomerName,StateName,TaxRate,ProductType,OrderArea,CostPerSquareFoot," +
                                    "LaborCostPerSquareFoot,MaterialCost,LaborCost,OrderTax,OrderTotalCost");

                    foreach (var order in orders)
                    {
                       

                        writer.WriteLine($"{order.OrderNumber},{order.OrderDate},{order.CustomerName},{order.StateName},{order.TaxRate},{order.ProductType},{order.OrderArea},{order.CostPerSquareFoot}," +
                                $"{order.LaborCostPerSquareFoot}, {order.MaterialCost},{order.LaborCost},{order.OrderTax},{order.OrderTotalCost} ");
                    }

           
                }
            }
        }

        public Order LoadOrderByDate(DateTime orderDate)
        {
            return _orders.FirstOrDefault(o => o.OrderDate == orderDate);
        }

        public void AddOrder(Order order)
        {
            var orders = ListOrderByDate(order.OrderDate);
            orders.Add(order);
            CreateAndWriteFile(orders, order.OrderDate);
        }

        public Order ListOneSpecificOrder(DateTime orderDate, int orderNumber)
        {
            var orders = ListOrderByDate(orderDate);
            var order = orders.FirstOrDefault(o => o.OrderNumber == orderNumber);

            return order;
        }

        public void EditOrder(Order order)
        {
            var currentOrder = ListOrderByDate(order.OrderDate);
            int index = currentOrder.FindIndex(o => o.OrderNumber == order.OrderNumber);
            currentOrder[index] = order;
            CreateAndWriteFile(currentOrder, order.OrderDate);
        }

        public void RemoveOrder(Order order)
        {
            //deletes the .txt file 
            //var currentOrder = ListOrderByDate(order.OrderDate);
            //currentOrder.RemoveAt(currentOrder.FindIndex(o => o.OrderNumber == order.OrderNumber));
            //CreateAndWriteFile(currentOrder, order.OrderDate);

            var orders = ListOrderByDate(order.OrderDate);
            Order deleteOrder = new Order();
            foreach (var o in orders)
            {
                if (o.OrderNumber == order.OrderNumber)
                {
                    deleteOrder = o;
                }
            }

            orders.Remove(deleteOrder);

            CreateAndWriteFile(orders, order.OrderDate);
            
        }

       
    }
}
