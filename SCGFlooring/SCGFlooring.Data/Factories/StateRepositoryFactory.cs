﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Data.Repositories.FileRepo;
using SCGFlooring.Data.Repositories.MockRepo;

namespace SCGFlooring.Data.Factories
{
    public static class StateRepositoryFactory
    {
        public static IStateRepository GetStateRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockStateRepository();
                case "Prod":
                    return new FileStateRespository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
