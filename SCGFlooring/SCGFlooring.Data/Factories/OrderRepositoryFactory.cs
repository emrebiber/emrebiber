﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGFlooring.Data.Interfaces;
using SCGFlooring.Data.Repositories;

namespace SCGFlooring.Data.Factories
{
    public static class OrderRepositoryFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockOrderRepository();
                case "Prod":
                    return new FileOrderRepository();
                default:
                    throw new ArgumentException();
            }

        }
    }

}
