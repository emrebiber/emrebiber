﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Data;
using GeneralReview.Helpers;
using GeneralReview.Models;
using NUnit.Framework;

namespace GeneralReview.Tests
{
    [TestFixture]
    public class EmployeeTests
    {
        [Test]
        public void CanReadDataFromFile()
        {
            EmployeeRepository repo = new EmployeeRepository(FileLocation.fileLocation);

            List<Employee> employees = repo.List();
            Assert.AreEqual(3, employees.Count);
        }
    }
}
