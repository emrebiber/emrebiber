﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Interfaces;
using GeneralReview.Models;

namespace GeneralReview.Data
{
    public class StudentRepository : IStudentRepository
    {
        private static List<Student> _student;
           
        static StudentRepository()
        {
            _student = new List<Student>()
            {
                new Student() {FirstName = "Emre", LastName = "Biber", Major = "Computer Science", GPA = 4.0M, StudentId = 1},
                new Student() {FirstName = "John", LastName = "Smith", Major = "English", GPA = 2.2M, StudentId = 5},
                new Student() {FirstName = "Tom", LastName = "Jerry", Major = "English", GPA = 3.0M, StudentId = 2},
                new Student() {FirstName = "Irem", LastName = "Orbay", Major = "Business", GPA = 2.8M, StudentId = 3},
                new Student() {FirstName = "Jennifer", LastName = "Lopez", Major = "Fine Arts", GPA = 3.5M, StudentId = 6},
                new Student() {FirstName = "Dave", LastName = "Balzer", Major = "Marketing", GPA = 2.0M, StudentId = 4}

            };

            //_student.Add(new Student() { FirstName = "Emre", LastName = "Biber", Major = "Computer Science", GPA = 4.0M, StudentId = 1 });
            //_student.Add(new Student() { FirstName = "John", LastName = "Smith", Major = "English", GPA = 2.2M, StudentId = 5 });
            //_student.Add(new Student() { FirstName = "Tom", LastName = "Jerry", Major = "English", GPA = 3.0M, StudentId = 2 });
            //_student.Add(new Student() { FirstName = "Irem", LastName = "Orbay", Major = "Business", GPA = 2.8M, StudentId = 3 });
            //_student.Add(new Student() { FirstName = "Jennifer", LastName = "Lopez", Major = "Fine Arts", GPA = 3.5M, StudentId = 6 });
            //_student.Add(new Student() { FirstName = "Dave", LastName = "Balzer", Major = "Marketing", GPA = 2.0M, StudentId = 4 });

        }

           

        public List<Student> LoadAll()
        {
            return _student;
        }

        public List<Employee> List()
        {
            throw new NotImplementedException();
        }

        public void AddStudent(Student student)
        {
           _student.Add(student);
        }

        public void Edit(Student student, int index)
        {
            StudentRepository repo = new StudentRepository();
            List<Student> students = repo.LoadAll();

            students[index] = student;
        }

        public void Delete(int index)
        {
            StudentRepository repo = new StudentRepository();
            List<Student> students = repo.LoadAll();

            students.RemoveAt(index);
        }
        


    }
}
