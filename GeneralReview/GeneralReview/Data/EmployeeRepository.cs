﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Interfaces;
using GeneralReview.Models;

namespace GeneralReview.Data
{
    public class EmployeeRepository : IStudentRepository
    {
   
        public string FilePath;

        public EmployeeRepository(string path)
        {
            FilePath = path;

        }

        
        public List<Employee> List()
        {
            List<Employee> employees = new List<Employee>();

            using (StreamReader reader = new StreamReader(FilePath))
            {
                reader.ReadLine();
                string line;

                while ((line = reader.ReadLine()) != null)
                {
                    Employee newEmployee = new Employee();
                    string[] columns = line.Split(' ');
                    newEmployee.FirstName = columns[0];
                    newEmployee.LastName = columns[1];
                    newEmployee.Description = columns[2];
                    newEmployee.EmployeeId = int.Parse(columns[3]);

                    employees.Add(newEmployee);


                }
            }
            return employees;
        }

        List<Student> IStudentRepository.LoadAll()
        {
            throw new NotImplementedException();
        }

        public void AddStudent(Student student)
        {
            throw new NotImplementedException();
        }

        public void Edit(Student student, int index)
        {
            throw new NotImplementedException();
        }

        public void Delete(int index)
        {
            throw new NotImplementedException();
        }

        public List<Employee> LoadAll()
        {
            throw new NotImplementedException();
        }
    }
}
