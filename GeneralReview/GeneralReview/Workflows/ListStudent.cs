﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Data;
using GeneralReview.Models;

namespace GeneralReview.Workflows
{
    public class ListStudent
    {
        public void Execute()
        {
            Console.Clear();        
            StudentRepository repo = new StudentRepository();
            List<Student> student = repo.LoadAll();
            UserPrompts.PrintStudents(student);
        }
    }
}
