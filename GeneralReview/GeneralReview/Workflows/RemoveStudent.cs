﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Data;
using GeneralReview.Models;

namespace GeneralReview.Workflows
{
    public class RemoveStudent
    {
        public void Execute()
        {
            StudentRepository repo = new StudentRepository();
            List<Student> student = repo.LoadAll();

            int index = UserPrompts.GetStudentIndexFromUser("Enter the student id to delete: ", student.Count);

            repo.Delete(index);

            Console.WriteLine("Student removed");

        }
    }
}
