﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Data;
using GeneralReview.Models;

namespace GeneralReview.Workflows
{
    public class AddStudent
    {
        public void Execute()
        {

            Console.Clear();
            StudentRepository repo = new StudentRepository();
            Student newStudent = new Student();

            newStudent.FirstName = UserPrompts.GetStringFromUser("Please Enter a first name: ");
            newStudent.LastName = UserPrompts.GetStringFromUser("Please enter last name: ");
            newStudent.Major = UserPrompts.GetStringFromUser("Please enter major: ");
            newStudent.GPA = UserPrompts.GetDecimalFromUser("Please enter the GPA: ");
            newStudent.StudentId = UserPrompts.GetIntegerFromUser("Please enter the Student ID: ");

            repo.AddStudent(newStudent);

            Console.WriteLine("You succesfully added student record");
            UserPrompts.PressAnyKeyToContinue();
        }


    }
}
