﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Data;
using GeneralReview.Models;

namespace GeneralReview.Workflows
{
    public class EditStudent
    {
        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("Edit Student");
            Console.WriteLine(UserPrompts._seperatorBar);

            StudentRepository repo = new StudentRepository();
            List<Student> student = repo.LoadAll();

            Console.WriteLine();
            Console.WriteLine(UserPrompts._seperatorBar);

            int index = UserPrompts.GetStudentIndexFromUser("Which student would you like to edit? ", student.Count());

            Console.WriteLine();

            student[index].GPA =
                UserPrompts.GetDecimalFromUser(string.Format("Enter new GPA for {0} {1}", student[index].FirstName,
                    student[index].LastName, student[index].Major, student[index].GPA, student[index].StudentId, index));

            Console.WriteLine("GPA Updated!");
            UserPrompts.PressAnyKeyToContinue();






        }
    }
}
