﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview;

namespace GeneralReview.Models
{
    public class Student : Person
    {
        public int StudentId { get; set; }
        public string Major { get; set; }
        public decimal GPA { get; set; }
        
    }
}
