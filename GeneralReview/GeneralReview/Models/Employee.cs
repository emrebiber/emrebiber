﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralReview.Models
{
    public class Employee : Person
    {
        public int EmployeeId { get; set; }
        public string Description { get; set; }

    }
}