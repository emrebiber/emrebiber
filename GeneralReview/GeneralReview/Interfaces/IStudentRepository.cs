﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Models;

namespace GeneralReview.Interfaces
{
    public interface IStudentRepository
    {
        List<Student> LoadAll();
        List<Employee> List();
        void AddStudent(Student student);
        void Edit(Student student, int index);
        void Delete(int index);

    }
}
