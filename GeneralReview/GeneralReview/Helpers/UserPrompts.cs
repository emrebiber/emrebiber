﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneralReview.Models;
using GeneralReview.Workflows;

namespace GeneralReview
{
    public class UserPrompts
    {
        public const string _seperatorBar = "==================================================";
        public const string _format = "{0, -15} {1, -15} {2, -15} {3, -15} {4, -15}";
       // public _headerFormat = ""

        public static int GetIntegerFromUser(string prompt)
        {
            do
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                int output;

                if (int.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("You must enter integer value");
                    PressAnyKeyToContinue();
                }
            } while (true);
        }

        public static decimal GetDecimalFromUser(string prompt)
        {
            do
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                decimal output;

                if (decimal.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("You must enter decimal value!");
                    ProcessUserChoice();
                }
            } while (true);
        }

        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input))
                {
                    return input;
                }
                else
                {
                    Console.WriteLine("You must enter string!");
                    PressAnyKeyToContinue();
                }
            } while (true);

        }

        public static int GetStudentIndexFromUser(string prompt, int count)
        {
            int output;
            while (true)
            {
                Console.Write(prompt);
                string input = Console.ReadLine();



                if (!int.TryParse(input, out output))
                {
                    Console.WriteLine("You must enter valid integer.");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey();
                }
                else
                {
                    if (output < 1 || output > count)
                    {
                        Console.WriteLine("Please choose a student by number betwwen {0} and {1}", 1, count);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                        continue;
                    }
                    return output;
                }
            }
        }


        public static void PrintStudents(List<Student> student)
        {
            Console.Clear();
            Console.WriteLine("Student List");
            Console.WriteLine(_seperatorBar);
            if (student.Count == 0)
            {
                Console.WriteLine("There is no student in the system.");
            }
            else
            {
                foreach (var s in student)
                {
                    Console.WriteLine(_format, s.FirstName, s.LastName, s.Major , s.GPA, s.StudentId);
                }
            }
        }

        public static void DisplayMenu()
        {
            Console.Title = "Student Manager";
            Console.WriteLine("Student Management System");
            Console.WriteLine("======================================");
            Console.WriteLine("1. List Student");
            Console.WriteLine("2. Add Student");
            Console.WriteLine("3. Remove Student ");
            Console.WriteLine("4. Edit Student GPA");
            Console.WriteLine();
            Console.WriteLine("Q - Quit");
            Console.WriteLine("======================================");
            Console.WriteLine();
            Console.Write("Enter Choice: ");

        }

        public static bool ProcessUserChoice()
        {
            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    ListStudent list = new ListStudent();
                    list.Execute();
                   // Console.WriteLine("List Students");
                    break;
                case "2":
                    AddStudent add = new AddStudent();
                    add.Execute();
                  //  Console.WriteLine("add Students");
                    break;
                case "3":
                    RemoveStudent remove = new RemoveStudent();
                    remove.Execute();
                    //Console.WriteLine("remove Students");
                    break;
                case "4":
                    EditStudent edit = new EditStudent();
                    edit.Execute();
                  //  Console.WriteLine("edit Students");
                    break;
                case "Q":
                    return false;
                default:
                    Console.WriteLine("This is not a valid choice.");
                    PressAnyKeyToContinue();
                    break;

            }

            Console.WriteLine();
            return true;
        }

        public static void PressAnyKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

      

        public static void Show()
        {
            
            bool continueRunning = true;

            while (continueRunning)
            {
                DisplayMenu();
                continueRunning = ProcessUserChoice();
            }



        }


    }
}
