﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCStudentInfo.Models
{
    public class Student
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int StudentId { get; set; }
        public decimal GPA { get; set; }
        public Major Major { get; set; }
        public List<Courses> Course { get; set; }
    }
}
