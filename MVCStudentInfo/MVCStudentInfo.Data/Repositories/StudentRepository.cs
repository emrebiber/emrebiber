﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private static List<Student> _students;

        static StudentRepository()
        {
            _students = new List<Student>
            {
                new Student()
                {
                    FirstName = "Emre", LastName = "Biber", StudentId = 1, GPA = 4.0M,
                    Major = new Major() {MajorId = 1, MajorName = "Computer Science"},
                    Course = new List<Courses>()
                    {
                        new Courses() {CourseId = 5, CourseName = "C# Fundamental"},
                        new Courses() {CourseId = 6, CourseName = "Economics 101"}
                        
                    }
                },

                new Student()
                {
                    FirstName = "Tom", LastName = "Jerry", StudentId = 2, GPA = 2.0M,
                    Major = new Major() {MajorId = 2, MajorName = "Business"},
                    Course = new List<Courses>()
                    {
                        new Courses() {CourseId = 1, CourseName = "Art History"},
                        new Courses() {CourseId = 8, CourseName = "Photography"},
                        new Courses() {CourseId = 3, CourseName = "Biology 101"}

                    }
                },

                new Student()
                {
                    FirstName = "Dave", LastName = "Balzer", StudentId = 3, GPA = 3.2M,
                    Major = new Major() {MajorId = 4, MajorName = "Mathematics"},
                    Course = new List<Courses>()
                    {
                        new Courses() {CourseId = 4, CourseName = "Business Law"},
                    }
                },

                new Student()
                {
                    FirstName = "John", LastName = "Smith", StudentId = 4, GPA = 2.9M,
                    Major = new Major() {MajorId = 5, MajorName = "Physics"},
                    Course = new List<Courses>()
                    {
                        new Courses() {CourseId = 7, CourseName = "Java Fundamentals"},
                    }
                }
            };
        }


        public List<Student> GetAll()
        {
            return _students;
        }

        public Student Get(int studentId)
        {
            return _students.FirstOrDefault(s => s.StudentId == studentId);
        }

        public void Add(Student student)
        {
            _students.Add(student);
        }

        public void Edit(Student student)
        {
            var selectedStudent = _students.First(s => s.StudentId == student.StudentId);

            selectedStudent.FirstName = student.FirstName;
            selectedStudent.LastName = student.LastName;
            selectedStudent.GPA = student.GPA;
        }

        public void Delete(int studentId)
        {
            _students.RemoveAll(s => s.StudentId == studentId);
        }
    }
}


