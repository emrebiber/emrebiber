﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private static List<Courses> _courses;

        static CourseRepository()
        {
            _courses = new List<Courses>
            {
                new Courses() { CourseId=1, CourseName="Art History" },
                new Courses() { CourseId=2, CourseName="Accounting 101" },
                new Courses() { CourseId=3, CourseName="Biology 101" },
                new Courses() { CourseId=4, CourseName="Business Law" },
                new Courses() { CourseId=5, CourseName="C# Fundamentals" },
                new Courses() { CourseId=6, CourseName="Economics 101" },
                new Courses() { CourseId=7, CourseName="Java Fundamentals" },
                new Courses() { CourseId=8, CourseName="Photography" }
            };
        }

        public List<Courses> GetAll()
        {
            return _courses;
        }

        public Courses Get(int courseId)
        {
            return _courses.FirstOrDefault(c => c.CourseId == courseId);
        }

        public void Add(Courses course)
        {
            course.CourseName = course.CourseName;
            course.CourseId = _courses.Max(c => c.CourseId) + 1;

            _courses.Add(course);
        }

        public void Edit(Courses course)
        {
            var selectedCourse = _courses.First(c => c.CourseId == course.CourseId);
            selectedCourse.CourseName = course.CourseName;
           
        }

        public void Delete(int courseId)
        {
            _courses.RemoveAll(c => c.CourseId == courseId);
        }
    }
}
