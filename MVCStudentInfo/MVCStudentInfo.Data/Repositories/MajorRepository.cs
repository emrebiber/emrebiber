﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Repositories
{
    public class MajorRepository : IMajorRepository
    {
        private static List<Major> _majors;

        static MajorRepository()
        {
            // sample data
            _majors = new List<Major>
                {
                    new Major { MajorId=1, MajorName="Art" },
                    new Major { MajorId=2, MajorName="Business" },
                    new Major { MajorId=3, MajorName="Computer Science" },
                    new Major { MajorId=4, MajorName="Mathematics" },
                    new Major { MajorId=5, MajorName="Physics" },   
                };
        }

        public List<Major> GetAll()
        {
            return _majors;
        }

        public Major Get(int majorId)
        {
            return _majors.FirstOrDefault(m => m.MajorId == majorId);
            
        }

        public void Add(Major major)
        {
            major.MajorName = major.MajorName;
            major.MajorId = _majors.Max(c => c.MajorId) + 1;

            _majors.Add(major);
        }

        public void Edit(Major major)
        {
            var selectedMajor = _majors.First(m => m.MajorId == major.MajorId);
            selectedMajor.MajorName = major.MajorName;

        }

        public void Delete(int majorId)
        {
            _majors.RemoveAll(c => c.MajorId == majorId);
        }
    }
}
