﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Interfaces
{
    public interface IStudentRepository
    {
        List<Student> GetAll();
        Student Get(int studentId);
        void Add(Student student);
        void Edit(Student student);
        void Delete(int studentId);
    }
}
