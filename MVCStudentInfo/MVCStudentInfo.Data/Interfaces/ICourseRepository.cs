﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Interfaces
{
    public interface ICourseRepository
    {
        List<Courses> GetAll();
        Courses Get(int courseId);
        void Add(Courses course);
        void Edit(Courses course);
        void Delete(int courseId);
    }
}
