﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Data.Interfaces
{
    public interface IMajorRepository
    {
        List<Major> GetAll();
        Major Get(int majorId);
        void Add(Major major);
        void Edit(Major major);
        void Delete(int majorId);
    }
}
