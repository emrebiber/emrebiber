﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Repositories;
using MVCStudentInfo.Models;
using NUnit.Framework;

namespace MVCStudentInfo.Tests
{
    [TestFixture]
    public class StudentRepositoryTests
    {

        [Test]
        public void CanListStudents()
        {
            var repo = new StudentRepository();
            var student = repo.GetAll();

            Assert.AreEqual(4, student.Count);
        }

        [Test]
        public void CanAddStudent()
        {
            var repo = new StudentRepository();
            var newStudent = new Student();

            newStudent.StudentId = 10;
            newStudent.FirstName = "Emre";
            newStudent.LastName = "Biber2";
            newStudent.GPA = 2.9M;

        }
    }
}
