﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Data.Repositories;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.BLL
{
    public class StudentManager
    {
        private  IStudentRepository _studentRepository;

        public StudentManager()
        {
            _studentRepository = new StudentRepository();
        }

        public  List<Student> ListAllStudents()
        {
            var student = _studentRepository.GetAll();

            return student;
        }

        public Student GetSpecificStudent(int studentId)
        {
            var student = _studentRepository.Get(studentId);
            return student;
        }

        public void AddStudent(Student student)
        {
            _studentRepository.Add(student);

        }

        public void EditStudent(Student student)
        {
            _studentRepository.Edit(student);
        }

        public void DeleteStudent(int studentId)
        {
            _studentRepository.Delete(studentId);
        }


    }
}
