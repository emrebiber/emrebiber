﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Data.Repositories;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.BLL
{
    public class MajorManager
    {
        private IMajorRepository _majorRepository;

        public MajorManager()
        {
            _majorRepository = new MajorRepository();
        }

        public List<Major> ListAllMajors()
        {
            var major = _majorRepository.GetAll();

            return major;
        }

        public Major GetSpecificMajor(int majorId)
        {
            var major = _majorRepository.Get(majorId);

            return major;
        }

        public void AddMajor(Major major)
        {
            _majorRepository.Add(major);
        }

        public void EditMajor(Major major)
        {
            _majorRepository.Edit(major);
        }

        public void DeleteMajor(int majorId)
        {
            _majorRepository.Delete(majorId);
        }
    }
}
