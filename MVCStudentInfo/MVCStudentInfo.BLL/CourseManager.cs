﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVCStudentInfo.Data.Interfaces;
using MVCStudentInfo.Data.Repositories;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.BLL
{
    public class CourseManager
    {
        private ICourseRepository _courseRepository;

        public CourseManager()
        {
            _courseRepository = new CourseRepository();
        }

        public List<Courses> ListAllCourses()
        {
            var course = _courseRepository.GetAll();

            return course;
        }

        public Courses GetSpecificCourse(int courseId)
        {
            var course = _courseRepository.Get(courseId);

            return course;
        }

        public void AddCourse(Courses course)
        {
           _courseRepository.Add(course);
        }

        public void EditCourse(Courses course)
        {
            _courseRepository.Edit(course);
        }

        public void DeleteCourse(int courseId)
        {
            _courseRepository.Delete(courseId);
        }

    }
}
