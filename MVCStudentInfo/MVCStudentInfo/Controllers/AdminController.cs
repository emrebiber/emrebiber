﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCStudentInfo.BLL;

namespace MVCStudentInfo.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ListMajors()
        {
            var manager = new MajorManager();
            var major = manager.ListAllMajors();

            return View(major);
        }
        
    }
}