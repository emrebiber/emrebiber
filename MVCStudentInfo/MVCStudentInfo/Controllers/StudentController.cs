﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using MVCStudentInfo.BLL;
using MVCStudentInfo.Models;

namespace MVCStudentInfo.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult List()
        {
            var manager  = new StudentManager();
            var student =  manager.ListAllStudents();

            return View(student.ToList());
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(new Student());
        }

        [HttpPost]
        public ActionResult Add(Student student)
        {
            var manager = new StudentManager();
            manager.AddStudent(student);

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var manager = new StudentManager();
            var student = manager.GetSpecificStudent(id);

            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(Student student)
        {
            var manager = new StudentManager();
            manager.EditStudent(student);

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var manager = new StudentManager();
            var student = manager.GetSpecificStudent(id);

            return View(student);
        }

        [HttpPost]
        public ActionResult Delete(Student student)
        {
            var manager = new StudentManager();
            manager.DeleteStudent(student.StudentId);

            return RedirectToAction("List");
        }
    }
}