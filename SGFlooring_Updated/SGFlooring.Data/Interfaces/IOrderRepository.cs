﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> ListOrders(DateTime orderDate);
        Order GetOrder(List<Order> ordersList, int orderNumber);
        void AddOrder(Order order);
        void EditOrder(List<Order> ordersListr);
        void RemoveOrder(Order order);
    }
}
