﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class MockProductRepository : IProductRepository
    {

        private static List<Product> products;

        static MockProductRepository()

        {
            products = new List<Product>()
            {
                new Product() {ProductType = "Wood Laminate", CostPerSquareFoot = 2.25M, LaborCostPerSquareFoot = 3.50M},
                new Product() {ProductType = "Mexican Ceramic Tile", CostPerSquareFoot = 4.25M, LaborCostPerSquareFoot = 3.75M},
                new Product() {ProductType = "Shag Carpet", CostPerSquareFoot = 1.85M, LaborCostPerSquareFoot = 2.50M}

            };
        }

        public List<Product> ListProducts()
        {
            return products;
        }

        public Product GetProduct(string productType)
        {
            return products.FirstOrDefault(a => a.ProductType == productType);
        }
    }
}
