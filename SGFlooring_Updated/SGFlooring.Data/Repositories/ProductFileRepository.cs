﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class ProductFileRepository : IProductRepository
    {

        private string _fileDirectory; // = @"DataFiles\Products.txt";

        public ProductFileRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];
            if (mode == "NewTest")
            {
                _fileDirectory = @"C:\Data\SGFlooring\Products.txt";
            }
            else
            {
                _fileDirectory = @"DataFiles\Products.txt";
            }
        }


        public List<Product> ListProducts()
        {
            List<Product> productList = new List<Product>();

            var rows = File.ReadAllLines(_fileDirectory);

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(',');

                var product = new Product();
                product.ProductType = columns[0];
                product.CostPerSquareFoot = decimal.Parse(columns[1]);
                product.LaborCostPerSquareFoot = decimal.Parse(columns[2]);

                productList.Add(product);

            }

            return productList;
        }

        public Product GetProduct(string productType)
        {
            Product getProduct = new Product();
            var list = ListProducts();

            foreach (var product in list)
            {
                if (product.ProductType == productType)
                {
                    return product;
                }
            }

            return getProduct;
        }
    }
}
