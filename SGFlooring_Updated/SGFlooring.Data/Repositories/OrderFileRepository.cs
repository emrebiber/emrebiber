﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{

    public class OrderFileRepository : IOrderRepository
    {

        private List<string> _orderFileList = new List<string>();
        private string _fileDirectory; // = @"DataFiles\";

        public OrderFileRepository()
        {

            var mode = ConfigurationManager.AppSettings["Mode"];
            if (mode == "NewTest")
            {
                _fileDirectory = @"C:\Data\SGFlooring\";
            }
            else
            {
                _fileDirectory = @"DataFiles\";
            }

            _orderFileList = ListOrderFilesInDirectory();
        }

        public List<string> ListOrderFilesInDirectory()
        {
            DirectoryInfo dir = new DirectoryInfo(_fileDirectory);
            foreach (var file in dir.GetFiles("*.txt"))
            {

                _orderFileList.Add(file.Name);

            }
            return _orderFileList;
        }

        public List<Order> ListOrders(DateTime orderDate)
        {
            string orderFile = ConvertDateToFileValue(orderDate);
            List<Order> ordersList = new List<Order>();

            if (_orderFileList.Contains(orderFile))
            {
                var rows = File.ReadAllLines(_fileDirectory + orderFile);

                for (int i = 1; i < rows.Length; i++)
                {
                    var columns = rows[i].Split(',');

                    var order = new Order();
                    order.OrderNumber = int.Parse(columns[0]);
                    order.OrderDate = DateTime.Parse(columns[1]);
                    order.CustomerName = columns[2];
                    order.StateName = columns[3];
                    order.TaxRate = decimal.Parse(columns[4]);
                    order.ProductType = columns[5];
                    order.OrderArea = decimal.Parse(columns[6]);
                    order.CostPerSquareFoot = decimal.Parse(columns[7]);
                    order.LaborCostPerSquareFoot = decimal.Parse(columns[8]);
                    order.MaterialCost = decimal.Parse(columns[9]);
                    order.LaborCost = decimal.Parse(columns[10]);
                    order.OrderTax = decimal.Parse(columns[11]);
                    order.OrderTotalCost = decimal.Parse(columns[12]);

                    ordersList.Add(order);
                }

            }

            return ordersList;

        }

        public Order GetOrder(List<Order> ordersList, int orderNumber)
        {
            return ordersList.FirstOrDefault(a => a.OrderNumber == orderNumber);
        }

        public void AddOrder(Order order)
        {
            var orders = ListOrders(order.OrderDate);
            string commaReplace = order.CustomerName.Replace(",", "Z#k@m");
            order.CustomerName = commaReplace;
            orders.Add(order);
            OverwriteOrders(orders, order.OrderDate);
        }

        public void EditOrder(List<Order> ordersList)
        {
            AddOrder(ordersList[1]);
            RemoveOrder(ordersList[0]);
        }

        public void RemoveOrder(Order orderToDelete)
        {
            var orders = ListOrders(orderToDelete.OrderDate);

            var order = orders.First(o => o.OrderNumber == orderToDelete.OrderNumber);
            orders.Remove(order);

            OverwriteOrders(orders, orderToDelete.OrderDate);

        }

        public string ConvertDateToFileValue(DateTime orderDate)
        {
            string orderFileName = "Orders_" + orderDate.ToString("MMddyyyy") + ".txt";
            return orderFileName;
        }

        public void OverwriteOrders(List<Order> orders, DateTime date)
        {
            string orderFileName = ConvertDateToFileValue(date);

            if (orders.Count == 0)
            {
                File.Delete(_fileDirectory + orderFileName);
            }
            else
            {
                File.Delete(_fileDirectory + orderFileName);

                using (var writer = File.CreateText(_fileDirectory + orderFileName))
                {
                    writer.WriteLine("OrderNumber,OrderDate,CustomerName,StateName,TaxRate,ProductType,OrderArea,CostPerSquareFoot," +
                                     "LaborCostPerSquareFoot,MaterialCost,LaborCost,OrderTax,OrderTotalCost");

                    foreach (var o in orders)
                    {
                        writer.WriteLine(
                            $"{o.OrderNumber},{o.OrderDate},{o.CustomerName},{o.StateName},{o.TaxRate},{o.ProductType},{o.OrderArea},{o.CostPerSquareFoot}," +
                            $"{o.LaborCostPerSquareFoot}, {o.MaterialCost},{o.LaborCost},{o.OrderTax},{o.OrderTotalCost} ");
                    }
                }

            }
        }
    }
}
