﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class StateFileRepository : IStateRepository
    {
        
        private string _fileDirectory; // = @"DataFiles\States.txt";

        public StateFileRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];
            if (mode == "NewTest")
            {
                _fileDirectory = @"C:\Data\SGFlooring\States.txt";
            }
            else
            {
                _fileDirectory = @"DataFiles\States.txt";
            }
        }

        public List<State> ListStates()
        {

            List<State> stateList = new List<State>();

            var rows = File.ReadAllLines(_fileDirectory);

            for (int i = 1; i < rows.Length; i++)
            {
                var columns = rows[i].Split(',');

                var state = new State();
                state.StateName = columns[1];
                state.StateAbbreviation = columns[0];
                state.TaxRate = decimal.Parse(columns[2]);

                stateList.Add(state);

            }

            return stateList;
            
        }

        public State GetState(string stateValue)
        {
            var getState = new State();
            var list = ListStates();

            foreach (var state in list)
            {
                if (state.StateAbbreviation == stateValue || state.StateName == stateValue)
                {
                    return state;
                }
            }

            return getState;

        }
    }
}
