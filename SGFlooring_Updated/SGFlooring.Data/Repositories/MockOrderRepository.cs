﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class MockOrderRepository : IOrderRepository
    {

        private static List<Order> orders;

        static MockOrderRepository()
        {
            orders = new List<Order>()
            {
                new Order()
                {
                    OrderNumber = 1,
                    OrderDate = DateTime.Parse("01/05/2016"),
                    CustomerName = "Dave Balzer",
                    StateName = "Ohio",
                    TaxRate = 0.0575M,
                    ProductType = "Shag Carpet",
                    OrderArea = 22.5M,
                    CostPerSquareFoot = 1.85M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 56.25M,
                    OrderTax = 5.63M,
                    OrderTotalCost = 103.51M
                },
                new Order()
                {
                    OrderNumber = 2,
                    OrderDate = DateTime.Parse("01/06/2016"),
                    CustomerName = "Willie Nelson",
                    StateName = "Texas",
                    TaxRate = 0.0625M,
                    ProductType = "Mexican Ceramic Tile",
                    OrderArea = 50.00M,
                    CostPerSquareFoot = 4.25M,
                    LaborCostPerSquareFoot = 3.75M,
                    MaterialCost = 212.50M,
                    LaborCost = 187.50M,
                    OrderTax = 25.00M,
                    OrderTotalCost = 425.00M
                },
                new Order()
                {
                    OrderNumber = 3,
                    OrderDate = DateTime.Parse("01/07/2016"),
                    CustomerName = "Bear Bryant",
                    StateName = "Alabama",
                    TaxRate = 0.04M,
                    ProductType = "Wood Laminate",
                    OrderArea = 12.25M,
                    CostPerSquareFoot = 2.25M,
                    LaborCostPerSquareFoot = 3.50M,
                    MaterialCost = 27.56M,
                    LaborCost = 42.88M,
                    OrderTax = 2.82M,
                    OrderTotalCost = 73.26M
                }
            };
        }

        public List<Order> ListOrders(DateTime orderDate)
        {
            return orders;
        }

        public Order GetOrder(List<Order> ordersList, int orderNumber)
        {
            return ordersList.FirstOrDefault(a => a.OrderNumber == orderNumber);

        }

        public void AddOrder(Order order)
        {
            orders.Add(order);
        }

        public void EditOrder(List<Order> ordersList)
        {
            Order order = ordersList[1];
            var orderToUpdate = orders.First(a => a.OrderNumber == order.OrderNumber);
            orderToUpdate.CustomerName = order.CustomerName;
            orderToUpdate.StateName = order.StateName;
            orderToUpdate.TaxRate = order.TaxRate;
            orderToUpdate.ProductType = order.ProductType;
            orderToUpdate.OrderArea = order.OrderArea;
            orderToUpdate.CostPerSquareFoot = order.CostPerSquareFoot;
            orderToUpdate.LaborCostPerSquareFoot = order.LaborCostPerSquareFoot;
            orderToUpdate.MaterialCost = order.MaterialCost;
            orderToUpdate.LaborCost = order.LaborCost;
            orderToUpdate.OrderTax = order.OrderTax;
            orderToUpdate.OrderTotalCost = order.OrderTotalCost;
        }

        public void RemoveOrder(Order orderToRemove)
        {
            orders.Remove(orderToRemove);
        }
    }
}
