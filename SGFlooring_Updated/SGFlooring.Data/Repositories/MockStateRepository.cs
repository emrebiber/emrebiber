﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class MockStateRepository : IStateRepository
    {

        private static List<State> states;

        static MockStateRepository()

        {
            states = new List<State>()
            {
                new State() {StateAbbreviation = "OH", StateName = "Ohio", TaxRate = 5.75M},
                new State() {StateAbbreviation = "AL", StateName = "Alabama", TaxRate = 4.00M},
                new State() {StateAbbreviation = "TX", StateName = "Texas", TaxRate = 6.25M}
            };
        }


        public List<State> ListStates()
        {
            return states;
        }

        public State GetState(string stateValue)
        {
            return states.FirstOrDefault(a => a.StateAbbreviation == stateValue);
        }
    }
}
