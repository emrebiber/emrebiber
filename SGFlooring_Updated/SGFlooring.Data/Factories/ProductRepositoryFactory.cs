﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Data.Repositories;

namespace SGFlooring.Data.Factories
{
    public static class ProductRepositoryFactory
    {
        public static IProductRepository GetProductRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "NewTest":
                case "Prod":
                    return new ProductFileRepository();
                case "Test":
                    return new MockProductRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
