﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.Models;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class RemoveOrderWorkflow
    {
        OrderManager removeOrderManager = new OrderManager();

        public void Execute()
        {
            var orderList = removeOrderManager.ListOrders(GetRemoveOrderDateInput());
            
            if (orderList.Success)
            {

                GetOrderToRemove(orderList.Data);
                
            }
            else
            {

                Console.WriteLine(orderList.Message);

            }

            UserPrompts.PressKeyToContinue();

        }

        private void GetOrderToRemove(List<Order> orderList)
        {
            bool validOrderNumber = false;

            while (!validOrderNumber)
            {
                DisplayScreens.DisplayOrdersListView(orderList);

                var resultGet = removeOrderManager.GetOrder(orderList, GetRemoveOrderNumberInput());

                if (!resultGet.Success)
                {

                    Console.Clear();
                    Console.WriteLine(resultGet.Message);

                }
                else
                {

                    Console.Clear();
                    DisplayScreens.DisplayOrderDetailsView(resultGet.Data, "Order To Remove: ");
                    validOrderNumber = true;

                    if (UserPrompts.GetYesNoAnswerFromUser("\nDo you want to remove this order") == "Y")
                    {
                        var resultRemove = removeOrderManager.RemoveOrder(resultGet.Data);

                        if (resultRemove.Success)
                        {
                            DisplayScreens.DisplayDeleteOrderConfirmationView(resultRemove.Data);
                        }
                        else
                        {
                            Console.WriteLine(resultRemove.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("\nOrder will not be removed.");
                    }

                }
            }
        }

        private DateTime GetRemoveOrderDateInput()
        {
            DisplayScreens.RemoveOrderInputHeader();
            DateTime removeOrderDate = UserPrompts.GetDateFromUser("Enter date for the order you would like to delete (MM/DD/YYYY): ");
            return removeOrderDate;
        }

        private int GetRemoveOrderNumberInput()
        {
            int orderNumber = UserPrompts.GetIntegerFromUser("Enter Order Number of order to be deleted: ");
            return orderNumber;
        }
    }
}
