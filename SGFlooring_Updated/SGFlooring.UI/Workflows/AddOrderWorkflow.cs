﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.Models;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class AddOrderWorkflow
    {
        Order newOrder = new Order();

        public void Execute()
        {
            var addOrderManager = new OrderManager();
            newOrder = GetAddOrderInput(newOrder);

            Order confirmOrder = addOrderManager.CalculateOrderValues(newOrder);

            DisplayScreens.DisplayOrderDetailsView(confirmOrder, "New Order To Add: ");

            if (UserPrompts.GetYesNoAnswerFromUser("\nDo you want to add this order") == "Y")
            {

                confirmOrder.OrderNumber = addOrderManager.GetNextOrderNumber(confirmOrder.OrderDate);
                var result = addOrderManager.AddOrder(confirmOrder);
                AddOrderConfirmation orderConfirmation = result.Data;
                DisplayScreens.DisplayAddOrderConfirmationView(orderConfirmation);

            }
            else
            {
                Console.WriteLine("\nOrder will be discarded.");
            }

            UserPrompts.PressKeyToContinue();

        }

        private Order GetAddOrderInput(Order newOrder)
        {
            DisplayScreens.AddOrderInputHeader();
            newOrder.OrderDate = UserPrompts.GetDateFromUser("Enter order date (MM/DD/YYYY): ");
            newOrder.CustomerName = UserPrompts.GetStringFromUser("Enter customer name: ");
            newOrder.StateName =  UserPrompts.GetValidStateEntryFromUser("Enter state name(full name or two - letter abbreviation): ");
            newOrder.ProductType = UserPrompts.GetValidProductTypeFromUser("Enter product type: ");
            newOrder.OrderArea = UserPrompts.GetDecimalFromUser("Enter order area in square feet: ");
            return newOrder;
        }
        
    }
}
