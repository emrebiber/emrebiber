﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class MainMenu
    {
        public void Execute()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Welcome to SG Flooring");
                Console.WriteLine("=================================");
                Console.WriteLine("\n1. Display Orders");
                Console.WriteLine("2. Add New Order");
                Console.WriteLine("3. Edit an Order");
                Console.WriteLine("4. Remove an Order");
                Console.WriteLine("\n(Q) to quit");

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                if (input.Substring(0, 1).ToUpper() == "Q")
                {

                    break;

                }
                else
                {

                    ProcessChoice(input);
                    
                }
            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    DisplayOrdersWorkflow displayOrders = new DisplayOrdersWorkflow();
                    displayOrders.Execute();
                    break;
                case "2":
                    AddOrderWorkflow addOrder = new AddOrderWorkflow();
                    addOrder.Execute();
                    break;
                case "3":
                    EditOrderWorkflow editOrder = new EditOrderWorkflow();
                    editOrder.Execute();
                    break;
                case "4":
                    RemoveOrderWorkflow removeOrder = new RemoveOrderWorkflow();
                    removeOrder.Execute();
                    break;
            }
        }
    }
}
