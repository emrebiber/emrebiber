﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.Models;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class EditOrderWorkflow
    {
        OrderManager editOrderManager = new OrderManager();
        const string EndOfLine = "): ";

        public void Execute()
        {
            
            var orderList = editOrderManager.ListOrders(GetEditOrderDateInput());

            if (orderList.Success)
            {

                GetOrderToEdit(orderList.Data);

            }
            else
            {

                Console.WriteLine(orderList.Message);

            }

            UserPrompts.PressKeyToContinue();
        }

        private void GetOrderToEdit(List<Order> orderList)
        {
            bool validOrderNumber = false;

            while (!validOrderNumber)
            {
                DisplayScreens.DisplayOrdersListView(orderList);

                var resultGet = editOrderManager.GetOrder(orderList, GetEditOrderNumberInput());

                if (!resultGet.Success)
                {

                    Console.Clear();
                    Console.WriteLine(resultGet.Message);

                }
                else
                {

                    List<Order> initialInput = GetEditOrderInputFromUser(resultGet.Data);
                    validOrderNumber = true;

                    if (initialInput.Count == 2)
                    {
                        var resultEdit = editOrderManager.EditOrder(initialInput);
                        DisplayScreens.DisplayEditOrderConfirmationView(resultEdit.Data);
                    }

                }
            }
        }

        private List<Order> GetEditOrderInputFromUser(Order orderToEdit)
        {
            DisplayScreens.GetEditOrderInputFromUserHeader();

            List<Order> originalAndUpdatedOrders = new List<Order>();
            Order updatedOrder = new Order();

            updatedOrder.OrderNumber = orderToEdit.OrderNumber;

            updatedOrder.OrderDate = GetEditedDateFromUser(orderToEdit);

            updatedOrder.CustomerName = GetEditedCustomerNameFromUser(orderToEdit);

            updatedOrder.StateName = GetEditedStateNameFromUser(orderToEdit);

            updatedOrder.ProductType = GetEditedProductTypeFromUser(orderToEdit);

            updatedOrder.OrderArea = GetEditedOrderAreaFromUser(orderToEdit);

            Order confirmOrder = editOrderManager.CalculateOrderValues(updatedOrder);

            Console.Clear();
            DisplayScreens.DisplayOrderDetailsView(confirmOrder, "Updated order: ");

            if (UserPrompts.GetYesNoAnswerFromUser("\nDo you want to save this updated order") == "Y")
            {

                originalAndUpdatedOrders.Add(orderToEdit);
                originalAndUpdatedOrders.Add(confirmOrder);

            }
            else
            {
                Console.WriteLine("\nEdits will be discarded");
            }

            return originalAndUpdatedOrders;

        }

        private DateTime GetEditedDateFromUser(Order orderToEdit)
        {
            while (true)
            {
                Console.Write("Enter new order date as MM/DD/YYYY (" + orderToEdit.OrderDate.ToString("d") + EndOfLine);
                string editDateInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(editDateInput))
                {
                    DateTime result = new DateTime();

                    DateTime.TryParse(editDateInput, out result);

                    if (!result.Equals(DateTime.MinValue))
                    {
                        return result;
                    }
                    else
                    {
                        Console.WriteLine("Please enter a valid date...\n");
                    }

                }
                else
                {
                    Console.WriteLine("--Date unchanged--");
                    return orderToEdit.OrderDate;
                }
            }
        }

        private string GetEditedCustomerNameFromUser(Order orderToEdit)
        {
            while (true)
            {
                Console.Write("Enter new customer name (" + orderToEdit.CustomerName.Replace("Z#k@m", ",") + EndOfLine);
                string editCustomerNameInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(editCustomerNameInput))
                {
                    return editCustomerNameInput;

                }
                else
                {
                    Console.WriteLine("--Customer name unchanged--");
                    return orderToEdit.CustomerName;
                }
            }
        }

        private string GetEditedStateNameFromUser(Order orderToEdit)
        {
            while (true)
            {
                Console.Write("Enter new state name [full name or two-letter abbreviation] (" + orderToEdit.StateName + EndOfLine);
                string editStateInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(editStateInput))
                {
                    var stateManager = new StateManager();
                    bool stateInputIsValid = stateManager.IsInputAValidSate(editStateInput);

                    if (!stateInputIsValid)
                    {
                        Console.WriteLine("That is not a valid state. Please re-enter.\n");
                    }
                    else
                    {
                        return editStateInput;
                    }

                }
                else
                {
                    Console.WriteLine("--State unchanged--");
                    return orderToEdit.StateName;
                }
            }
        }

        private string GetEditedProductTypeFromUser(Order orderToEdit)
        {
            while (true)
            {
                Console.Write("Enter new product type (" + orderToEdit.ProductType + EndOfLine);
                string editProductInput = Console.ReadLine();
                if (!string.IsNullOrEmpty(editProductInput))
                {
                    var productManager = new ProductManager();
                    bool productInputIsValid = productManager.IsInputAValidProductType(editProductInput);

                    if (!productInputIsValid)
                    {
                        Console.WriteLine("That is not a valid product type. Please re-enter.\n");
                    }
                    else
                    {
                        return editProductInput;
                    }

                }
                else
                {
                    Console.WriteLine("--Product type unchanged--");
                    return orderToEdit.ProductType;
                }
            }
        }

        private decimal GetEditedOrderAreaFromUser(Order orderToEdit)
        {
            while (true)
            {
                Console.Write("Enter order area in square feet (" + orderToEdit.OrderArea + EndOfLine);
                string editOrderAreaInput = Console.ReadLine();

                if (!string.IsNullOrEmpty(editOrderAreaInput))
                {
                    decimal output;

                    if (decimal.TryParse(editOrderAreaInput, out output))
                    {
                        return output;
                    }

                    Console.WriteLine("Please enter a valid number...\n");

                }
                else
                {
                    Console.WriteLine("--Order area unchanged--");
                    return orderToEdit.OrderArea;
                }

            }
        }

        private int GetEditOrderNumberInput()
        {
            int orderNumber = UserPrompts.GetIntegerFromUser("Enter Order Number of order to be edited: ");
            return orderNumber;
        }

        private DateTime GetEditOrderDateInput()
        {
            DisplayScreens.EditOrderInputHeader();
            DateTime editOrderDate = UserPrompts.GetDateFromUser("Enter date for the order you would like to edit (MM/DD/YYYY): ");
            return editOrderDate;
        }
    }
}
