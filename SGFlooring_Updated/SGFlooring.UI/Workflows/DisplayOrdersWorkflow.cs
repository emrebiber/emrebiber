﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;
using SGFlooring.UI.Utilities;

namespace SGFlooring.UI.Workflows
{
    public class DisplayOrdersWorkflow
    {
        public void Execute()
        {
            DateTime orderDate = UserPrompts.GetDateFromUser("Enter date (MM/DD/YYYY): ");
            var orderManager = new OrderManager();
            var result = orderManager.ListOrders(orderDate);

            if (result.Success)
            {

                DisplayScreens.DisplayOrdersListView(result.Data);

            }
            else
            {

                Console.WriteLine(result.Message);

            }

            UserPrompts.PressKeyToContinue();
            
        }
        
    }
}
