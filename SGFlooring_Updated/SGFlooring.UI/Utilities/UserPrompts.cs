﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.BLL;

namespace SGFlooring.UI.Utilities
{
    public class UserPrompts
    {
        public static int GetIntegerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write("\n" + prompt);
                string input = Console.ReadLine();
                int output;

                if (int.TryParse(input, out output))
                {
                    return output;
                }
                else
                {
                    Console.WriteLine("Please enter a valid number...");
                }
                
            }
            

        }

        public static decimal GetDecimalFromUser(string prompt)
        {
            do
            {
                Console.Write(prompt);
                string input = Console.ReadLine();
                decimal output;

                if (decimal.TryParse(input, out output))
                {
                    return output;
                }

                Console.WriteLine("Please enter a valid number...");
                PressKeyToContinue();

            } while (true);

            
        }

        //public static decimal CHeckDecimalFromUserWhenEditing(string input)
        //{
        //    while (true)
        //    {
        //        decimal output;

        //        if (decimal.TryParse(input, out output))
        //        {
        //            return output;
        //        }

        //        Console.WriteLine("Please enter a valid number...\n");
        //        PressKeyToContinue();

        //    }


        //}

        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.Write(prompt);
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("\nYou must enter valid option. Please try again...");
                    PressKeyToContinue();
                }
                else
                {
                    return input;
                }
            } while (true);



        }

        public static DateTime GetDateFromUser(string prompt)
        {
            while (true)
            {
                Console.Write("\n" + prompt);
                string input = Console.ReadLine();

                DateTime result = new DateTime();

                DateTime.TryParse(input, out result);

                if (!result.Equals(DateTime.MinValue))
                {
                    return result;
                }
                else
                {
                    Console.WriteLine("Please enter a valid date...");
                }
            }

        }

        //public static DateTime CheckDateFromUserWhenEditing(string input)
        //{
        //    while (true)
        //    {

        //        DateTime result = new DateTime();

        //        DateTime.TryParse(input, out result);

        //        if (!result.Equals(DateTime.MinValue))
        //        {
        //            return result;
        //        }
        //        else
        //        {
        //            Console.WriteLine("Please enter a valid date...");
        //        }
        //    }

        //}

        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + "(Y/N)? ");
                string input = Console.ReadLine().ToUpper();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N");
                    PressKeyToContinue();
                }
                else
                {
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N");
                        PressKeyToContinue();
                        continue;
                    }

                    return input;
                }
            }
        }

        public static string GetValidStateEntryFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt);
                string stateInput = Console.ReadLine();
                var stateManager = new StateManager();
                bool validatedStateInput = stateManager.IsInputAValidSate(stateInput);
                if (!validatedStateInput)
                {
                    Console.WriteLine("That is not a valid state. Please re-enter.\n");
                }
                else
                {
                    return stateInput;
                }
            }
        }

        //public static string CheckValidStateEntryFromUserWhenEditing(string input)
        //{
        //    while (true)
        //    {
        //        var stateManager = new StateManager();
        //        bool validatedStateInput = stateManager.IsInputAValidSate(input);
        //        if (!validatedStateInput)
        //        {
        //            Console.WriteLine("That is not a valid state. Please re-enter.\n");
        //        }
        //        else
        //        {
        //            return input;
        //        }
        //    }
        //}

        public static string GetValidProductTypeFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt);
                string productInput = Console.ReadLine();
                var productManager = new ProductManager();
                bool validatedProductInput = productManager.IsInputAValidProductType(productInput);
                if (!validatedProductInput)
                {
                    Console.WriteLine("That is not a valid product type. Please re-enter.\n");
                }
                else
                {
                    return productInput;
                }
            }
        }

        public static void PressKeyToContinue()
        {
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

    }
}
