﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.UI.Utilities
{
    public static class DisplayScreens
    {
        private const string _seperatorBar = "================================================================";
        private const string _headerFormat = "{0, -7} {1, -15} {2, -20} {3, -9} {4, -20}";

        public static void DisplayOrdersListView(List<Order> ordersList)
        {
            Console.WriteLine("\nOrders for {0}: ", ordersList[0].OrderDate.ToString("d"));
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product");
            Console.WriteLine(_seperatorBar);
            foreach (var order in ordersList)
            {
                Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName.Replace("Z#k@m", ","), order.StateName, order.ProductType);
            }
        }

        public static void DisplayOrderDetailsView(Order order, string heading)
        {
            Console.WriteLine("\n" + heading);
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "Order#", "OrderDate", "CustomerName", "State", "Product");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, order.OrderNumber, order.OrderDate.ToString("d"), order.CustomerName.Replace("Z#k@m", ","),
                order.StateName, order.ProductType);
            Console.WriteLine("\n{0} sq. ft. @ {1} per sq. ft. = {2} in materials;", order.OrderArea.ToString(), order.CostPerSquareFoot.ToString("c"), order.MaterialCost.ToString("c"));
            Console.WriteLine("{0} sq. ft. @ {1} per sq. ft. = {2} in labor;", order.OrderArea.ToString(), order.LaborCostPerSquareFoot.ToString("c"), order.LaborCost.ToString("c"));
            Console.WriteLine("\nSub-total of {0} @ {1} % tax = {2} total tax;", (order.MaterialCost + order.LaborCost).ToString("c"), order.TaxRate.ToString(), order.OrderTax.ToString("c"));
            Console.WriteLine("Order Total: {0}", order.OrderTotalCost.ToString("c"));
        }

        public static void DisplayAddOrderConfirmationView(AddOrderConfirmation confirmation)
        {
            Console.WriteLine("\nThe following order has been added: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("{0} - {1} - {2}", confirmation.OrderNumber, confirmation.OrderDate.ToString("d"), confirmation.CustomerName.Replace("Z#k@m", ","));

        }

        public static void DisplayEditOrderConfirmationView(EditOrderConfirmation confirmation)
        {
            Console.WriteLine("\nThe following order has been edited: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, "NewOrd#", "NewOrderDate", "NewCustomerName", "NewState", "NewProduct");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine(_headerFormat, confirmation.NewOrderNumber, confirmation.NewOrderDate.ToString("d"), confirmation.NewOrderCustomerName.Replace("Z#k@m", ","), confirmation.NewOrderState, confirmation.NewOrderProductType);

        }


        public static void DisplayDeleteOrderConfirmationView(RemoveOrderConfirmation orderConfirmation)
        {
            Console.WriteLine("\nThe following order has been deleted: ");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("{0, -7} {1, -15} {2, -20} {3, -9}", "Order#", "OrderDate", "CustomerName", "Date/TimeDeleted");
            Console.WriteLine(_seperatorBar);
            Console.WriteLine("{0, -7} {1, -15} {2, -20} {3, -9}", orderConfirmation.OrderNumber, orderConfirmation.OrderDate.ToString("d"), orderConfirmation.CustomerName.Replace("Z#k@m", ","), orderConfirmation.DateTimeDeleted.ToString("G"));
        }

        public static void AddOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine("Add Order: ");
            Console.WriteLine(_seperatorBar);
        }
        
        public static void RemoveOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine("Remove An Order: ");
            Console.WriteLine(_seperatorBar);
        }

        public static void EditOrderInputHeader()
        {
            Console.Clear();
            Console.WriteLine("Edit An Order: ");
            Console.WriteLine(_seperatorBar);
        }


        public static void GetEditOrderInputFromUserHeader()
        {
            Console.Clear();
            Console.WriteLine("\nEach editable field will display its current value in parentheses. \nEnter changes, or press enter to keep current value: ");
            Console.WriteLine(_seperatorBar);
        }
    }
}
