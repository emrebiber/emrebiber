﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class StateManager
    {

        private IStateRepository _stateRepo;

        public StateManager()
        {
            _stateRepo = StateRepositoryFactory.GetStateRepository();
        }

        public bool IsInputAValidSate(string stateInput)
        {
            bool result = false;
            var states = _stateRepo.ListStates();
            foreach (var state in states)
            {
                if (stateInput == state.StateAbbreviation)
                {
                    result = true;
                    
                }
                else if (stateInput == state.StateName)
                {
                    result = true;
                    
                }
                
            }

            return result;
        }

        public State GetState(string stateName)
        {
            State matchedState = new State();
            var states = _stateRepo.ListStates();

            foreach (var state in states)
            {
                if (stateName == state.StateName)
                {
                    matchedState = state;
                }
                if (stateName == state.StateAbbreviation)
                {
                    matchedState = state;
                }
            }

            return matchedState;
        }
    }
}
