﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class OrderManager
    {

        private IOrderRepository _repo;

        public OrderManager()
        {
            _repo = OrderRepositoryFactory.GetOrderRepository();
        }


        public Response<List<Order>> ListOrders(DateTime orderDate)
        {
            var result = new Response<List<Order>>();
            var orders = _repo.ListOrders(orderDate);

            try
            {
                if (orders.Count == 0)
                {
                    result.Success = false;
                    result.Message = "Cannot display orders. There are no order records associated with this date.\n";
                    ErrorManager errorManager = new ErrorManager();
                    var error = errorManager.ProcesseDeveloperImplementedException("No records found",
                        "No records found for date " + orderDate.ToString("d"));
                    errorManager.WriteToErrorLog(error);
                }

                else

                {
                    result.Data = orders;
                    result.Success = true;
                }
            }
            catch (Exception ex)
            {
                ErrorManager errorManager = new ErrorManager();
                var error = errorManager.ProcessExceptionObject(ex);
                errorManager.WriteToErrorLog(error);

                result.Success = false;
                result.Message = "The system was unable to list records. Please try again later.";
            }

            return result;

        }

        public Response<Order> GetOrder(List<Order> ordersList, int orderNumber)
        {
            var result = new Response<Order>();
            
            try
            {

                var order = _repo.GetOrder(ordersList, orderNumber);

                if (order == null)
                {
                    result.Success = false;
                    result.Message = "\nUnable to retrieve record. Be sure to enter a valid order number.";
                }
                else
                {
                    result.Data = order;
                    result.Success = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                ErrorManager errorManager = new ErrorManager();
                var error = errorManager.ProcessExceptionObject(ex);
                errorManager.WriteToErrorLog(error);

            }

            return result;
        }

        public Response<AddOrderConfirmation> AddOrder(Order newOrder)
        {

            var result = new Response<AddOrderConfirmation>();

            _repo.AddOrder(newOrder);

            result.Success = true;
            result.Data = new AddOrderConfirmation()
            {
                OrderNumber = newOrder.OrderNumber,
                OrderDate = newOrder.OrderDate,
                CustomerName = newOrder.CustomerName
            };

            return result;


        }

        public Response<EditOrderConfirmation> EditOrder(List<Order> ordersToEditList)
        {
            EditOrderConfirmation confirmation = new EditOrderConfirmation();
            var result = new Response<EditOrderConfirmation>();

            try
            {
                Order originalOrder = ordersToEditList[0];
                Order updatedOrder = ordersToEditList[1];

                if (originalOrder.OrderDate != updatedOrder.OrderDate)
                {
                    updatedOrder.OrderNumber = GetNextOrderNumber(updatedOrder.OrderDate);
                }

                ordersToEditList[0] = originalOrder;
                ordersToEditList[1] = updatedOrder;

                _repo.EditOrder(ordersToEditList);

                confirmation.EditedOrderDate = originalOrder.OrderDate;
                confirmation.EditedOrderNumber = originalOrder.OrderNumber;
                confirmation.NewOrderDate = updatedOrder.OrderDate;
                confirmation.NewOrderNumber = updatedOrder.OrderNumber;
                confirmation.NewOrderCustomerName = updatedOrder.CustomerName;
                confirmation.NewOrderState = updatedOrder.StateName;
                confirmation.NewOrderProductType = updatedOrder.ProductType;
                result.Data = confirmation;
                result.Success = true;

            }
            catch (Exception ex)
            {
                ErrorManager errorManager = new ErrorManager();
                var error = errorManager.ProcessExceptionObject(ex);
                errorManager.WriteToErrorLog(error);

                result.Success = false;
                result.Message = "The system was unable to edit the record. Please try again later.";
            }

            return result;
        }

        public Response<RemoveOrderConfirmation> RemoveOrder(Order orderToDelete)
        {
            RemoveOrderConfirmation confirmation = new RemoveOrderConfirmation();
            var result = new Response<RemoveOrderConfirmation>();

            try
            {

                _repo.RemoveOrder(orderToDelete);

                confirmation.OrderNumber = orderToDelete.OrderNumber;
                confirmation.OrderDate = orderToDelete.OrderDate;
                confirmation.CustomerName = orderToDelete.CustomerName;
                confirmation.DateTimeDeleted = DateTime.Now;

                result.Success = true;
                result.Data = confirmation;

                WriteToDeletedOrdersTextFile(confirmation);
            }
            catch (Exception ex)
            {
                ErrorManager errorManager = new ErrorManager();
                var error = errorManager.ProcessExceptionObject(ex);
                errorManager.WriteToErrorLog(error);

                result.Success = false;
                result.Message = "The system was unable to delete the record. Please try again later.";
            }

            return result;
        }
        
        public Order CalculateOrderValues(Order newOrder)
        {
            var stateManager = new StateManager();
            var productManager = new ProductManager();

            var state = stateManager.GetState(newOrder.StateName);

            newOrder.StateName = state.StateAbbreviation;
            newOrder.TaxRate = state.TaxRate;

            var product = productManager.GetProduct(newOrder.ProductType);

            newOrder.CostPerSquareFoot = product.CostPerSquareFoot;
            newOrder.LaborCostPerSquareFoot = product.LaborCostPerSquareFoot;

            newOrder.MaterialCost = newOrder.CostPerSquareFoot * newOrder.OrderArea;
            newOrder.LaborCost = newOrder.LaborCostPerSquareFoot * newOrder.OrderArea;
            decimal orderSubTotal = newOrder.MaterialCost + newOrder.LaborCost;
            newOrder.OrderTax = orderSubTotal * (newOrder.TaxRate / 100);
            newOrder.OrderTotalCost = orderSubTotal + newOrder.OrderTax;

            return newOrder;
        }

        public int GetNextOrderNumber(DateTime newOrderDate)
        {

            List<Order> orders = _repo.ListOrders(newOrderDate);

            if (orders.Count == 0)
            {
                return 1;
            }
            else
            {
                var num = orders.Max(o => o.OrderNumber) + 1;
                return num;
                //var orderNums = orders.OrderBy(o => o.OrderNumber).Select(o => o.OrderNumber);
                //return Enumerable.Range(1, orderNums.Max() + 1).Except(orderNums).First();
            }

        }

        private void WriteToDeletedOrdersTextFile(RemoveOrderConfirmation confirmation)
        {
            string _filePath = @"DataFiles\DeletedOrdersLog.txt";

            if (!File.Exists(_filePath))
            {
                var newFile = File.Create(_filePath);
                using (StreamWriter headingsWriter = new StreamWriter(newFile))
                {
                    headingsWriter.WriteLine("OrderDate|OrderNumber|CustomerName|DateTimeDeleted");
                }
            }

            string formattedRemoveOrderConfirmation = FormatRemoveOrderConfirmation(confirmation);

            using (StreamWriter writer = File.AppendText(_filePath))
            {
                writer.WriteLine(formattedRemoveOrderConfirmation);
            }
            
        }

        private string FormatRemoveOrderConfirmation(RemoveOrderConfirmation confirmation)
        {
            string result = confirmation.OrderDate.ToString("d") + "|" + confirmation.OrderNumber + "|" + confirmation.CustomerName + "|" +
                            confirmation.DateTimeDeleted.ToString("G");
            return result;
        }
    }
}
