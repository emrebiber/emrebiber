﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class ProductManager
    {

        private IProductRepository _productRepo;

        public ProductManager()
        {
            _productRepo = ProductRepositoryFactory.GetProductRepository();
        }

        public bool IsInputAValidProductType(string productInput)
        {
            bool result = false;
            var products = _productRepo.ListProducts();
            foreach (var prod in products)
            {
                if (productInput == prod.ProductType)
                {
                    result = true;
                }
                
            }

            return result;
        }

        public Product GetProduct(string productType)
        {
            Product matchedProduct = new Product();
            var products = _productRepo.ListProducts();

            foreach (var prod in products)
            {
                if (productType == prod.ProductType)
                {
                    matchedProduct = prod;
                }
                
            }

            return matchedProduct;
        }
    }
}
