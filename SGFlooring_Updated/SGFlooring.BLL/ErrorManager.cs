﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.BLL
{
    public class ErrorManager
    {

        private string _filePath = @"DataFiles\errorlog.txt";

        public void WriteToErrorLog(Error error)
        {
            if (!File.Exists(_filePath))
            {
                var newFile = File.Create(_filePath);
                using (StreamWriter headingsWriter = new StreamWriter(newFile))
                {
                    headingsWriter.WriteLine("ErrorDateTime|ErrorName|ErrorMessage|ErrorSource|ErrorStackTrace");
                }
            }

            string formattedMessage = FormatErrorMessage(error);

            using (StreamWriter writer = File.AppendText(_filePath))
            {
                writer.WriteLine(formattedMessage);
            }
        }

        private string FormatErrorMessage(Error error)
        {
            string result = error.ErrorDateTime.ToString("G") + "|" + error.ErrorName + "|" + error.ErrorMessage + "|" +
                            error.ErrorSource + "|" + error.StackTrace;
            return result;
        }

        public Error ProcessExceptionObject(Exception ex)
        {
            Error newError = new Error();

            newError.ErrorName = ex.GetType().ToString();
            newError.ErrorMessage = ex.Message;
            newError.ErrorSource = ex.Source;
            newError.StackTrace = ex.StackTrace;
            newError.ErrorDateTime = DateTime.Now;

            return newError;
            
        }

        public Error ProcesseDeveloperImplementedException(string errorName, string errorMessage)
        {
            Error newError = new Error();

            newError.ErrorName = errorName;
            newError.ErrorMessage = errorMessage;
            newError.ErrorSource = "No source associated with this error message";
            newError.StackTrace = "No stack trace associated with this error message";
            newError.ErrorDateTime = DateTime.Now;

            return newError;
        }
    }
}
