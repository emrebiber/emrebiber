﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Data.Repositories;
using SGFlooring.Models;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class OrderFileRepositoryTests
    {
        private OrderFileRepository _repo;

        public OrderFileRepositoryTests()
        {
            _repo = new OrderFileRepository();
        }

        [TestCase(15)]
        public void CanListOrderFilesInDirectory(int totalTxtFiles)
        {
            var txtFiles = _repo.ListOrderFilesInDirectory();
            Assert.AreEqual(txtFiles.Count, totalTxtFiles);
        }

        [Test]
        public void CanListAllOrders()
        {
            var orders = _repo.ListOrders(DateTime.Parse("01/15/2016"));
            var orders2 = _repo.ListOrders(DateTime.Parse("08/26/2016"));

            Assert.AreEqual(3, orders.Count);
            Assert.AreEqual(2, orders2.Count);
        }

        [Test]
        public void CanConvertDateToFileValue()
        {
            DateTime testDate1 = new DateTime(2016, 01, 15);
            DateTime testDate2 = new DateTime(2016, 01, 02);
            DateTime testDate3 = new DateTime(2008, 04, 04);

            string orderFile1 = _repo.ConvertDateToFileValue(testDate1);
            string orderFile2 = _repo.ConvertDateToFileValue(testDate2);
            string orderFile3 = _repo.ConvertDateToFileValue(testDate3);
            Assert.AreEqual(orderFile1, "Orders_01152016.txt");
            Assert.AreEqual(orderFile2, "Orders_01022016.txt");
            Assert.AreEqual(orderFile3, "Orders_04042008.txt");
        }

        [Test]
        public void CanGetSpecificOrderWithGetMethod()
        {
            DateTime testDate1 = new DateTime(2016, 01, 15);
            DateTime testDate2 = new DateTime(2016, 01, 06);
            DateTime testDate3 = new DateTime(2016, 08, 28);

            List<Order> testOrderList1 = _repo.ListOrders(testDate1);
            List<Order> testOrderList2 = _repo.ListOrders(testDate2);
            List<Order> testOrderList3 = _repo.ListOrders(testDate3);

            Order newOrder1 = _repo.GetOrder(testOrderList1, 1);
            Order newOrder2 = _repo.GetOrder(testOrderList2, 11);
            Order newOrder3 = _repo.GetOrder(testOrderList3, 3);

            Assert.AreEqual(newOrder1.OrderNumber, 1);
            Assert.AreEqual(newOrder2.OrderNumber, 11);
            Assert.AreEqual(newOrder3.OrderNumber, 3);

        }

        //[TestCase("Bill mcLovin", 33.99, 20.190060)]
        [TestCase("Lyle Lovett", 50.00, 25.00)]
        public void MoreCanGetSpecificOrderWithGetMethod(string customerName, decimal orderArea, 
            decimal orderTax)
        {
            var testOrderList = _repo.ListOrders(new DateTime(2016, 08, 29));
            var testOrder = _repo.GetOrder(testOrderList, 2);
            Assert.AreEqual(testOrder.CustomerName, customerName);
            Assert.AreEqual(testOrder.OrderArea, orderArea);
            Assert.AreEqual(testOrder.OrderTax, orderTax);
        }

        [TestCase("manZ#k@m monkey", 23.33, 6.25)]
        public void CanAddOrder(string customerName, decimal orderArea,
            decimal taxRate)
        {
            Order testOrder = new Order();
            testOrder.OrderNumber = 4;
            testOrder.OrderDate = new DateTime(2016, 08, 29);
            testOrder.CustomerName = "man, monkey";
            testOrder.OrderArea = 23.33M;
            testOrder.StateName = "Ohio";
            testOrder.TaxRate = 6.25M;
            _repo.AddOrder(testOrder);
            var testOrderList = _repo.ListOrders(new DateTime(2016, 08, 29));
            var checkTestOrder = _repo.GetOrder(testOrderList, 4);
            Assert.AreEqual(checkTestOrder.CustomerName, customerName);
            Assert.AreEqual(checkTestOrder.OrderArea, orderArea);
            Assert.AreEqual(checkTestOrder.TaxRate, taxRate);
        }

        [TestCase(3)]
        public void CanRemoveOrder(int totalRecords)
        {
            Order testOrder = new Order();
            testOrder.OrderNumber = 4;
            testOrder.OrderDate = new DateTime(2016, 08, 29);
            _repo.RemoveOrder(testOrder);
            var testOrderList = _repo.ListOrders(new DateTime(2016, 08, 29));
            Assert.AreEqual(testOrderList.Count, totalRecords);
        }
    }
}
