﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Repositories;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class ProductRepositoryTests
    {

        [Test]
        public void CanListProducts()
        {
            var repo = new MockProductRepository();
            var products = repo.ListProducts();
            Assert.AreEqual(3, products.Count);
        }

        [TestCase("Shag Carpet",1.85, 2.50)]
        public void CanGetSpecificProduct(string productType, decimal costPerSquareFoot, decimal laborCostPerSquareFoot)
        {
            var repo = new MockProductRepository();
            var product = repo.GetProduct("Shag Carpet");
            Assert.AreEqual(product.ProductType, productType);
            Assert.AreEqual(product.CostPerSquareFoot, costPerSquareFoot);
            Assert.AreEqual(product.LaborCostPerSquareFoot, laborCostPerSquareFoot);
        }

        [TestCase("Carpet", 2.25, 2.10)]
        public void CanGetSpecificProductFromFile(string productType, decimal costPerSquareFoot,
            decimal laborCostPerSquareFoot)
        {
            var repo = new ProductFileRepository();
            var product = repo.GetProduct("Carpet");
            Assert.AreEqual(product.ProductType, productType);
            Assert.AreEqual(product.CostPerSquareFoot, costPerSquareFoot);
            Assert.AreEqual(product.LaborCostPerSquareFoot, laborCostPerSquareFoot);
        }
    }
}
