﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Repositories;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class StateRepositoryTests
    {

        [Test]
        public void CanListStates()
        {
            var repo = new MockStateRepository();
            var states = repo.ListStates();
            Assert.AreEqual(3, states.Count);

        }

        [TestCase("TX", "Texas", 6.25)]
        public void CanGetSpecificState(string stateAbbreviation, string stateName, decimal taxRate)
        {
            var repo = new MockStateRepository();
            var state = repo.GetState("TX");
            Assert.AreEqual(state.StateAbbreviation, stateAbbreviation);
            Assert.AreEqual(state.StateName, stateName);
            Assert.AreEqual(state.TaxRate, taxRate);
        }

        [TestCase("IN", "Indiana", 6.00)]
        public void CanGetSpecificStateFromFile(string stateAbbreviation, string stateName, decimal taxRate)
        {
            var repo = new StateFileRepository();
            var state = repo.GetState("IN");
            Assert.AreEqual(state.StateAbbreviation, stateAbbreviation);
            Assert.AreEqual(state.StateName, stateName);
            Assert.AreEqual(state.TaxRate, taxRate);
        }
    }
}
