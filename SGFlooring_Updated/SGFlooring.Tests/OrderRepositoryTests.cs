﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGFlooring.Data.Factories;
using SGFlooring.Data.Interfaces;
using SGFlooring.Data.Repositories;
using SGFlooring.Models;

namespace SGFlooring.Tests
{
    [TestFixture]
    public class OrderRepositoryTests
    {
        private IOrderRepository repo;

        public OrderRepositoryTests()
        {
            repo = OrderRepositoryFactory.GetOrderRepository();
        }

        [SetUp]
        public void Setup()
        {
            List<Order> newOrders = new List<Order>()
            {
                new Order()
                {
                    OrderNumber = 1,
                    OrderDate = DateTime.Parse("01/05/2016"),
                    CustomerName = "Dave Balzer",
                    StateName = "Ohio",
                    TaxRate = 0.0575M,
                    ProductType = "Shag Carpet",
                    OrderArea = 22.5M,
                    CostPerSquareFoot = 1.85M,
                    LaborCostPerSquareFoot = 2.50M,
                    MaterialCost = 41.63M,
                    LaborCost = 56.25M,
                    OrderTax = 5.63M,
                    OrderTotalCost = 103.51M
                },
                new Order()
                {
                    OrderNumber = 2,
                    OrderDate = DateTime.Parse("01/06/2016"),
                    CustomerName = "Willie Nelson",
                    StateName = "Texas",
                    TaxRate = 0.0625M,
                    ProductType = "Mexican Ceramic Tile",
                    OrderArea = 50.00M,
                    CostPerSquareFoot = 4.25M,
                    LaborCostPerSquareFoot = 3.75M,
                    MaterialCost = 212.50M,
                    LaborCost = 187.50M,
                    OrderTax = 25.00M,
                    OrderTotalCost = 425.00M
                },
                new Order()
                {
                    OrderNumber = 3,
                    OrderDate = DateTime.Parse("01/07/2016"),
                    CustomerName = "Bear Bryant",
                    StateName = "Alabama",
                    TaxRate = 0.04M,
                    ProductType = "Wood Laminate",
                    OrderArea = 12.25M,
                    CostPerSquareFoot = 2.25M,
                    LaborCostPerSquareFoot = 3.50M,
                    MaterialCost = 27.56M,
                    LaborCost = 42.88M,
                    OrderTax = 2.82M,
                    OrderTotalCost = 73.26M
                }
            };

            var currentOrders = repo.ListOrders(DateTime.Parse("01/01/2016"));
            //List<int> numbers = new List<int>();
            foreach (var o in currentOrders)
            {
                repo.RemoveOrder(o);
            }
            foreach (var o in newOrders)
            {
                repo.AddOrder(o);
            }
        }


        [Test]
        public void CanListAllOrders()
        {
            var orders = repo.ListOrders(DateTime.Parse("01/01/2016"));

            Assert.AreEqual(3, orders.Count);
        }

        //[Test]
        //public void CanRemoveSpecificOrder()
        //{
        //    repo.RemoveOrder(*need Order object*);
        //    var deletedRecord = repo.GetOrder(repo.ListOrders(DateTime.Parse("01/01/2016")), 1);
        //    var orders = repo.ListOrders(DateTime.Parse("01/01/2016"));
        //    Assert.IsNull(deletedRecord);

        //}

        [TestCase(1, "Dave Balzer", 22.5, 103.51)]
        [TestCase(3, "Bear Bryant", 12.25, 73.26)]
        public void CanGetSpecificOrder(int orderNumber, string customerName, decimal orderArea,
            decimal orderTotalCost)
        {
            var order = repo.GetOrder(repo.ListOrders(DateTime.Parse("01/01/2016")), orderNumber);
            Assert.AreEqual(customerName, order.CustomerName);
            Assert.AreEqual(orderArea, order.OrderArea);
            Assert.AreEqual(orderTotalCost, order.OrderTotalCost);
        }

        [TestCase(4, "Keith Manecke", 14.33, 3.22)]
        public void CanAddSpecificOrder(int orderNumber, string customerName, decimal orderArea,
            decimal laborCostPerSquareFoot)
        {
            Order newOrder = new Order();
            newOrder.OrderNumber = orderNumber;
            newOrder.CustomerName = customerName;
            newOrder.OrderArea = orderArea;
            newOrder.LaborCostPerSquareFoot = laborCostPerSquareFoot;
            repo.AddOrder(newOrder);
            var orders = repo.ListOrders(DateTime.Parse("01/01/2016"));
            var addedOrder = repo.GetOrder(repo.ListOrders(DateTime.Parse("01/01/2016")), 4);
            Assert.AreEqual(4, orders.Count);
            Assert.AreEqual(customerName, addedOrder.CustomerName);
            Assert.AreEqual(orderArea, addedOrder.OrderArea);
            Assert.AreEqual(laborCostPerSquareFoot, addedOrder.LaborCostPerSquareFoot);
        }

        [TestCase(3, "Jimmy Jam", 3.33, 25.33)]
        public void CanEditSpecificOrder(int orderNumber, string customerName, decimal orderArea,
            decimal materialCost)
        {
            Order editedOrder = repo.GetOrder(repo.ListOrders(DateTime.Parse("01/01/2016")), 3);
            var currentOrders = repo.ListOrders(DateTime.Parse("01/01/2016"));
            editedOrder.CustomerName = customerName;
            editedOrder.OrderArea = orderArea;
            editedOrder.MaterialCost = materialCost;
            repo.EditOrder(currentOrders);
            Order updatedOrder = repo.GetOrder(repo.ListOrders(DateTime.Parse("01/01/2016")), 3);
            Assert.AreEqual(orderNumber, updatedOrder.OrderNumber);
            Assert.AreEqual(customerName, updatedOrder.CustomerName);
            Assert.AreEqual(orderArea, updatedOrder.OrderArea);
            Assert.AreEqual(materialCost, updatedOrder.MaterialCost);
        }


    }
}
