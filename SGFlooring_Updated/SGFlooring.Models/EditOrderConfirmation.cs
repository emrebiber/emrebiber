﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGFlooring.Models
{
    public class EditOrderConfirmation
    {
        public int EditedOrderNumber { get; set; }
        public DateTime EditedOrderDate { get; set; }
        public int NewOrderNumber { get; set; }
        public DateTime NewOrderDate { get; set; }
        public string NewOrderCustomerName { get; set; }
        public string NewOrderState { get; set; }
        public string NewOrderProductType { get; set; }
    }
}
