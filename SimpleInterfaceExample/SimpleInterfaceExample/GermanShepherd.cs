﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaceExample
{
    public class GermanShepherd : IDog
    {
        public void Bark()
        {
            Console.WriteLine("WOOF!");
        }

        public string GoForAWalk()
        {
            return "Go for a long walk!";
        }
    }
}
