﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SimpleInterfaceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            GermanShepherd bigDog = new GermanShepherd();
            WeinerDoodle smallDog = new WeinerDoodle();

            GiveTreat(bigDog);
            GiveTreat(smallDog);
            TakeOutside(bigDog);
            TakeOutside(smallDog);

            Console.ReadLine();
        }

        static void GiveTreat(IDog dog)
        {
            dog.Bark();
        }

        static void TakeOutside(IDog dog)
        {
            Console.WriteLine(dog.GoForAWalk());
        }
    }
}
