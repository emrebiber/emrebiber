﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaceExample
{
    public class WeinerDoodle : IDog
    {
        public void Bark()
        {
            Console.WriteLine("Yip");
        }

        public string GoForAWalk()
        {
            return "Go for a short walk";
        }
    }
}
