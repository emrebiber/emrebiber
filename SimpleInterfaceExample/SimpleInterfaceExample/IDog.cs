﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterfaceExample
{
    public interface IDog
    {
        void Bark();
        string GoForAWalk();
    }
}
