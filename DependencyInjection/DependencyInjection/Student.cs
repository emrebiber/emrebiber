﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public class Student : ITestInterface
    {
        public void WriteSomething(string print)
        {
            Console.WriteLine(print);
            Console.WriteLine("Comes from STUDENT Class");
        }
    }
}
