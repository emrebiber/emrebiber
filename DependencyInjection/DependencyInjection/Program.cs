﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person(new Student());
            Person person2 = new Person(new Employee());

            Console.ReadLine();

        }
    }
}
