﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public class Person
    {
        public Person(ITestInterface test)
        {
            test.WriteSomething("New Person Created!");
        }
    }
}
