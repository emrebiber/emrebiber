﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjection
{
    public class Employee : ITestInterface
    {
        public void WriteSomething(string print)
        {
            Console.WriteLine("**************\n {0} \n**************", print);
            Console.WriteLine("Comes from EMPLOYEE Class");
        }
    }
}
