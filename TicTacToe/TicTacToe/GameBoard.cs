﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class GameBoard
    {
       public static  char[] arr = { '0','1', '2', '3', '4', '5', '6', '7', '8', '9' };

        public static void DrawBoard()
        {
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[1], arr[2], arr[3]);
            Console.WriteLine("_____|_____|_____ ");
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[4], arr[5], arr[6]);
            Console.WriteLine("_____|_____|_____ ");
            Console.WriteLine("     |     |      ");
            Console.WriteLine("  {0}  |  {1}  |  {2}", arr[7], arr[8], arr[9]);
            Console.WriteLine("     |     |      ");
        }
        

        public static int CheckWin()
        {
            //Winning by 1st row
            if (arr[1] == arr[2] && arr[2] == arr[3])
            {
                return 1;
            }
                //Winning by 2nd row
            else if (arr[4] == arr[5] && arr[5] == arr[6])
            {
                return 1;
            }
            //Winning by 3rd row
            else if (arr[7] == arr[8] && arr[8] == arr[9])
            {
                return 1;
            }
            // Winning by 1st column
            else if (arr[1] == arr[4] && arr[4] == arr[7])
            {
                return 1;
            }
            // Winning by 2st column
            else if (arr[2] == arr[5] && arr[5] == arr[8])
            {
                return 1;
            }
            // Winning by 3rd column
            else if (arr[3] == arr[6] && arr[6] == arr[9])
            {
                return 1;
            }
            //Winning by diagonal
            else if (arr[3] == arr[5] && arr[5] == arr[7])
            {
                return 1;
            }
            else if (arr[1] == arr[5] && arr[5] == arr[9])
            {
                return 1;
            }
            else if (arr[1] != '1' && arr[2] != '2'  && arr[3] != '3'  && arr[4] != '4'  && arr[5] != '5'  && arr[6] != '6'  && arr[7] != '7'  && arr[8] != '8'  && arr[9] != '9')
            {
                return -1;
            }
            else
            {
                return 0;
            }
            
        }
        

    }
}
