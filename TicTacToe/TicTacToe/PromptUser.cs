﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class PromptUser
    {
        public static string PromptUserForString(string prompt)
        {
            do
            {
                Console.WriteLine(prompt);
                string userValue = Console.ReadLine();

                if (!string.IsNullOrWhiteSpace(userValue))
                {
                    return userValue;
                }

                Console.WriteLine("You must enter string! Try Again..");

            } while (true);
        }

        public static string GetUserInput(string validPattern = null)
        {
            string input = Console.ReadLine();
            input = input.Trim();

            if (validPattern != null && !System.Text.RegularExpressions.Regex.IsMatch(input, validPattern))
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("\"" + input + "\" is not valid.\n");
                Console.ResetColor();
                return null;
            }

            return input;
        }

        public static void PromptKeyToContinue()
        {
            Console.WriteLine("\n\nPress any key to contiue...");
            Console.ReadKey();
        }

    }
}
