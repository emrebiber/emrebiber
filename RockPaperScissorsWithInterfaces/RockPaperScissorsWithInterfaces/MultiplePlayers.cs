﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsWithInterfaces
{
    public class MultiplePlayers : IMultiPlayerSelector
    {
        public Choice GetMultiPlayer()
        {
            return Choice.Paper;
        }
    }
}
