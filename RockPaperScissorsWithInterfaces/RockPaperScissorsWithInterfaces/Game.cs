﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.XPath;
using Microsoft.Win32;

namespace RockPaperScissorsWithInterfaces
{
    public class Game
    {
        private IChoiceSelector picker;
        private IMultiPlayerSelector _multi;

        public Game(IChoiceSelector choiceSelector)
        {
            picker = choiceSelector;
            
        }

        public Game(IMultiPlayerSelector multiPlayerSelector)
        {
            _multi = multiPlayerSelector;
        }

        public MatchResult PlayRound(Choice userChoice)
        {
            var result = new MatchResult();
            result.UserChoice = userChoice;
            //WeightedChoicePicker picker = new WeightedChoicePicker();
            result.OpponentChoice = picker.GetOpponentChoice();

            if (result.OpponentChoice == result.UserChoice)
            {
                result.Result = GameResult.Tie;
                return result;
            }

            if (result.OpponentChoice == Choice.Rock && result.UserChoice == Choice.Scissors ||
                result.OpponentChoice == Choice.Scissors && result.UserChoice == Choice.Paper ||
                result.OpponentChoice == Choice.Paper && result.UserChoice == Choice.Rock)
            {
                result.Result = GameResult.Lose;
                return result;
            }
                
            result.Result = GameResult.Win;
            return result;
        }

        public MatchResult MultiPlayer(Choice userChoice, Choice otherUserChoice)
        {
            MatchResult result = new MatchResult();
            result.UserChoice = userChoice;
            result.OtherUserChoice = otherUserChoice;

            if (result.UserChoice == result.OtherUserChoice)
            {
                result.Result = GameResult.Tie;
                return result;
            
            }

            if (result.UserChoice == Choice.Rock && result.OtherUserChoice == Choice.Scissors ||
                result.UserChoice == Choice.Scissors && result.OtherUserChoice == Choice.Paper ||
                result.UserChoice == Choice.Paper && result.OtherUserChoice == Choice.Rock)
            {
                result.Result = GameResult.Win;
                return result;
            }

            result.Result = GameResult.Lose;
            return result;



        }

   
        
        
    }
}
