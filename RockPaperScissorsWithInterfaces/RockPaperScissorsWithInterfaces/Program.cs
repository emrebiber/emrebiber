﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsWithInterfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            string input;
            do
            {
                Console.WriteLine("Select your mode: (E)asy, (N)ormal, (T)wo Player");
                input = Console.ReadLine();

                if (input.ToUpper() == "E")
                    PlayAgainstComputerEasyMode();

                else if (input.ToUpper() == "N")
                    PlayAgainstComputerNormalMode();
                else
                    PlayMultiplayer();

                Console.WriteLine("Press Q to quit: ");
                input = Console.ReadLine();

            } while (input.ToUpper() != "Q");
           
        }

        public static void ProcessResult(MatchResult result)
        {
            Console.WriteLine("You picked {0}, your opponent picked {1}",
                Enum.GetName(typeof(Choice), result.UserChoice),
                Enum.GetName(typeof(Choice), result.OpponentChoice));
              //  Enum.GetName(typeof(Choice), result.OtherUserChoice));

            switch (result.Result)
            {
                case GameResult.Tie:
                    Console.WriteLine("You tied!");
                    break;
                case GameResult.Lose:
                    Console.WriteLine("You lose!");
                    break;
                default:
                    Console.WriteLine("You Won!");
                    break;
            } 
        }

        public static Choice GetUserChoice()
        {
            Console.Clear();
            Console.WriteLine("Player : Enter a choice (R)ock, (P)aper, (S)cissors");
            string input = Console.ReadLine();

            switch (input.ToUpper())
            {
                case "P":
                    return Choice.Paper;
                case "S":
                    return Choice.Scissors;
                default:
                    return Choice.Rock;
            }
        }

        public static void PlayAgainstComputerEasyMode()
        {
            Game game = new Game(new WeightedChoicePicker());
            Choice userChoice = GetUserChoice();
            var result = game.PlayRound(userChoice);
            ProcessResult(result);
        }

        public static void PlayAgainstComputerNormalMode()
        {
            Game game = new Game(new RandomChoicePicker());
            Choice userChoice = GetUserChoice();
            var result = game.PlayRound(userChoice);
            ProcessResult(result);
        }

        public static void PlayMultiplayer()
        {
            Game game = new Game(new MultiplePlayers());
            Choice userChoice = GetUserChoice();
            Choice otherUserChoice = GetUserChoice();
            var result = game.MultiPlayer(userChoice, otherUserChoice);
            ProcessResult(result);
        }

        
    }
}
