﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace Warmups.Tests
{
    [TestFixture]
    class LogicTests
    {
        //Exercise 1
        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void GreatParty(int cigars, bool isWeekend,bool result)
        {
            Logic log = new Logic();
            bool actual = log.GreatParty(cigars, isWeekend);
            Assert.AreEqual(result,actual);


        }

        //Exercise 2
        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5, 5, 1)]
        public void CanHazTable(int yourStyle, int dateStyle, int result)
        {
            Logic log = new Logic();
            int actual = log.CanHazTable(yourStyle, dateStyle);
            Assert.AreEqual(result,actual);

        }

        //Exercise 3
        [TestCase(70, false, true)]
        [TestCase(95, false, false)]
        [TestCase(95, true, true)]
        public void PlayOutside(int temp, bool isSummer, bool result)
        {
            Logic log = new Logic();
            bool actual = log.PlayOutside(temp, isSummer);
            Assert.AreEqual(result, actual);
        }

        //Exercise 4
        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeeding(int speed, bool isBirthday, int  result)
        {
            Logic log = new Logic();
            int actual = log.CaughtSpeeding(speed, isBirthday);
            Assert.AreEqual(result,actual);
        }

        //Exercise 5
        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void SkipSum(int a, int b, int result)
        {
            Logic log = new Logic();
            int actual = log.SkipSum(a, b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 6
        [TestCase(1, false, "7:00")]
        [TestCase(5, false, "7:00")]
        [TestCase(0, false, "10:00")]
        public void AlarmClock(int day, bool vacation, string result)
        {
            Logic log = new Logic();
            string actual = log.AlarmClock(day, vacation);
            Assert.AreEqual(result,actual);
        }

        //Exercise 7
        [TestCase(6, 4, true)]
        [TestCase(4, 5, false)]
        [TestCase(1, 5, true)]
        public void LoveSix(int a, int b, bool result)
        {
            Logic log = new Logic();
            bool actual = log.LoveSix(a, b);
            Assert.AreEqual(result,actual);
        }

        //Exercise 8
        [TestCase(5, false, true)]
        [TestCase(11, false, false)]
        [TestCase(11, true, true)]
        public void InRange(int n, bool outsideMode, bool result)
        {
            Logic log = new Logic();
            bool actual = log.InRange(n, outsideMode);
            Assert.AreEqual(result, actual);
        }

        //Exercise 9
        [TestCase(22,true)]
        [TestCase(23, true)]
        [TestCase(24, false)]
        public void SpecialEleven(int n,bool result)
        {
            Logic log = new Logic();
            bool actual = log.SpecialEleven(n);
            Assert.AreEqual(result, actual);
        }

        //Exercise 10
        [TestCase(20,false)]
        [TestCase(21, true)]
        [TestCase(22, true)]
        public void Mod20(int n, bool result)
        {
            Logic log = new Logic();
            bool actual = log.Mod20(n);
            Assert.AreEqual(result, actual);
        } 
        
        //Exercise 11
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(15, true)]
        public void Mod35(int n,bool result)
        {
            Logic log = new Logic();
            bool actual = log.Mod35(n);
            Assert.AreEqual(result, actual);
        }

        //Exercise 12
        [TestCase(false,false,false,true)]
        [TestCase(false, false, true, false)]
        [TestCase(true, false, false, false)]
        public void AnswerCell(bool isMorning, bool isMom, bool isAsleep, bool result)
        {
            Logic log = new Logic();
            bool actual = log.AnswerCell(isMorning,isMom,isAsleep);
            Assert.AreEqual(result, actual); 
        }

        //Exercise 13
        [TestCase(1,2,3,true)]
        [TestCase(3, 1, 2, true)]
        [TestCase(3, 2, 2, false)]
        public void TwoIsOne(int a, int b, int c, bool result)
        {
            Logic log = new Logic();
            bool actual = log.TwoIsOne(a, b, c);
            Assert.AreEqual(result, actual); 
        }

        //Exercise 14
        [TestCase(1,2,4,false,true)]
        [TestCase(1, 2, 1, false, false)]
        [TestCase(1, 1, 2, true, true)]
        public void AreInOrder(int a, int b, int c, bool bOk,bool result)
        {
            Logic log = new Logic();
            bool actual = log.AreInOrder(a,b,c,bOk);
            Assert.AreEqual(result, actual);
        }

        //Exercise 15
        [TestCase(2,5,11,false, true)]
        [TestCase(5, 7, 6, false, false)]
        [TestCase(5, 5, 7, true, true)]
        public void InOrderEqual(int a, int b, int c, bool equalOk,bool result)
        {
            Logic log = new Logic();
            bool actual = log.InOrderEqual(a, b, c, equalOk);
            Assert.AreEqual(result, actual);
        }
        
        //Exercise 16
        [TestCase(23,19,13,true)]
        [TestCase(23, 19, 12, false)]
        [TestCase(23, 19, 3, true)]
        public void LastDigit(int a, int b, int c,bool result)
        {
            Logic log = new Logic();
            bool actual = log.LastDigit(a, b, c);
            Assert.AreEqual(result, actual);
        }

        //Exercise 17
        [TestCase(2,3,true,5)]
        [TestCase(3, 3, true, 7)]
        [TestCase(3, 3, false, 6)]
        public void RollDice(int die1, int die2, bool noDoubles, int result)
        {
            Logic log = new Logic();
            int actual = log.RollDice(die1, die2, noDoubles);
            Assert.AreEqual(result,actual);
        }
    }
}

