﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    class ConditionalTests
    {
        //Exercise 1
        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void AreWeInTrouble(bool aSmile, bool bSmile, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.AreWeInTrouble(aSmile, bSmile);
            Assert.AreEqual(result,actual);
        }

        //Exercise 2
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        public void CanSleepIn(bool isWeekday, bool isVacation, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.CanSleepIn(isWeekday,isVacation);
            Assert.AreEqual(result, actual);
        }

        //Exercise 3
        [TestCase(1,2,3)]
        [TestCase(3,2,5)]
        [TestCase(2, 2, 8)]
        public void SumDouble(int a, int b,int result)
        {
            Conditional condition = new Conditional();
            int actual = condition.SumDouble(a, b);
            Assert.AreEqual(result,actual);
        }

        //Exercise 4
        [TestCase(23,4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21(int n, int result)
        {
            Conditional condition = new Conditional();
            int actual = condition.Diff21(n);
            Assert.AreEqual(result,actual);
        }

        // Exercise 5
        [TestCase(true , 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTrouble(bool isTalking, int hour, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.ParrotTrouble(isTalking, hour);
            Assert.AreEqual(result,actual);
        }

        // Exercise 6
        [TestCase(9,10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]

        public void Makes10(int a, int b, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.Makes10(a, b);
            Assert.AreEqual(result,actual);
        }

        // Exercise 7
        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundred(int n,bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.NearHundred(n);
            Assert.AreEqual(result,actual);
        }

        // Exercise 8
        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNeg(int a, int b, bool negative,bool results)
        {
            Conditional condition = new Conditional();
            bool actual = condition.PosNeg(a, b, negative);
            Assert.AreEqual(results,actual);
        }

        //Exercise 9
        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotString(string s, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.NotString(s);
            Assert.AreEqual(result, actual);
        }

        //Exercise 10
        
        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingChar(string str, int n, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.MissingChar(str, n);
            Assert.AreEqual(result,actual);
        }
        

        // Exercise 11
        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        public void FrontBack(string str, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.FrontBack(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 12
        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3(string str, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.Front3(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 13
        [TestCase("cat","tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAround(string str,string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.BackAround(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 14
        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void Multiple3Or5(int number, bool result)
        {
            Conditional condition  = new Conditional();
            bool actual = condition.Multiple3Or5(number);
            Assert.AreEqual(result,actual);
        }

        //Exercise 15
        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", true)]
        public void StartHi(string str,bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.StartHi(str);
            Assert.AreEqual(result, actual);
        }

        //Execise 16
        [TestCase(120, -1, true)]
        [TestCase(-1, 120, true)]
        [TestCase(2, 120, false)]
        public void IcyHot(int temp1, int temp2, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.IcyHot(temp1,temp2);
            Assert.AreEqual(result, actual);
        }

        //Exercise 17
        [TestCase(12, 99,true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10And20(int a, int b, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.Between10And20(a,b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 18
        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 10, 12, false)]
        public void HasTeen(int a, int b, int c, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.HasTeen(a,b,c);
            Assert.AreEqual(result, actual);
        }

        //Exercise 19
        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13, 13, false)]
        public void SoAlone(int a, int b, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.SoAlone(a,b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 20
        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void RemoveDel(string str, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.RemoveDel(str);   
            Assert.AreEqual(result,actual);
        }

        //Exercise 21
        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStart(string str, bool result)
        {
           Conditional condition = new Conditional();
           bool actual = condition.IxStart(str);
           Assert.AreEqual(result,actual);

        }

        //Exercise 22
        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        [TestCase("oxx", "o")]
        public void StartOz(string str, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.StartOz(str);
            Assert.AreEqual(result,actual);

        }

        //Exercise 23
        [TestCase(1, 2, 3, 3)]
        [TestCase(1, 3, 2, 3)]
        [TestCase(3, 3, 1, 3)]
        public void Max(int a, int b, int c, int result)
        {
            Conditional condition = new Conditional();
            int actual = condition.Max(a, b, c);
            Assert.AreEqual(result,actual);
        }

        //Exercise 24
        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        [TestCase(13, 7, 0)]
        public void Closer(int a, int b, int result)
        {
            Conditional condition = new Conditional();
            int actual = condition.Closer(a, b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 25
        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void GotE(string str, bool result)
        {
            Conditional condition = new Conditional();
            bool actual = condition.GotE(str);
            Assert.AreEqual(result,actual);
        }

        //Exercise 26
        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("Hi", "HI")]
        public void EndUp(string str, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.EndUp(str);
            Assert.AreEqual(result,actual);
        }

        //Exercise 27
        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]

        public void EveryNth(string str, int n, string result)
        {
            Conditional condition = new Conditional();
            string actual = condition.EveryNth(str, n);
            Assert.AreEqual(result,actual);
        }



    }
}
