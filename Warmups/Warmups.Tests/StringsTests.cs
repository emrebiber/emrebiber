﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringsTests
    {
        //Exercise 1
        [TestCase("Bob", "Hi Bob!")]
        [TestCase("Alice", "Hi Alice!")]
        [TestCase("X", "Hi X!")]
        public void SayHiTest(string name, string result)
        {
            // Arrange
            Strings stringValue = new Strings();

            // Act
            string actual = stringValue.SayHi(name);

            // Assert
            Assert.AreEqual(result, actual);
        }

        // Exercise 2
        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.Abba(a, b);
            Assert.AreEqual(result, actual);
        }

        // Exercise 3
        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void MakeTag(string tag, string content, string result)
        {
            Strings value = new Strings();
            string actual = value.MakeTag(tag, content);
            Assert.AreEqual(result, actual);
        }

        //Exercise 4
        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWord(string container, string word, string result)
        {
            Strings value = new Strings();
            string actual = value.InsertWord(container, word);
            Assert.AreEqual(result, actual); 
        }

        //Exercise 5
        [TestCase("Hello","lololo" )]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndings(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MultipleEndings(str);
            actual = string.Concat(Enumerable.Repeat(actual, 3));
            Assert.AreEqual(result, actual);

        }

        //Exercise 6
        [TestCase("Woohoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalf(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.FirstHalf(str);
            Assert.AreEqual(result, actual);

        }

        //////Exercise 7
        ////[TestCase("Hello", "ell")]
        ////[TestCase("java", "av")]
        ////[TestCase("coding", "odin")]
        ////public void TrimOne(string str, string result)
        ////{
        ////    Strings strings = new Strings();
        ////    string actual = strings.TrimOne(str);
        ////    Assert.AreEqual(result, actual);

        //}

        //Exercise 8
        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddle(string a, string b, string result)
        {
            Strings strings = new Strings();
            string actual = strings.LongInMiddle(a, b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 9
        [TestCase("Hello","lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotateleft2(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.Rotateleft2(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 10
        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotateright2(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.Rotateleft2(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 11
        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOne(string str, bool fromFront, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TakeOne(str,fromFront);
            Assert.AreEqual(result, actual);
        }

        //Exercise 12
        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwo(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.MiddleTwo(str);
            Assert.AreEqual(result,actual);
        }
        
        //Exercise 13
        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void EndsWithLy(string str, bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.EndsWithLy(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 14
        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBack(string str, int n, string result)
        {
            Strings strings = new Strings();
            string actual = strings.FrontAndBack(str, n);
            Assert.AreEqual(result,actual);
        }

        // Exercise 15
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2 , "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPosition(string str, int n, string result)
        {
            Strings strings = new Strings();
            string actual = strings.TakeTwoFromPosition(str, n);
            Assert.AreEqual(result,actual);

        }

        //Exercise 16
        [TestCase("badxx", true)]
        [TestCase("xbadxx", false)]
        [TestCase("xxbadxx", false)]
        public void HasBad(string str, bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.HasBad(str);
            Assert.AreEqual(result,actual);
        }

        //Exercise 17
        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirst(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.AtFirst(str);
            Assert.AreEqual(result, actual);

        }

        //Exercise 18
        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        public void LastChars(string a,string b,string result)
        {
            Strings strings = new Strings();
            string actual = strings.LastChars(a, b);
            Assert.AreEqual(result, actual);
        }
        /*
        //Exercise 19
        [TestCase("abc","cat","abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCat(string a, string b,string result)
        {
            Strings strings = new Strings();
            string actual = strings.LastChars(a, b);
            Assert.AreEqual(result, actual);
            
        }
        */

        //Exercise 20
        [TestCase("coding","codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]  
        public void SwapLast(string str, string result)
        {
            Strings strings = new Strings();
            string actual = strings.SwapLast(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 21
        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FrontAgain(string str,bool result)
        {
            Strings strings = new Strings();
            bool actual = strings.FrontAgain(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 22
        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCat(string a, string b,string result)
        {
            Strings strings = new Strings();
            string actual = strings.MinCat(a,b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 23
        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
       // [TestCase("abed", "abed")]
        public void TweakFront(string str,string result)
        {
            Strings strings = new Strings();
            string actual = strings.TweakFront(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 24
        [TestCase("xHi", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripX(string str,string result)
        {
            Strings strings = new Strings();
            string actual = strings.StripX(str);
            Assert.AreEqual(result, actual);
        }


    }
}
