﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class ArraysTests
    {
        //Exercise 1
        [TestCase(new[] { 1, 2, 6 }, true)]
        [TestCase(new[] { 6, 1, 2, 3 }, true)]
        [TestCase(new[] { 13, 6, 1, 2, 3 }, false)]
        public void FirstLast6(int[] numbers, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.FirstLast6(numbers);
            Assert.AreEqual(result,actual);
        }

        //Exercise 2
        [TestCase(new[] { 1, 2, 3 }, false)]
        [TestCase(new[] { 1, 2, 3, 1 }, true)]
        [TestCase(new[] { 1, 2, 1 }, true)]
        public void SameFirstLast(int[] numbers, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.SameFirstLast(numbers);
            Assert.AreEqual(result, actual);
        }


        //Exercise 3
        //[TestCase(new[] { 3, 1, 4 })]
        //public void MakePi(int n, int[] result)
        //{
        //    Arrays array = new Arrays();
        //    int[] actual = array.MakePi(n);
        //    Assert.AreEqual(result,actual);

        //}

        //Exercise 4
        [TestCase(new[] { 1, 2, 3 }, new[]{7,3}, true)]
        [TestCase(new[] { 1, 2, 3 }, new[] { 7, 3, 2 }, false)]
        [TestCase(new[] { 1, 2, 3 }, new[] { 1, 3 }, true)]
        public void CommonEnd(int[] a, int[] b, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.CommonEnd(a, b);
            Assert.AreEqual(result,actual);
        }

        //Exercise 5
        [TestCase(new[] { 1, 2, 3 }, 6)]
        [TestCase(new[] { 5, 11, 2 }, 18)]
        [TestCase(new[] { 7, 0, 0 }, 7)]
        public void Sum(int[] numbers, int result)
        {
            Arrays array =new Arrays();
            int actual = array.Sum(numbers);
            Assert.AreEqual(result,actual);
        }

        //Exercise 6
        [TestCase(new[] { 1 ,2, 3},  new[] { 2, 3, 1})]
        [TestCase(new[] { 5, 11, 9 }, new[] { 11, 9, 5 })]
        [TestCase(new[] { 7, 0, 0 }, new[] { 0, 0, 7 })]
        public void RotateLeft(int[] numbers, int[] result)
        {
           Arrays array = new Arrays();
           int[] actual = array.RotateLeft(numbers);
           Assert.AreEqual(result,actual);
        }

        //Exercise 7
        [TestCase(new[] { 1, 2, 3 },new[] { 3, 2, 1 })]
        public void Reverse(int[] numbers, int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.Reverse(numbers);
            Assert.AreEqual(result, actual);
        }

        //Exercise 8
        [TestCase(new[] { 1, 2, 3 }, new[] { 3, 3, 3 })]
        [TestCase(new[] { 11, 5, 9 }, new[] { 11, 11, 11 })]
        [TestCase(new[] { 2, 11, 3 }, new[] { 3, 3, 3 })]
        public void HigherWins(int[] numbers, int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.HigherWins(numbers);
            Assert.AreEqual(result,actual);

        }

        // Exercise 9
        [TestCase(new []{1,2,3}, new []{4,5,6}, new[]{2,5})]
        [TestCase(new[] { 7, 7, 7 }, new[] { 3, 8, 0 }, new[] { 7, 8 })]
        [TestCase(new[] { 5, 2, 9 }, new[] { 1, 4, 5 }, new[] { 2, 4 })]
        public void GetMiddle(int[] a, int[] b, int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.GetMiddle(a, b);
            Assert.AreEqual(result,actual);
        }

        //Exercise 10
        [TestCase(new[] { 2, 5 }, true)]
        [TestCase(new[] { 4, 3 }, true)]
        [TestCase(new[] { 7, 5 }, false)]
        public void HasEven(int[] numbers, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.HasEven(numbers);
            Assert.AreEqual(result,actual);
        }

        // Exercise 11
        [TestCase(new []{4,5,6}, new[] {0,0,0,0,0,6})]
        [TestCase(new[] { 4, 2}, new[] { 0, 0, 0, 2 })]
        [TestCase(new[] { 3 }, new[] { 0, 3 })]
        public void KeepLast(int[] numbers,int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.KeepLast(numbers);
            Assert.AreEqual(result,actual);
        }

        //Exercise 12
        [TestCase(new[] { 2, 2, 3 }, true)]
        [TestCase(new[] { 3, 4, 5, 3 }, true)]
        [TestCase(new[] { 2, 3, 2, 2 }, false)]
        public void Double23(int[] numbers, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.Double23(numbers);
            Assert.AreEqual(result,actual);

        }

        // Exercise 13
        [TestCase(new[] { 1, 2, 3 }, new[] { 1, 2, 0 })]
        [TestCase(new[] { 2, 3, 5 }, new[] { 2, 0, 5 })]
        [TestCase(new[] { 1, 2, 1 }, new[] { 1, 2, 1 })]
        public void Fix23(int[] numbers, int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.Fix23(numbers);
            Assert.AreEqual(result, actual);
        }

        //Exercise 14
        [TestCase(new[] { 1, 3, 4, 5 }, true)]
        [TestCase(new[] { 2, 1, 3, 4, 5 }, true)]
        [TestCase(new[] { 1, 1, 1 }, false)]
        public void Unlucky1(int[] numbers, bool result)
        {
            Arrays array = new Arrays();
            bool actual = array.Unlucky1(numbers);
            Assert.AreEqual(result,actual);

        }

        //Exercise 15
        [TestCase(new[] { 4, 5 }, new[] { 1, 2, 3 }, new[] { 4, 5 })]
        [TestCase(new[] { 4 }, new[] { 1, 2, 3 }, new[] { 4, 1 })]
        [TestCase(new int[] { }, new[] { 1, 2}, new[] { 1, 2 })]
        public void Make2(int[] a, int[] b, int[] result)
        {
            Arrays array = new Arrays();
            int[] actual = array.Make2(a, b);
            Assert.AreEqual(result,actual);
        }


    }
}
