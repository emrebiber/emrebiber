﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Warmups.Tests
{
    [TestFixture]
    public class LoopsTests
    {
        //Exercise 1
        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimes(string str, int n, string result)
        {
            Loops loopsValue = new Loops();
            string actual = loopsValue.StringTimes(str, n);
            Assert.AreEqual(result, actual);

        }

        //Exercise 2
        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimes(string str, int n,string result)
        {
            Loops loopsValue = new Loops();
            string actual = loopsValue.FrontTimes(str, n);
            Assert.AreEqual(result, actual);
        }

        //Exercise 3
        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        public void CountXx(string str,int result)
        {
            Loops loops = new Loops();
            int actual = loops.CountXx(str);
            Assert.AreEqual(result,actual);
        }

        //Exercise 4
        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        public void DoubleX(string str, bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.DoubleX(str);
            Assert.AreEqual(result,actual);
        }

        // Exercise 5
        [TestCase("Hello", "Hlo")]
        [TestCase("Hi", "H")]
        [TestCase("Heeololeo", "Hello")]
        public void EveryOther(string str,string result)
        {
            Loops loops = new Loops();
            string actual = loops.EveryOther(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 6
        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        public void StringSplosion(string str, string result)
        {
            Loops loops = new Loops();
            string actual = loops.StringSplosion(str);
            Assert.AreEqual(result,actual);
        }

        //Exercise 7
        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaaxx", 2)]
        public void CountLast2(string str,int result)
        {
            Loops loops = new Loops();
            int actual = loops.CountLast2(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 8
        [TestCase(new int[] {1, 2, 9}, 1)]
        [TestCase(new int[] { 1, 9, 9 }, 2)]
        [TestCase(new int[] { 1, 9, 9, 3, 9 }, 3)]
        public void Count9(int[] numbers, int result)
        {
            Loops loops = new Loops();
            int actual = loops.Count9(numbers);
            Assert.AreEqual(result, actual);
        }

        //Exercise 9
        [TestCase(new int[] {1, 2, 9, 3, 4}, true)]
        [TestCase(new int[] {1, 2, 3, 4, 9}, false)]
        [TestCase(new int[] {1, 2, 3, 4, 5}, false)]
        public void ArrayFront9(int[] numbers,bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.ArrayFront9(numbers);
            Assert.AreEqual(result, actual);
        }

        //Exercise 10
        [TestCase(new int[] {1, 1, 2, 3, 1}, true)]
        [TestCase(new int[] {1, 2, 2, 4, 1}, false)]
        [TestCase(new int[] {1, 1, 2, 1, 2, 3}, true)]
        public void Array123(int[] numbers,bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.Array123(numbers);
            Assert.AreEqual(result, actual);
        }

        //Exercise 11
        [TestCase("xxcaazz", "xxbaaz", 3)]
        [TestCase("abc", "abc", 2)]
        [TestCase("abc", "axc", 0)]
        public void SubStringMatch(string a, string b,int result)
        {
            Loops loops = new Loops();
            int actual = loops.SubStringMatch(a,b);
            Assert.AreEqual(result, actual);
        }

        //Exercise 12
        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringX(string str,string result)
        {
            Loops loops = new Loops();
            string actual = loops.StringX(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 13
        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chol")]
        [TestCase("CodingHorror", "Congrr")]
        public void AltPairs(string str, string result)
        {
            Loops loops = new Loops();
            string actual = loops.AltPairs(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 14
       // [TestCase(new string[] { "yakpak" }, "pak")]
       // [TestCase(new string[] { "pakyak" }, "pak")]
      //  [TestCase(new string[] { "yak123ya" }, "123ya")]
        public void DoNotYak(string[] str,string result)
        {
            Loops loops = new Loops();
            string actual = loops.DoNotYak(str);
            Assert.AreEqual(result, actual);
        }

        //Exercise 15
        [TestCase(new int[] {6, 6, 2}, 1)]
        [TestCase(new int[] { 6, 6, 2, 6 }, 1)]
        [TestCase(new int[] { 6, 7, 2, 6 }, 1)]

        public void Array667(int[] numbers, int result)
        {
            Loops loops = new Loops();
            int actual = loops.Array667(numbers);
            Assert.AreEqual(result, actual); 
        }

        //Exercise 16
        [TestCase(new int[] { 1, 1, 2, 2, 1 }, true)]
        [TestCase(new int[] { 1, 1, 2, 2, 2, 1 }, false)]
        [TestCase(new int[] { 1, 1, 1, 2, 2, 2, 1 }, false)]
        public void NoTriples(int[] numbers, bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.NoTriples(numbers);
            Assert.AreEqual(result, actual); 
        }

        //Exercise 17
        [TestCase(new int[] { 1, 2, 7, 1 }, true)]
        [TestCase(new int[] { 1, 2, 8, 1 }, true)]
        [TestCase(new int[] { 2, 7, 1 }, true)]
        public void Pattern51(int[] numbers, bool result)
        {
            Loops loops = new Loops();
            bool actual = loops.NoTriples(numbers);
            Assert.AreEqual(result, actual); 
        }

    }
}
