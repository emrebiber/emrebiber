﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using RPG.Inentory.Containers;
using RPG.Inentory.Weapons;

namespace RPG.Inventroy.Tests
{
    [TestFixture]
    public class BackpackTests
    {
        [Test]
        public void PutItemInBackpackSuccessfully()
        {
            //Arrange
            Backpack bag = new Backpack();
            WoodenSword sword = new WoodenSword();

            //Act
            bag.Add(sword);

            //Assert
            Assert.AreEqual(1, bag.ItemCount);

            bag.DisplayContents();
        }
    }
}
