﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inentory.Base;

namespace RPG.Inentory.Containers
{
    public class ReagentPouch : SpecificContainer
    {
        public ReagentPouch() : base(ItemType.Reagent, 12)
        {
            Name = "A small reagent pouch";
            Description = "blah....";
            Weight = 1;
            Value = 30;
            Type = ItemType.Container;
        }
    }
}
