﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inentory.Base;

namespace RPG.Inentory.Containers
{
    public class Backpack : Container
    {
        public Backpack() : base(8)
        {
            Name = "A leather Backpack";
            Description = string.Format("This backpack has {0} slots", _capacity);
            Weight = 5;
            Value = 150;
            Type = ItemType.Container;
        }
    }
}
