﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelValidation2.Models;

namespace ModelValidation2.Controllers
{
    public class AppoinmentController : Controller
    {
        // GET: Appoinment
        public ActionResult MakeBooking()
        {
            return View(new Appointment() {Date = DateTime.Now});
        }

        [HttpPost]
        public ActionResult MakeBooking(Appointment appt)
        {
            //Explicit validation
            //if (string.IsNullOrEmpty(appt.ClientName))
            //{
            //    ModelState.AddModelError("ClientName", "Please enter your name");
            //}
            //if (ModelState.IsValidField("Date") && DateTime.Now > appt.Date)
            //{
            //    ModelState.AddModelError("Date", "Please enter a date in the future");
            //}
            //if (!appt.TermsAccepted)
            //{
            //    ModelState.AddModelError("TermsAccepted", "You must accept the terms and conditions");
            //}
            //if (ModelState.IsValidField("ClientName") && ModelState.IsValidField("Date"))
            //{
            //    if (appt.ClientName == "Garfield" && appt.Date.DayOfWeek == DayOfWeek.Monday)
            //    {
            //        ModelState.AddModelError("", "Garfield can't book appoinments on monyda");
            //    }
            //}



            if (ModelState.IsValid)
            {
                //This is where I would persist my data
                return View("Completed", appt);
            }

            return View(appt);
        }
    }
}