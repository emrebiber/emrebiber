﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using ModelValidation2.Models.Attributes;

namespace ModelValidation2.Models
{
    //[NoGarfieldOnMondays(ErrorMessage =  "No Garfield on Mondays! EVER!")]
    public class Appointment : IValidatableObject
    {
        /// <summary>
        /// in required attribute we could put error message too
        /// [Required(ErrorMessage = "You must enter name")]
        /// </summary>
        //[Required]
        [DisplayName("Your Name")]
        public string ClientName { get; set; }

        [DataType(DataType.Date)]
       // [Required(ErrorMessage = "Please enter a date")]
       [FutureDate(ErrorMessage= "Please enter a date in the future")]
        public DateTime Date { get; set; }

        //[Range(typeof(bool), "true", "true", ErrorMessage = "You must accept the terms")]
       // [MustBeTrue(ErrorMessage = "You must accept my custom terms")]
        public bool TermsAccepted { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(ClientName))
            {
                errors.Add(new ValidationResult("Please enter your name", new [] {"ClientName"}));
            }

            if (DateTime.Now > Date)
            {
                errors.Add(new ValidationResult("Please select a future date", new [] {"Date"}));
            }

            if (errors.Count == 0 && ClientName == "Garfield" && Date.DayOfWeek == DayOfWeek.Monday)
            {
                errors.Add(new ValidationResult("Garfield cannot book appoinment on Mondays"));
            }

            if (!TermsAccepted)
            {
                errors.Add(new ValidationResult("You must accept the terms", new []{"TermsAccepted"}));
            }

            return errors;
        }
    }
}