﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValueVsReferenceTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            //DemonstrateValueType();
            DemonstrateReferenceType();

            Console.ReadLine();

            Console.WriteLine();
        }

        static void DemonstrateReferenceType()
        {

            Person myPerson = new Person();
            myPerson.Name = "Jim";

            ManipulatePerson(myPerson);

            Console.WriteLine("The name of myPerson is {0}", myPerson.Name);

        }

        static void ManipulatePerson(Person myPerson)
        {
            myPerson.Name = "Bob";
        }

        static void DemonstrateValueType()
        {
            int x = 5;

            ManipulateInt(x);

            Console.Write("The value of x is: {0}", x);
        }

        static void ManipulateInt(int x)
        {
            x = 10;
        }

    }
}
