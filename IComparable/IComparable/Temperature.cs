﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IComparable
{
    public class Temperature : IComparable
    {
        public double Fahrenheit { get; set; }

        public double Celsius
        {
            get { return (Fahrenheit - 32)*(5.0/9); }
            set { Fahrenheit = (value*9.0/5 )+ 32; }
        }
    }
}
