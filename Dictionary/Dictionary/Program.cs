﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {

            // First Example
            Dictionary<int, string> dict = new Dictionary<int, string>();

            dict.Add(100, "Bill");
            dict.Add(200, "Steve");

            if (dict.ContainsKey(200))
            {
                Console.WriteLine(true);
            }


            //Second Example
            Dictionary<string, string> values = new Dictionary<string, string>();

            values.Add("cat", "Garfield");
            values.Add("dog", "Odie");
           
            string test;

            if (values.TryGetValue("cat", out test)) 
            {
                Console.WriteLine(test); 
            }
            if (values.TryGetValue("bird", out test)) 
            {
                Console.WriteLine(false); 
            }

            Console.ReadLine();
        }
    }
}
