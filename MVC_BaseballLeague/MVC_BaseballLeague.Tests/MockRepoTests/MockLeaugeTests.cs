﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Repositories;
using MVC_BaseballLeague.Models;
using NUnit.Framework;

namespace MVC_BaseballLeague.Tests.MockRepoTests
{
    [TestFixture]
    public class MockLeaugeTests
    {
        [Test]
        public void CanGetLeagueFromMockLeaugeRepository()
        {
            var repo = new MockLeagueRepository();
            var league = repo.Get();

            Assert.AreEqual(1, league.LeaugeId);
            Assert.AreEqual("Major League Baseball", league.LeaugeName);
            Assert.AreEqual("MLB", league.LeaugeAbb);
        }

        [Test]
        public void CanGetAllTeamsFromMockTeamRepository()
        {
            var repo = new MockTeamRepository();
            var teams = repo.GetAll();

            Assert.AreEqual(4, teams.Count);
        }

        [Test]
        public void CanGetOneSpecificTeam()
        {
            var repo = new MockTeamRepository();
            var team = repo.GetSpecificOne(1);

            Assert.AreEqual(1,team.TeamId);
            Assert.AreEqual("New York Yankees", team.TeamName);
            Assert.AreEqual("Emre Biber", team.Manager);
            Assert.AreEqual(1, team.Leauge.LeaugeId);
            Assert.AreEqual("Major Baseball Leauge", team.Leauge.LeaugeName);
            Assert.AreEqual("MLB", team.Leauge.LeaugeAbb);
            Assert.AreEqual(11, team.Players[0].PlayerId);
            Assert.AreEqual("Ben Heller", team.Players[0].PlayerName);
            Assert.AreEqual(61,team.Players[0].JerseyNumber);
            Assert.AreEqual("LF", team.Players[1].Position);
            
        } 

        [Test]
        public void CanAddTeamsToMockTeamRepository()
        {
            var repo = new MockTeamRepository();
            var newTeam = new Team();

            newTeam.TeamId = 5;
            newTeam.TeamName = "Besiktas";
            newTeam.Manager = "Erdem Hoca";
            newTeam.Leauge = new Leauge()
            {
                LeaugeId = 1,
                LeaugeName = "Super Lig",
                LeaugeAbb = "SP"
            }; 
            newTeam.Players = new List<Player>()
            {
                new Player()
                {
                    PlayerId = 18,
                    PlayerName = "Sergen Yalcin",
                    JerseyNumber = 10,
                    Position = "C"
                }
            };

            repo.Add(newTeam);
            var total = repo.GetAll();
            
            Assert.AreEqual(4, total.Count );
        }

       // [Test]
        public void CanDeleteTeamFromRepository()
        {
            var repo =  new MockTeamRepository();
            repo.Delete(1);
            var teams = repo.GetAll(); 

            Assert.AreEqual(2, teams.Count);
        }

        [Test]
        public void CanListAllPlayers()
        {
            var repo = new MockPlayerRepository();
            var players = repo.GetAll();

            Assert.AreEqual(3, players.Count);
        }

        [Test]
        public void CanListOneSpecificPlayer()
        {
            var repo = new MockPlayerRepository();
            var player = repo.GetOne(13);

            Assert.AreEqual(13, player.PlayerId);
            Assert.AreEqual("Mason Butler", player.PlayerName);
            Assert.AreEqual(36, player.JerseyNumber);
            Assert.AreEqual("P", player.Position);
            Assert.AreEqual(9.0, player.PrevYearAvg);
            Assert.AreEqual(7, player.NumberOfYearsPlayed);
        }

        
    }
}
