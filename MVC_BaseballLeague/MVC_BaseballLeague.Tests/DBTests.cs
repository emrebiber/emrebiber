﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Repositories;
using MVC_BaseballLeague.Models;
using NUnit.Framework;

namespace MVC_BaseballLeague.Tests
{
    [TestFixture]
    public class DBTests
    {
        [Test]
        public void CanGetLeaugeFromDB()
        {
            var repo = new DBLeaugeRepository();
            var leauge = repo.Get();

            Assert.AreEqual(1, leauge.LeaugeId);
            Assert.AreEqual("Major Baseball Leauge", leauge.LeaugeName);
            Assert.AreEqual("MLB", leauge.LeaugeAbb);
        }

        [Test]
        public void CanGetAllPlayerFromDB()
        {
            var repo = new DBPlayerRepository();
            var player = repo.GetAll();

            Assert.AreEqual(1, player[0].PlayerId);
            Assert.AreEqual("Emre Biber", player[0].PlayerName);
            Assert.AreEqual(10,player[0].JerseyNumber);
            Assert.AreEqual("2B", player[0].Position);

            Assert.AreEqual(2, player[1].PlayerId);
            Assert.AreEqual("Bobby Singer", player[1].PlayerName);
            Assert.AreEqual(20, player[1].JerseyNumber);
            Assert.AreEqual("1B", player[1].Position);

        }

        [Test]
        public void GetOnePlayerById()
        {
            var repo = new DBPlayerRepository();
            var player = repo.GetOne(1);

            Assert.AreEqual(1, player.PlayerId);
            Assert.AreEqual("Emre Biber", player.PlayerName);
            Assert.AreEqual(10, player.JerseyNumber);
            Assert.AreEqual("2B", player.Position);
            Assert.AreEqual(9.90, player.PrevYearAvg);
            Assert.AreEqual(5, player.NumberOfYearsPlayed);
        }

        [Test]
        public void CanGetAllTeamsFromDB()
        {
            var repo = new DBTeamRepository();
            var teams = repo.GetAll();

            Assert.AreEqual(5, teams.Count);
            Assert.AreEqual(1, teams[0].TeamId);
            Assert.AreEqual("New York Yankees", teams[0].TeamName);
            Assert.AreEqual("Mel Gibson", teams[0].Manager);
            Assert.AreEqual(1, teams[0].Leauge.LeaugeId);
        }

        [Test]
        public void CanGetOneTeamById()
        {
            var repo = new DBTeamRepository();
            var team = repo.GetSpecificOne(2);

            Assert.AreEqual(2, team.TeamId);
            Assert.AreEqual("Boston Red Sox", team.TeamName);
            Assert.AreEqual("Tom Smith", team.Manager);
            Assert.AreEqual(1, team.Leauge.LeaugeId);
        }

        [Test]
        public void CanAddNewPlayer()
        {
            DBPlayerRepository repo = new DBPlayerRepository();
            Player newPlayer = new Player();

            newPlayer.PlayerName = "Dean Williams";
            newPlayer.JerseyNumber = 53;
            newPlayer.Position = "C";
            newPlayer.PrevYearAvg = 7.7M;
            newPlayer.NumberOfYearsPlayed = 8;

            repo.Add(newPlayer);


            Assert.AreEqual("Dean Williams", newPlayer.PlayerName);
            Assert.AreEqual(53, newPlayer.JerseyNumber);
            Assert.AreEqual("C", newPlayer.Position);
            Assert.AreEqual(7.7, newPlayer.PrevYearAvg);
            Assert.AreEqual(8,newPlayer.NumberOfYearsPlayed);
        }

        [Test]
        public void CanAddNewTeam()
        {
            DBTeamRepository repo =new DBTeamRepository();
            Team newTeam = new Team();

            newTeam.TeamName = "San Francisco Giants";
            newTeam.Manager = "Sam White";
            newTeam.Leauge.LeaugeId = 1;

            repo.Add(newTeam);

            Assert.AreEqual("San Francisco Giants", newTeam.TeamName);
            Assert.AreEqual("Sam White", newTeam.Manager);
            Assert.AreEqual(1, newTeam.Leauge.LeaugeId);


        }
    }.
}
