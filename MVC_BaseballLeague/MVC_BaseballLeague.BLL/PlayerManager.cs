﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Factories;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.BLL
{
    public class PlayerManager
    {
        private static IPlayerRepository _playerRepository;

        public PlayerManager()
        {
            _playerRepository = PlayerRepositoryFactory.GetPlayerRepository();
        }

        public List<Player> GetAllPlayers()
        {
            var players = _playerRepository.GetAll();
            return players;
        }

        public Player GetPlayerById(int id)
        {
            var player = _playerRepository.GetOne(id);
            return player;
        }

        public void AddPlayer(Player player)
        {
            _playerRepository.Add(player);
        }

        public void EditPlayer(Player player)
        {
            _playerRepository.Edit(player);
        }

        public void DeletePlayer(int id)
        {
            _playerRepository.Delete(id);
        }
    }
}
