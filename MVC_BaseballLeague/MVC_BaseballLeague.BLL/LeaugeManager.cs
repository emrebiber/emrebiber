﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Factories;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.BLL
{
    public class LeaugeManager
    {
        private ILeaugeRepository _leaugeRepository;

        public LeaugeManager()
        {
            _leaugeRepository = LeaugeRepositoryFactory.GetRightRepository();
        }

        public Leauge GetLeauge()
        {
            var leauge = _leaugeRepository.Get();
            return leauge;
        }      
    }
}
