﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Factories;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Data.Repositories;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.BLL
{
    public class TeamManager
    {
        private static ITeamRepository _teamRepository;

        public TeamManager()
        {
            _teamRepository = TeamRepositoryFactory.GetTeamRepository();
        }

        public List<Team> GetAllTeams()
        {
            var teams = _teamRepository.GetAll();
            return teams;
        }
        
        public Team GetTeamById(int id)
        {
            var team = _teamRepository.GetSpecificOne(id);
            return team;
        }

        public void AddTeam(Team team)
        {
            _teamRepository.Add(team);
        }

        public void DeleteTeam(int id)
        {
            _teamRepository.Delete(id);
        }
    }
}
