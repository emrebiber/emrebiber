﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class MockTeamRepository : ITeamRepository
    {
        private static List<Team> _teams;

        static MockTeamRepository()
        {
            _teams = new List<Team>()
            {
                new Team()
                {
                    TeamId = 1,
                    TeamName = "New York Yankees",
                    Manager = "Emre Biber",
                    Leauge = new Leauge
                    {
                        LeaugeId = 1,
                        LeaugeName = "Major Baseball Leauge",
                        LeaugeAbb = "MLB"
                    },
                    Players = new List<Player>()
                    {
                         new Player()
                {
                    PlayerId = 11,
                    PlayerName = "Ben Heller",
                    JerseyNumber = 61,
                    Position = "C",
                    PrevYearAvg = 5.5M,
                    NumberOfYearsPlayed = 3
                },
                 new Player()
                {
                    PlayerId = 12,
                    PlayerName = "Blake Parker",
                    JerseyNumber = 47,
                    Position = "LF",
                    PrevYearAvg = 7.2M,
                    NumberOfYearsPlayed = 2
                },
                  new Player()
                {
                    PlayerId = 13,
                    PlayerName = "Mason Butler",
                    JerseyNumber = 36,
                    Position = "P",
                    PrevYearAvg = 9.0M,
                    NumberOfYearsPlayed = 7
                },
                    }
                },

                 new Team()
                 {
                    TeamId = 2,
                    TeamName = "Boston Red Sox",
                    Manager = "Dave Balzer",
                    Leauge = new Leauge
                    {
                        LeaugeId = 1,
                        LeaugeName = "Major Baseball Leauge",
                        LeaugeAbb = "MLB"
                    },
                    Players = new List<Player>()
                    {
                         new Player()
                {
                    PlayerId = 14,
                    PlayerName = "John Smith",
                    JerseyNumber = 15,
                    Position = "SS",
                    PrevYearAvg = 8.5M,
                    NumberOfYearsPlayed = 1
                },
                 new Player()
                {
                    PlayerId = 15,
                    PlayerName = "Keith Brown",
                    JerseyNumber = 99,
                    Position = "CF",
                    PrevYearAvg = 1.2M,
                    NumberOfYearsPlayed = 2
                },
                  new Player()
                {
                    PlayerId = 16,
                    PlayerName = "Corey White",
                    JerseyNumber = 10,
                    Position = "1B",
                    PrevYearAvg = 4.0M,
                    NumberOfYearsPlayed = 4
                },
                    }
                },
                  new Team()
                 {
                    TeamId = 3,
                    TeamName = "Texas",
                    Manager = "Tom Jerry",
                    Leauge = new Leauge
                    {
                        LeaugeId = 1,
                        LeaugeName = "Major Baseball Leauge",
                        LeaugeAbb = "MLB"
                    },
                    Players = new List<Player>()
                    {
                        new Player()
                        {
                            PlayerId = 15,
                            PlayerName = "Michael Orange",
                            JerseyNumber = 53,
                            Position = "P",
                            PrevYearAvg = 6.7M,
                            NumberOfYearsPlayed = 9
                         },
                    }
                }


            };
        }


        public List<Team> GetAll()
        {
            return _teams;
        }

        public Team GetSpecificOne(int id)
        {
           return _teams.FirstOrDefault(t => t.TeamId == id);
        }

        public void Add(Team team)
        {
            _teams.Add(team);
        }

        public void Delete(int id)
        {
            _teams.RemoveAll(t => t.TeamId == id);
        }
    }
}
