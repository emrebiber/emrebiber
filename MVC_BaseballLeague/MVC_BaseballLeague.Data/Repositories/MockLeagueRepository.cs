﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class MockLeagueRepository : ILeaugeRepository
    {
        private static Leauge _leauge;

        static MockLeagueRepository()
        {
            _leauge = new Leauge()
            {
                LeaugeId = 1,
                LeaugeName = "Major League Baseball",
                LeaugeAbb = "MLB"
            };
        }


        public Leauge Get()
        {
            return _leauge;
        }
    }
}
