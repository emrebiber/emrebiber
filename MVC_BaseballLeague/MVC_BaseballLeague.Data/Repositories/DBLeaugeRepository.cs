﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class DBLeaugeRepository : ILeaugeRepository
    {
        public Leauge Get()
        {
            Leauge leauge = new Leauge();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Leauge";
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        leauge.LeaugeId = (int)dr["LeaugeID"];
                        leauge.LeaugeName = dr["LeaugeName"].ToString();
                        leauge.LeaugeAbb = dr["LeaugeAbb"].ToString();
                    }
                }
            }

            return leauge;
        }
    }
}
