﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class MockPlayerRepository : IPlayerRepository
    {
        private static List<Player> _players;

        static MockPlayerRepository()
        {
            _players = new List<Player>()
            {
                new Player()
                {
                    PlayerId = 11,
                    PlayerName = "Ben Heller",
                    JerseyNumber = 61,
                    Position = "C",
                    PrevYearAvg = 5.5M,
                    NumberOfYearsPlayed = 3
                },
                 new Player()
                {
                    PlayerId = 12,
                    PlayerName = "Blake Parker",
                    JerseyNumber = 47,
                    Position = "LF",
                    PrevYearAvg = 7.2M,
                    NumberOfYearsPlayed = 2
                },
                  new Player()
                {
                    PlayerId = 13,
                    PlayerName = "Mason Butler",
                    JerseyNumber = 36,
                    Position = "P",
                    PrevYearAvg = 9.0M,
                    NumberOfYearsPlayed = 7
                },
            };
        }

        public List<Player> GetAll()
        {
            return _players;
        }

        public Player GetOne(int id)
        {
            return _players.FirstOrDefault(p => p.PlayerId == id);
        }

        public void Add(Player player)
        {
           _players.Add(player);
        }

        public void Edit(Player player)
        {
            var selectedPlayer = _players.First(p => p.PlayerId == player.PlayerId);

            selectedPlayer.PlayerName = player.PlayerName;
            selectedPlayer.JerseyNumber = player.JerseyNumber;
            selectedPlayer.Position = player.Position;
            selectedPlayer.PrevYearAvg = player.PrevYearAvg;
            selectedPlayer.NumberOfYearsPlayed = player.NumberOfYearsPlayed;
        }

        public void Delete(int id)
        {
            _players.RemoveAll(p => p.PlayerId == id);
        }
    }
}
