﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class DBTeamRepository : ITeamRepository
    {
        public List<Team> GetAll()
        {
            List<Team> teams = new List<Team>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select TeamID, TeamName, Manager, t.LeaugeID " +
                                  "from Team as t " +
                                  "inner join Leauge as l " +
                                  "on t.LeaugeID = l.LeaugeID";


                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        teams.Add(PopulateTeamFromDataReader(dr));
                    }
                }
            }

            return teams;
        }

        public Team GetSpecificOne(int id)
        {
            Team team = new Team();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "TeamGetById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@TeamId", id);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        team = PopulateTeamFromDataReader(dr);
                    }
                }
            }

            return team;
        }

        public void Add(Team team)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddTeam";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@TeamName", team.TeamName);
                cmd.Parameters.AddWithValue("@Manager", team.Manager);
                cmd.Parameters.AddWithValue("@LeaugeId", team.Leauge.LeaugeId);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Team PopulateTeamFromDataReader(SqlDataReader dr)
        {
            Team team = new Team();

            team.TeamId = (int) dr["TeamID"];
            team.TeamName = dr["TeamName"].ToString();
            team.Manager = dr["Manager"].ToString();
            team.Leauge.LeaugeId = (int) dr["LeaugeID"];

            return team;
        }

    }
}
