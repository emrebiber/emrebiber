﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Repositories
{
    public class DBPlayerRepository : IPlayerRepository
    {

        public List<Player> GetAll()
        {
            List<Player> players = new List<Player>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Player";

                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        players.Add(PopulatePlayerFromDataReader(dr));
                    }
                }
            }

            return players;
        }

        public Player GetOne(int id)
        {
            Player player = new Player();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "PlayersGetById";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@PlayerId", id);

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        player = PopulatePlayerFromDataReader(dr);
                    }
                }

                return player;
            }
        }

        public void Add(Player player)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "insert into Player (PlayerName, JerseyNumber, Position, PrevYearAvg, NumberOfYearsPlayed) " + 
                                  "values (@PlayerName, @JerseyNumber, @Position, @PrevYearAvg, @NumberOfYearsPlayed)";

                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@PlayerName", player.PlayerName);
                cmd.Parameters.AddWithValue("@JerseyNumber", player.JerseyNumber);
                cmd.Parameters.AddWithValue("@Position", player.Position);
                cmd.Parameters.AddWithValue("@PrevYearAvg", player.PrevYearAvg);
                cmd.Parameters.AddWithValue("@NumberOfYearsPlayed", player.NumberOfYearsPlayed);

                cn.Open();

                cmd.ExecuteNonQuery();

            }
        }

        public void Edit(Player player)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public Player PopulatePlayerFromDataReader(SqlDataReader dr)
        {
            Player player = new Player();

            player.PlayerId = (int)dr["PlayerID"];
            player.PlayerName = dr["PlayerName"].ToString();
            player.JerseyNumber = (int)dr["JerseyNumber"];
            player.Position = dr["Position"].ToString();
            player.PrevYearAvg = (decimal)dr["PrevYearAvg"];
            player.NumberOfYearsPlayed = (int)dr["NumberOfYearsPlayed"];

            return player;

        }
    }
}
