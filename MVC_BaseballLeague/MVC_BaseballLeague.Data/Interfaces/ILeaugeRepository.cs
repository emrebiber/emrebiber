﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Interfaces
{
    public interface ILeaugeRepository
    {
        Leauge Get();
    }
}
