﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Models;

namespace MVC_BaseballLeague.Data.Interfaces
{
    public interface IPlayerRepository
    {
        List<Player> GetAll();
        Player GetOne(int id);
        void Add(Player player);
        void Edit(Player player);
        void Delete(int id);

    }
}
