﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Data.Repositories;

namespace MVC_BaseballLeague.Data.Factories
{
    public static class PlayerRepositoryFactory
    {
        public static IPlayerRepository GetPlayerRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockPlayerRepository();
                default:
                    throw new ArgumentException("This error comes from PlayerRepositoryFactory.");
            }
        }
    }
}
