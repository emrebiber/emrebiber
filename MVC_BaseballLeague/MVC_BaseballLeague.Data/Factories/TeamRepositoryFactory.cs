﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_BaseballLeague.Data.Interfaces;
using MVC_BaseballLeague.Data.Repositories;

namespace MVC_BaseballLeague.Data.Factories
{
    public static class TeamRepositoryFactory
    {

        public static ITeamRepository GetTeamRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Test":
                    return new MockTeamRepository();
                default:
                    throw new ArgumentException("This error comes from TeamRepositoryFactory");
            }
        }

      

    }
}
