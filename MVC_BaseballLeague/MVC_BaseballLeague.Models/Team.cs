﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_BaseballLeague.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string Manager { get; set; }
        public Leauge Leauge { get; set; } 
        public List<Player> Players { get; set; }

        public Team()
        {
            Leauge = new Leauge();
            Players = new List<Player>();
        }

    }
}
