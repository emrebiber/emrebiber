﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVC_BaseballLeague.Models
{
    public class Player
    {
        public int PlayerId { get; set; }
        public string PlayerName { get; set; }
        public int JerseyNumber { get; set; }   
        public string Position { get; set; }
        public decimal PrevYearAvg { get; set; }
        public int NumberOfYearsPlayed { get; set; }
        //public List<Team> Teams { get; set; }
    }
}
