﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RollDice
{
    class Program
    {
        static void Main(string[] args)
        {
            int counterForSix = 0;
            int counterForFive = 0;
            int counterForFour = 0;
            int counterForThree = 0;
            int counterForTwo = 0;
            int counterForOne = 0;

            Random rng = new Random();
            int number;
            
            for (int i = 0; i < 100; i++)
            {
                number = rng.Next(1, 7);

                if (number == 6)
                    counterForSix++;
                else if (number == 5)
                    counterForFive++;
                else if (number == 4)
                    counterForFour++;
                else if (number == 3)
                    counterForThree++;
                else if (number == 2)
                    counterForTwo++;
                else if (number == 1)
                    counterForOne++;
            }

            Console.Write("There is {0} times six. \n", counterForSix);
            Console.Write("There is {0} times five. \n", counterForFive);
            Console.Write("There is {0} times four. \n", counterForFour);
            Console.Write("There is {0} times three. \n", counterForThree);
            Console.Write("There is {0} times two. \n", counterForTwo);
            Console.Write("There is {0} times one. \n", counterForOne);

            Console.ReadLine();

        }
    }
}
