﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    public class Exercises
    {

        public void Ex01()
        {
            //  Find all products that are out of stock.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                          where p.UnitsInStock == 0
                          select p;

            //var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        public void Ex02()
        {
            //Find all products that are in stock and cost more than 3.00 per unit.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitsInStock != 0 && p.UnitPrice > 3.00M
                select p.ProductName;

            //var results = products.Where(p => p.UnitsInStock != 0 && p.UnitPrice > 3.00M).Select(p => p.ProductName);

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }


        }

        public void Ex03()
        {
            //Find all customers in Washington, print their name then their orders. (Region == "WA")

            var customers = DataLoader.LoadCustomers();

            var results = from cus in customers
                where cus.Region == "WA"
                select cus;

            //var results = customers.Where(cus => cus.Region == "WA");

            foreach (var print in results)
            {
                Console.WriteLine("{0, -20}", print.CompanyName);
                foreach (var order in print.Orders)
                {
                    Console.WriteLine("\t{0}", order.OrderID);
                }
            }
        }

        public void Ex04()
        {
            //Create a new sequence with just the names of the products.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {p.ProductName};

           // var results = products.Select(p => new { p.ProductName });

            foreach (var print in results)
            {
                Console.WriteLine(print.ProductName);
            }
        }

        public void Ex05()
        {
            //Create a new sequence of products and unit prices where the unit prices are increased by 25%.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {p.ProductName, unitPrice = p.UnitPrice * 1.25M};

           // var results = products.Select(p => new { p.ProductName, unitPrice = p.UnitPrice * 1.25M });

            foreach (var print in results)
            {
                Console.WriteLine("{0, -35} {1, -20}",print.ProductName ,print.unitPrice);
            }

        }

        public void Ex06()
        {
            //Create a new sequence of just product names in all upper case.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {UpperCaseProductName = p.ProductName.ToUpper()};

            // var results = products.Select(p => new {UpperCaseProductName = p.ProductName.ToUpper()});

            foreach (var print in results)
            {
                Console.WriteLine(print.UpperCaseProductName);
            }

        }

        public void Ex07()
        {
            //Create a new sequence with products with even numbers of units in stock.
            var products = DataLoader.LoadProducts();

            var results = from p in products
                where p.UnitsInStock%2 == 0
                select new {p.UnitsInStock};

           // var results = products.Where(p => p.UnitsInStock % 2 == 0).Select(p => new { p.UnitsInStock });

            foreach (var print in results)
            {
                Console.WriteLine(print.UnitsInStock);
            }

        }

        public void Ex08()
        {
            //Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                select new {p.ProductName, p.Category, Price = p.UnitPrice};

            // var results = products.Select(p => new {p.ProductName, p.Category, Price = p.UnitPrice});

            foreach (var print in results)
            {
                Console.WriteLine("{0, -35} {1, -15} {2, -10}", print.ProductName, print.Category,print.Price);
            }
        }

        public void Ex09()
        {
            //Make a query that returns all pairs of numbers from both arrays such that the number from numbersB is less than the number from numbersC.

            var numberB = DataLoader.NumbersB;
            var numberC = DataLoader.NumbersC;
            
            var results = from num1 in numberB
                from num2 in numberC
                where num1 < num2
                select new {arrayB = num1, arrayC= num2};

            foreach (var print in results)
            {
                Console.WriteLine("{0} \t {1}", print.arrayB, print.arrayC );
            }
        }

        public void Ex10()
        {
            //Select CustomerID, OrderID, and Total where the order total is less than 500.00.

            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                from o in c.Orders
                where o.Total < 500
                select new {c.CustomerID, o.OrderID, o.Total};

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }

        }

        public void Ex11()
        {
            //Write a query to take only the first 3 elements from NumbersA.
            var numbersA = DataLoader.NumbersA.Take(3);

            foreach (var print in numbersA)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex12()
        {
            //Get only the first 3 orders from customers in Washington.

            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers
                from o in c.Orders
                where c.Region == "WA"
                select new 
                {o.OrderID}).Take(3);

              //var results = (customers.SelectMany(c => c.Orders, (c, o) => new {c, o})
              //  .Where(@t => @t.c.Region == "WA")
              //  .Select(@t => new
              //  {@t.o.OrderID})).Take(3);

            foreach (var print in results)
            {
                Console.WriteLine(print);   
            }

        }

        public void Ex13()
        {
            //Skip the first 3 elements of NumbersA.

          //var numbersA = DataLoader.NumbersA.Skip(3);
            var numbersA = DataLoader.NumbersA;

            var result = (from num in numbersA
                select num).Skip(3);

        // var result = (numbersA.Select(num => num)).Skip(3);

            foreach (var print in result)
            {
                    Console.WriteLine(print);
            }
        }

        public void Ex14()
        {
            // Get all except the first two orders from customers in Washington.

            var customers = DataLoader.LoadCustomers();

            var results = (from c in customers
                from o in c.Orders
                where c.Region == "WA"
                select new {o.OrderID}).Skip(2);

            //var results = (customers.SelectMany(c => c.Orders, (c, o) => new { c, o })
            //    .Where(@t => @t.c.Region == "WA")
            //    .Select(@t => new { @t.o.OrderID })).Skip(2);

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex15()
        {
            //Get all the elements in NumbersC from the beginning until an element is greater or equal to 6.
            var numbersC = DataLoader.NumbersC.TakeWhile(n => n <= 6);

            foreach (var print in numbersC)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex16()
        {
            //Return elements starting from the beginning of NumbersC until a number is hit that is less than its position in the array. 

            var numbers = DataLoader.NumbersC.TakeWhile(n => n > Array.IndexOf(DataLoader.NumbersC,n));

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }

        }

        public void Ex17()
        {
            // Return elements from NumbersC starting from the first element divisible by 3.

            var numbers = DataLoader.NumbersC;

            var results = (numbers.SkipWhile(n => n % 3 != 0));

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }

        }

        public void Ex18()
        {
            //Order products alphabetically by name.

            var products = DataLoader.LoadProducts();

            var results = products.OrderBy(p => p.ProductName);

            foreach (var print in results)
            {
                 Console.WriteLine(print.ProductName);
            }
        }

        public void Ex19()
        {
            //Order products by UnitsInStock descending.

            var products = DataLoader.LoadProducts();

            var results = products.OrderByDescending(p => p.UnitsInStock);

            foreach (var print in results)
            {
                Console.WriteLine(print.UnitsInStock);
            }
        }

        public void Ex20()
        {
            //Sort the list of products, first by category, and then by unit price, from highest to lowest.

            var products = DataLoader.LoadProducts();

            var results = products.OrderBy(p => p.Category).ThenByDescending(p => p.UnitPrice);

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1,-15}", print.Category,print.UnitPrice);
            }

        }

        public void Ex21()
        {
            //Reverse NumbersC.

            var numbers = DataLoader.NumbersC.Reverse();

            foreach (var print in numbers)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex22()
        {
            
            // Display the elements of NumbersC grouped by their remainder when divided by 5.

            var numbers = DataLoader.NumbersC;

            var results = from num in numbers
                group num by num % 5
                into g
                select new 
                {   remainder = g.Key, 
                    numbers = g
                };

            foreach (var group in results)
            {
                Console.WriteLine("Numbers with a remainder of {0} when divided by 5:", group.remainder);
                foreach (var print2 in group.numbers)
                {
                    Console.WriteLine(print2);
                }
            }
        }

        public void Ex23()
        {
            //Display products by Category.

            var products = DataLoader.LoadProducts();

            var results = products.GroupBy( p => p.Category);

            foreach (var group in results)
            {
                foreach (var print in group)
                {
                    Console.WriteLine("{0, -35} {1, -15}", print.ProductName,print.Category);
                }
            }


        }

        public void Ex24()
        {
            // Group customer orders by year, then by month.

            var customers = DataLoader.LoadCustomers();

            var result = (from c in customers
                from o in c.Orders
                select new {c.CustomerID, o.OrderDate}).OrderBy(c => c.OrderDate.Year).ThenBy(c => c.OrderDate.Month);

            foreach (var print in result)
            {
                Console.WriteLine("{0, -20} {1, -20}", print.CustomerID,print.OrderDate);
            }
        }

        public void Ex25()
        {
            // Create a list of unique product category names.

            var products = DataLoader.LoadProducts();

            var results = (from p in products
                           select p.Category).Distinct();

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex26()
        {
            // Get a list of unique values from NumbersA and NumbersB.

            //var numbersA = DataLoader.NumbersA.Distinct();
            //var numbersB = DataLoader.NumbersB.Distinct();
            //var CombineList = numbersB.Union(numbersA);

            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;
            var CombineList = (numbersB.Union(numbersA)).Distinct();

            foreach (var print in CombineList)
            {
                Console.WriteLine(print);
            }


        }

        public void Ex27()
        {
            // Get a list of the shared values from NumbersA and NumbersB.

            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var IntersectAB = numbersB.Intersect(numbersA);

            foreach (var print in IntersectAB)
            {
                Console.WriteLine(print);
            }
        }

        public void Ex28()
        {
            //Get a list of values in NumbersA that are not also in NumbersB.

            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;
            var ADifferThanB = numbersA.Except(numbersB);

            foreach (var print in ADifferThanB)
            {
                Console.WriteLine(print);
            }

        }

        public void Ex29()
        {
            //Select only the first product with ProductID = 12 (not a list).

           // var products = DataLoader.LoadProducts().Where(p => p.ProductID == 12).Select(p => p.ProductName).First();
          //  Console.WriteLine(products);

            var products = DataLoader.LoadProducts();


            var results = products.First(p => p.ProductID == 12);
           
            //var results = (from p in products
            //    where p.ProductID == 12
            //    select p.ProductID).First();

            Console.WriteLine(results.ProductID);
   
        }

        public void Ex30()
        {
            // Write code to check if ProductID 789 exists.

            var products = DataLoader.LoadProducts();

            var results = products.Exists(p => p.ProductID == 789);

            Console.WriteLine(results);
        }

        public void Ex31()
        {

            //ask
            // Get a list of categories that have at least one product out of stock.

            var products = DataLoader.LoadProducts();

            var results = (from p in products
                where p.UnitsInStock == 0
                select p.Category).Distinct();

            foreach (var print in results)
            {
                Console.WriteLine(print);
            }


        }

        public void Ex32()
        {
            // Determine if NumbersB contains only numbers less than 9.

            var numbers = DataLoader.NumbersB;

            var results = numbers.All(n => n < 9);

            Console.WriteLine(results);
        }

        public void Ex33()
        {
            // Get a grouped a list of products only for categories that have all of their products in stock.

            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock != 0).Select(p => p).GroupBy(p => p.Category);

            foreach (var print in results)
            {
                foreach (var print2 in print)
                {
                    Console.WriteLine("{0, -35} {1, -20} {2,-15}", print2.ProductName,print2.Category,print2.UnitsInStock);
                }
                
            }
        }

        public void Ex34()
        {
            // Count the number of odds in NumbersA.

            var numbers = DataLoader.NumbersA;

            var results = numbers.Count(n => n%2 != 0);

            Console.WriteLine(results);
            
        }

        public void Ex35()
        {
            // Display a list of CustomerIDs and only the count of their orders.

            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                select new {c.CustomerID, CountCustomerOrder = c.Orders.Count()};

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -10}", print.CustomerID, print);
            }
        }

        public void Ex36()
        {
            // Display a list of categories and the count of their products.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                group p by p.Category
                into g
                select new {category = g.Key, count = g.Count()};

            //var results = products.GroupBy(p => p.Category).Select(g => new { category = g.Key, count = g.Count() });



            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -10}", print.category, print.count);
            }
        }

        public void Ex37()
        {
            // Display the total units in stock for each category.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                group p by p.Category
                into g
                select new
                {
                    Category = g.Key,
                    Count = g.Count(),
                    UnitInStock = g.Sum(p => p.UnitsInStock)
                };

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -5} {2,-15}", print.Category, print.Count, print.UnitInStock);
            }
        }

        public void Ex38()
        {
            // Display the lowest priced product in each category.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                group p by p.Category
                into g
                select new
                {
                    Category = g.Key,
                    LowestPrice = g.Min(p => p.UnitPrice)
                };

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -5}", print.Category, print.LowestPrice);
            }
        }

        public void Ex39()
        {
            // Display the highest priced product in each category.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                              into g
                              select new
                              {
                                  Category = g.Key,
                                  HighestPrice = g.Max(p => p.UnitPrice)
                              };

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -5}", print.Category, print.HighestPrice);
            }

        }

        public void Ex40()
        {
            // Show the average price of product for each category.

            var products = DataLoader.LoadProducts();

            var results = from p in products
                          group p by p.Category
                              into g
                              select new
                              {
                                  Category = g.Key,
                                  AveragePrice = g.Average(p => p.UnitPrice)
                              };

            foreach (var print in results)
            {
                Console.WriteLine("{0, -15} {1, -5}", print.Category, print.AveragePrice);
            }
        }
    }
}
