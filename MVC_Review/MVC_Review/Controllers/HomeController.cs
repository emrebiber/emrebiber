﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



/*  • Explain shared layouts
    • Use the Add View Dialogue window to add a
      layout page
    • Identify _Layout.cshtml and its location
    • Use the @RenderBody( ) and @RenderSection( ) methods
    • Use a partial view
*/

namespace MVC_Review.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult ActionName()
        {
            return View();
        }
    }
}
