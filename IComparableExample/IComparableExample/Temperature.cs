﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IComparableExample
{
    class Temperature : IComparable
    {
        public double Fahrenheit { get; set; }

        public double Celsius
        {
            get { return (Fahrenheit - 32)*(5.0/ 9); }
            set { Fahrenheit = (value*9.0/5) + 32; }
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Temperature otherTemp = obj as Temperature;

            if (otherTemp != null)
            {
                if (this.Fahrenheit < otherTemp.Fahrenheit)
                    return -1;

                if (this.Fahrenheit == otherTemp.Fahrenheit)
                    return 0;

                return 1;
            }
            else
            {
                throw new ArgumentException("Object is not a Temperature");
            }
        }
    }
}
