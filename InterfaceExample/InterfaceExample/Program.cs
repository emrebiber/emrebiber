﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.Print();

            Student s = new Student();
            s.Print();

            Employee e = new Employee();
            e.Print("This comes from Employee Class");

            Console.ReadLine();
        }
    }
}
