﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
    public class Person : ITest
    {
        public void Print()
        {
            string input = "This is comes from Person Class";
            Console.WriteLine(input);
            
        }
    }
}
