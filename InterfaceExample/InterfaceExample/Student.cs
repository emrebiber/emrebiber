﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceExample
{
    public class Student : ITest
    {
        public void Print()
        {
            Console.WriteLine("Comes from Student Class");
        }
    }
}
