﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVDLibrary.MODELS;

namespace MVC_DVDLibrary.DATA.Interfaces
{
    public interface IMovieRepository
    {
        List<Movie> List();
        Movie GetOneSpecificMovie(int movieId);
        Movie GetOneSpecificMovieByTitle(string title);
        void Add(Movie movie);
        void Edit(Movie movie);
        void Delete(int movieId);
        void Search(string title);
        

    }
}
