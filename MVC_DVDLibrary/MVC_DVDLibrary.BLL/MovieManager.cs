﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_DVDLibrary.DATA.Factories;
using MVC_DVDLibrary.DATA.Interfaces;
using MVC_DVDLibrary.DATA.Repositories;
using MVC_DVDLibrary.MODELS;

namespace MVC_DVDLibrary.BLL
{
    public class MovieManager
    {
        private IMovieRepository _movieRepository;

        public MovieManager()
        {
            _movieRepository = MovieRepositoryFactory.GetRightRepository();
        }

        public List<Movie> ListAllDvd()
        {
            var movies = _movieRepository.List();
            return movies;
        }

        public Movie GetOneDvd(int dvdId)
        {
            var movie = _movieRepository.GetOneSpecificMovie(dvdId);
            return movie;
        }

        public Movie GetOneDvdByTitle(string title)
        {
            var movie = _movieRepository.GetOneSpecificMovieByTitle(title);
            return movie;
        }

        public void AddDvd(Movie movie)
        {
            _movieRepository.Add(movie);
        }

        public void EditDvd(Movie movie)
        {
            _movieRepository.Edit(movie);
        }

        public void DeleteDvd(int dvdId)
        {
            _movieRepository.Delete(dvdId);
        }

        public void SearchDvd(string title)
        {
            _movieRepository.Search(title);
        }
    }
}
