﻿var url = '/api/Movie/';

$(document)
    .ready(function () {
        $('#addMovieForm')
                        .validate({
                            rules: {
                                dvdId: {
                                    required: true,
                                    number: true
                                },
                                title: {
                                    required: true
                                },
                                releaseDate: {
                                    required: true,
                                    date: true
                                },
                                MpaaRating: {
                                    required: true
                                },
                                directorName: {
                                    required: true
                                },
                                studio: {
                                    required: true
                                },
                                userRating: {
                                    required: true,
                                    number: true,  
                                },
                                userNotes: {
                                    required: true
                                },
                                mainActors: {
                                    required: true
                                }
                            }
                          
                        });

        $('#BtnAddMovie')
            .on('click',
                function () {
                    $('#dvdId').val('');
                    $('#title').val('');
                    $('#releaseDate').val('');
                    $('#mpaaRating').val('');
                    $('#directorName').val('');
                    $('#studio').val('');
                    $('#userRating').val('');
                    $('#userNotes').val('');
                    $('#mainActors').val('');
                    $('#myAddModal').modal('show');

                });

        $('#BtnSaveContent')
            .on('click',
                function() {
                    var movie = {};
                    movie.DVDId = $('#dvdId').val();
                    movie.Title = $('#title').val();
                    movie.ReleaseDate = $('#releaseDate').val();
                    movie.MpaaRating = $('#addRadio input[name=rating]:checked').val();
                    movie.DirectorName = $('#directorName').val();
                    movie.Studio = $('#studio').val();
                    movie.UserRating = $('#userRating').val();
                    movie.UserNotes = $('#userNotes').val();
                    movie.ActorsInMovie = $('#mainActors').val();
                    if ($("#addMovieForm").valid() === false) {
                        alert('Something bad');
                        return;
                    }
                    $.post(url, movie)
                        .done(function() {
                            loadMovies();
                            $('#myAddModal').modal('hide');
                        })
                        .fail(function(jqXhr, status, err) {
                            alert(status + " - " + err);
                        });

                });
    });




        
