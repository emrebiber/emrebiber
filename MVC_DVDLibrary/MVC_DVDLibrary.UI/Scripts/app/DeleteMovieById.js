﻿
//var url = '/api/Movie/';

$(document)
    .ready(function() {
        $('#BtnDeleteMovie')
            .on('click',
                function () {
                    $('#dvdDeleteId').val('');
                    $('#myDeleteModal').modal('show');
                    
                });

        $('#BtnConfirmDelete')
            .on('click',
                function () {
                    var id = $('#dvdDeleteId').val();
                    $.ajax({
                        url: '/api/Movie/' + id,
                        type: 'DELETE',
                        success: function (movie) {
                            deleteRow(movie);
                            loadMovies();

                        },
                        error: function(xhr, status, err) {
                            alert('error1: ' + err);
                        }
                    });

                    $('#myDeleteModal').modal('hide');
                });
    });

function deleteRow(movie) {
    $('#MovieTable tbody tr').remove(movie);
}