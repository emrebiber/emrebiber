﻿
var url = '/api/Movie/';

$(document)
    .ready(function() {
        $('#BtnSearchMovie')
            .on('click',
                function() {
                    $('searchId').val('');
                    $('#mySearchModal').modal('show');
                    $('#searchMovieForm')
                        .validate({
                            rules: {
                                searchId: {
                                    required: true,
                                    number: true
                                }
                            }
                        });
                });

        $('#BtnSaveSearchContent')
            .on('click',
                function () {
                    var id = $('#searchId').val();
                    $.getJSON(url + id)
                        .done(function (data) {
                            $('#modalSearchTable tbody tr, #modalSearchTable thead tr').remove();
                            $(createHeader()).appendTo($('#modalSearchTable thead'));
                            $(createRow(data)).appendTo($('#modalSearchTable tbody'));
                            //$('#mySearchModal').modal('hide');
                        })
                        .fail(function(jqXhr, status, err) {
                            alert(status + " - " + err);
                        });
                   
                  
                });

    });


