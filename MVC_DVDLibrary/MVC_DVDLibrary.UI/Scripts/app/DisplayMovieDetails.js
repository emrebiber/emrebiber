﻿var url = '/api/Movie/';

$(document)
    .ready(function() {
        $('#BtnDisplayMovie')
            .on('click',
                function () {
                    $('#dvdDisplayId').val('');
                    $('#myDisplayModal').modal('show');
                    $('#displayMovieForm')
                        .validate({
                            rules: {
                                dvdDisplayId: {
                                    required: true,
                                    number: true
                                }

                            }
                        });
                    
                    $('#BtnDisplayConfirm')
                        .on('click',
                            function () {
                                var id = $('#dvdDisplayId').val();
                                $.getJSON(url + id)
                                    .done(function(data) {
                                        $('#modalDisplayTable tbody tr, #modalDisplayTable thead tr' ).remove();
                                        $(createHeader()).appendTo($('#modalDisplayTable thead'));
                                        $(createRow(data)).appendTo($('#modalDisplayTable tbody'));
                                        $('#modalDisplayModal').modal('hide');
                                       
                                    })
                                    .fail(function(jqXhr, status, err) {
                                        alert('error1: ' + err);
                                    });


                            });
                   
                                });
                        });
                   

function createHeader() {
    return '<tr><th>ID</th><th>Title</th><th>Date</th><th>Rating<tr><th>Director</th><th>' +
        'Studio</th><th>User Rating</th><th>Notes</th><th></th><th>Actors</th></tr>';

}