﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.AccessControl;
using System.Web.Http;
using MVC_DVDLibrary.BLL;
using MVC_DVDLibrary.MODELS;

namespace MVC_DVDLibrary.UI.Controllers
{
    public class MovieController : ApiController
    {
        public List<Movie> Get()
        {
            var manager = new MovieManager();
            var movies = manager.ListAllDvd();
            return movies;
        }

        public Movie Get(int id)
        {
            var manager = new MovieManager();
            var movie = manager.GetOneDvd(id);
            return movie;
        }

        public HttpResponseMessage Post(Movie newMovie)
        {
            var manager = new MovieManager();
            manager.AddDvd(newMovie);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        public Movie Get(string title)
        {
            var manager = new MovieManager();
            var movie = manager.GetOneDvdByTitle(title);
            return movie;
        }

        public HttpResponseMessage Delete(int id)
        {
            var manager = new MovieManager();
            manager.DeleteDvd(id);
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;

        }



    }
}
