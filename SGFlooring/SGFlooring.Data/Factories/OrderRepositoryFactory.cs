﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Data.Repositories;

namespace SGFlooring.Data.Factories
{
    public static class OrderRepositoryFactory
    {
        public static IOrderRepository GetOrderRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new OrderFileRepository();
                case "Test":
                    return new MockOrderRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
