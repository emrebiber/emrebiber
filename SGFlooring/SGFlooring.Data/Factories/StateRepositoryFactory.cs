﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Data.Repositories;

namespace SGFlooring.Data.Factories
{
    public static class StateRepositoryFactory
    {
        public static IStateRepository GetStateRepository()
        {
            var mode = ConfigurationManager.AppSettings["Mode"];

            switch (mode)
            {
                case "Prod":
                    return new StateFileRepository();
                case "Test":
                    return new MockStateRepository();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
