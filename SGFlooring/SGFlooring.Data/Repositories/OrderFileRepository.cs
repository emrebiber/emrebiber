﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Data.Interfaces;
using SGFlooring.Models;

namespace SGFlooring.Data.Repositories
{
    public class OrderFileRepository : IOrderRepository
    {
        public List<Order> ListOrders()
        {
            throw new NotImplementedException();
        }

        public Order GetOrder(int orderNumber)
        {
            throw new NotImplementedException();
        }

        public void AddOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public void EditOrder(Order order)
        {
            throw new NotImplementedException();
        }

        public void RemoveOrder(int orderNumber)
        {
            throw new NotImplementedException();
        }
    }
}
