﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IStateRepository
    {
        State GetState(string stateAbbreviation);
    }
}
