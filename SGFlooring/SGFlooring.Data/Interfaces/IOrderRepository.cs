﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGFlooring.Models;

namespace SGFlooring.Data.Interfaces
{
    public interface IOrderRepository
    {
        List<Order> ListOrders();
        Order GetOrder(int orderNumber);
        void AddOrder(Order order);
        void EditOrder(Order order);
        void RemoveOrder(int orderNumber);
    }
}
