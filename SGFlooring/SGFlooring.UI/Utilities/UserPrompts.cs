﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGFlooring.UI.Utilities
{
    public class UserPrompts
    {

        private const string _seperatorBar = "==================================================";
        private const string _headerFormat = "{0, 2} {1, -20} {2, -15} {3, 5}";

        public static int GetIntegerFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                int output;

                if (int.TryParse(input, out output))
                {
                    return output;
                }
                
                PressKeyToContinue();

            } while (true);
            
        }

        public static decimal GetDecimalFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();
                decimal output;

                if (decimal.TryParse(input, out output))
                {
                    return output;
                }

                PressKeyToContinue();

            } while (true);


        }

        public static string GetStringFromUser(string prompt)
        {
            do
            {
                Console.Clear();
                Console.WriteLine(prompt);
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter valid string!");
                    PressKeyToContinue();
                }
                else
                {
                    return input;
                }
            } while (true);
            

           
        }

        public static string GetYesNoAnswerFromUser(string prompt)
        {
            while (true)
            {
                Console.Write(prompt + "(Y/N)? ");
                string input = Console.ReadLine();

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine("You must enter Y/N");
                    PressKeyToContinue();
                }
                else
                {
                    if (input != "Y" && input != "N")
                    {
                        Console.WriteLine("You must enter Y/N");
                        PressKeyToContinue();
                        continue;
                    }

                    return input;
                }
            }
        }

        public static void PressKeyToContinue()
        {
            Console.WriteLine("Press any to continue...");
            Console.ReadKey();
        }

    }
}
