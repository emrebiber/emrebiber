﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGFlooring.Models
{
    public class Order
    {
        public int OrderNumber { get; set; }
        public string CustomerName { get; set; }
        public string StateName { get; set; }
        public decimal TaxRate { get; set; }
        public string ProductType { get; set; }
        public decimal OrderArea { get; set; }
        public decimal CostPerSquareFoot { get; set; }
        public decimal LaborCostPerSquareFoot { get; set; }
        public decimal MaterialCost { get; set; }
        public decimal LaborCost { get; set; }
        public decimal OrderTax { get; set; }
        public decimal OrderTotalCost { get; set; }
    }
}
