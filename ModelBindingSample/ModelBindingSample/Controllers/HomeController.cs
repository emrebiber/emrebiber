﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModelBindingSample.Models;

namespace ModelBindingSample.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddPersonForm()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult RequestFromPost()
        {
            var model = new Person();
            model.PersonId = int.Parse(Request.Form["PersonId"]);
            model.FirstName = Request.Form["FirstName"];
            model.LastName = Request.Form["LastName"];

            return View("AddPerson", model);
        }

        [HttpGet]
        public ActionResult AddPersonForm2()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult AddPersonForm2(Person person)
        {
            return View("AddPerson", person);
        }

        [HttpGet]
        public ActionResult SimpleBindingOfTypes()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SimpleBindingOfTypesResponse(int id, string name, int age)
        {
            ViewBag.Id = id;
            ViewBag.Name = name;
            ViewBag.Age = age;

            return View();
        }

        [HttpGet]
        public ActionResult BindComplexTypes()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult BindComplexTypes(Person person)
        {
            return View("ComplexResult", person);
        }

        [HttpGet]
        public ActionResult BindMultipleObject()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BindMultipleObject(List<Address> addresses)
        {
            return View("MultipleObjects", addresses);
        }

    }
}