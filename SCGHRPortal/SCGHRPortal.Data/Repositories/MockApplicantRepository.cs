﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Repositories
{
    public class MockApplicantRepository : IApplicantRepository
    {
        private static List<Applicant> _applicants;

        static MockApplicantRepository()
        {
            _applicants = new List<Applicant>()
            {
                new Applicant()
                {
                    ApplicantId = 1,
                    ApplicantFirstName = "Emre",
                    ApplicantLastName = "Biber",
                    Address1 = "123 Main Street",
                    City = "Akron",
                    State = "Ohio",
                    Zip = "44311",
                    Country = "United States",
                    PhoneNumber = "870-966-3322",
                    Email = "emrebiber@gmail.com",
                },

                new Applicant()
                {
                    ApplicantId = 2,
                    ApplicantFirstName = "Tom",
                    ApplicantLastName = "Jerry",
                    Address1 = "100 Sesame Street",
                    City = "Columbus",
                    State = "Ohio",
                    Zip = "44311",
                    Country = "United States",
                    PhoneNumber = "501-123-4567",
                    Email = "tomjerry@hotmail.com",
                },

                new Applicant()
                {
                    ApplicantId = 3,
                    ApplicantFirstName = "John",
                    ApplicantLastName = "Smith",
                    Address1 = "328 S Malibu Street",
                    City = "Little Rock",
                    State = "Arkansas",
                    Zip = "72401",
                    Country = "United States",
                    PhoneNumber = "402-555-7558",
                    Email = "john.smith@gmail.com",
                }
            };
        }


        public List<Applicant> GetAll()
        {
            return _applicants;
        }

        public Applicant Get(int applicantId)
        {
            return _applicants.FirstOrDefault(a => a.ApplicantId == applicantId);
        }

        public void Add(Applicant applicant)
        {
            _applicants.Add(applicant);
        }

        public void Edit(Applicant applicant)
        {
            var selectedApplicant = _applicants.First(a => a.ApplicantId == applicant.ApplicantId);

            selectedApplicant.ApplicantFirstName = applicant.ApplicantFirstName;
            selectedApplicant.ApplicantLastName = applicant.ApplicantLastName;
            selectedApplicant.Address1 = applicant.Address1;
            selectedApplicant.Address2 = applicant.Address2;
            selectedApplicant.City = applicant.City;
            selectedApplicant.State = applicant.State;
            selectedApplicant.Zip = applicant.Zip;
            selectedApplicant.Country = applicant.Country;
            selectedApplicant.PhoneNumber = applicant.PhoneNumber;
            selectedApplicant.Email = applicant.Email;
        }

        public void Delete(int applicantId)
        {
            _applicants.RemoveAll(a => a.ApplicantId == applicantId);
        }
    }
}
