﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Repositories
{
    public class MockCategoryRepository : ICategoryRepository
    {
        private static List<Category> _categories;

        static MockCategoryRepository()
        {
            _categories = new List<Category>()
            {
                new Category() {CategoryId = 1, CategoryName = "Financial"},
                new Category() {CategoryId = 2, CategoryName = "Benefits"},
                new Category() {CategoryId = 3, CategoryName = "Compensation"},
                new Category() {CategoryId = 4, CategoryName = "Performance and Discipline"},
                new Category() {CategoryId = 5, CategoryName = "Time Off"},
                new Category() {CategoryId = 6, CategoryName = "Other"},
            };
        }


        public List<Category> GetAll()
        {
            return _categories;
        }

        public Category Get(int categoryId)
        {
            return _categories.FirstOrDefault(a => a.CategoryId == categoryId);
        }

        public void Add(Category category)
        {
            _categories.Add(category);
        }

        public void Edit(Category category)
        {
            var selectedCategory = _categories.First(p => p.CategoryId == category.CategoryId);

            selectedCategory.CategoryName = category.CategoryName;
           
        }

        public void Delete(int categoryId)
        {
            _categories.RemoveAll(p => p.CategoryId == categoryId);
        }   
    }
}
