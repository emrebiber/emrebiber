﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Repositories
{
    public class MockPolicyRepository : IPolicyRepository
    {
        private static List<Policy> _policies;

        static MockPolicyRepository()
        {
            _policies = new List<Policy>()
            {
                new Policy()
                {
                    PolicyId = 1,
                    PolicyName = "Dress Code",
                    Description = "This policy defines the how to wear properly at work enviroment"
                },
                new Policy()
                {
                    PolicyId = 2,
                    PolicyName = "Bonus Payment ",
                    Description = "This policy defines the nature and purpose of Bonus Payments, which will be considered to " +
                                  "recognise outstanding performance or on achievement of negotiated stretch outcomes. " +
                                  "It sets out eligibility criteria and other conditions that apply."
                },
                new Policy()
                {
                    PolicyId = 3,
                    PolicyName = "Employee Benefits",
                    Description = "defines that what kind benefits employee will have."
                },
            };
        }

        public List<Policy> GetAll()
        {
            return _policies;;
        }

        public Policy Get(int policyId)
        {
            return _policies.FirstOrDefault(p => p.PolicyId == policyId);
        }

        public void Add(Policy policy)
        {
            _policies.Add(policy);
        }

        public void Edit(Policy policy)
        {
            var selectedPolicy = _policies.First(p => p.PolicyId == policy.PolicyId);

            selectedPolicy.PolicyName = policy.PolicyName;
            selectedPolicy.Description = policy.Description;
        }

        public void Delete(int policyId)
        {
            _policies.RemoveAll(p => p.PolicyId == policyId);
        }
    }
}
