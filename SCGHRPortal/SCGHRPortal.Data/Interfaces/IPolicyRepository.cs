﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Interfaces
{
    public interface IPolicyRepository
    {
        List<Policy> GetAll();
        Policy Get(int policyId);
        void Add(Policy policy);
        void Edit(Policy policy);
        void Delete(int policyId);

    }
}
