﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Interfaces
{
    public interface IApplicantRepository
    {
        List<Applicant> GetAll();
        Applicant Get(int applicantId);
        void Add(Applicant applicant);
        void Edit(Applicant applicant);
        void Delete(int applicantId);

    }
}
