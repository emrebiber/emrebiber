﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Models;

namespace SCGHRPortal.Data.Interfaces
{
    public interface ICategoryRepository
    {
        List<Category> GetAll();
        Category Get(int categoryId);
        void Add(Category category);
        void Edit(Category category);
        void Delete(int categoryId);
    }
}
