﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SCGHRPortal.Data.Repositories;
using SCGHRPortal.Models;

namespace SCGHRPortal.Tests
{
    [TestFixture]
    public class PolicyTests
    {
        [Test]
        public void CanGetAllApplicants()
        {
            var repo = new MockApplicantRepository();
            var applicant = repo.GetAll();
            Assert.AreEqual(3, applicant.Count);

           
            var check = applicant[0];
            Assert.AreEqual(1, check.ApplicantId);
            Assert.AreEqual("Emre", check.ApplicantFirstName);
            Assert.AreEqual("Biber", check.ApplicantLastName);
            Assert.AreEqual("123 Main Street",check.Address1);
            Assert.AreEqual("Akron", check.City);
            Assert.AreEqual("Ohio", check.State);
            Assert.AreEqual("44311", check.Zip);
            Assert.AreEqual("United States", check.Country);
            Assert.AreEqual("870-966-3322", check.PhoneNumber);
            Assert.AreEqual("emrebiber@gmail.com", check.Email);
        }

        [TestCase(1,"Emre","Biber","123 Main Street","Akron","44311","emrebiber@gmail.com")]
        public void CanGetSpecificApplicant(int applicantId, string firstName,string lastName,string address, string city, string zip,string email)
        {
            var repo = new MockApplicantRepository();
            var applicant = repo.Get(applicantId);

            Assert.AreEqual(1, applicant.ApplicantId);
            Assert.AreEqual(firstName, applicant.ApplicantFirstName);
            Assert.AreEqual(lastName, applicant.ApplicantLastName);
            Assert.AreEqual(address, applicant.Address1);
            Assert.AreEqual(city, applicant.City);
            Assert.AreEqual(zip, applicant.Zip);
            Assert.AreEqual(email, applicant.Email);
        }

       

    }
}
