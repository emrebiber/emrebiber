﻿$(document)
    .ready(function () {
        $('#applicationForm')
            .validate({
                rules: {
                    ApplicantFirstName: {
                        required: true
                    },
                    ApplicantLastName: {
                        required: true
                    },
                    Address1: {
                        required: true
                    },
                    City: {
                        required: true
                        
                    },
                    State: {
                        required: true
                    },
                    Zip: {
                        required: true,
                        minlength: 5,
                        maxlength: 5,
                        digits: true
                    }
                },
                messages: {
                    ApplicantFirstName: "Enter your first name",
                    ApplicationLastName: "Enter your last name",
                    Address1: "Enter your address",
                    City: "Enter your city",
                    State: "Enter your state",
                    Zip: {
                        required: "Enter your zip code",
                        minlength: "Your zip code must be 5 digits",
                        maxlength: "Your zip cide must be 5 digits",
                        digits: "Your zip code must consist of digits"

                    },
                    Email: {
                        required: "Type your email!!!",
                        email: "That's not the right way to type your email!!!"
                    }

                   
                }

            });
    });

$("#applicationForm").validate({
    rules: {
        PhoneNumber: {
            required: true,
            phoneUS: true
        }
    }
});

$("#applicationForm").validate({
    rules: {
        Email: {
            required: true,
            email: true
        }
    }
});

$("#applicationForm").validate({
    rules: {
        TermsAccepted: {
            required: true
        }
    }
});