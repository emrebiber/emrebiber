﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCGHRPortal.BLL;
using SCGHRPortal.Models;

namespace SCGHRPortal.Controllers
{
    public class ApplicantController : Controller
    {
        // GET: Applicant
        public ActionResult ListApplicants()
        {
            var manager = new ApplicantManager();
            var applicant = manager.ListAllApplicants();

            return View(applicant.ToList());
        }

        [HttpGet]
        public ActionResult AddApplicant()
        {
           
            return View(new Applicant());
        }

        [HttpPost]
        public ActionResult AddApplicant(Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                //var manager = new ApplicantManager();
                //manager.AddApplicant(applicant);
                return View("CompletedApplication", applicant);
            }
            else
            {
                return View(applicant);
            }
            

           
        }
    }
}