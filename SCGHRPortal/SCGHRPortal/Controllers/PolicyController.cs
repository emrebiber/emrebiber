﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SCGHRPortal.BLL;
using SCGHRPortal.Models;

namespace SCGHRPortal.Controllers
{
    public class PolicyController : Controller
    {
        //// GET: Policy
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public  ActionResult ListPolicies ()
        {
            var manager = new  PolicyManager();
            var policy = manager.GetAllPolicies();

           return View(policy.ToList());
        }

        [HttpGet]
        public ActionResult AddPolicy()
        {
            return View(new Policy());
        }

        [HttpPost]
        public ActionResult AddPolicy(Policy policy)
        {
            var manager = new PolicyManager();
            manager.AddPolicy(policy);

            return  RedirectToAction("ListPolicies");
        }

        [HttpGet]
        public ActionResult ListCategories()
        {
            var manager = new CategoryManager();
            var categories = manager.GetAllCategories();

            return View(categories.ToList());
        }

        [HttpGet]
        public ActionResult AddCategory()
        {
            return View(new Category());
        }

        [HttpPost]
        public ActionResult AddCategory(Category category)
        {
            var manager = new CategoryManager();
            manager.AddCategory(category);

            return RedirectToAction("ListCategories");
        }

        [HttpGet]
        public ActionResult EditCategory(int categoryId)
        {
            var manager = new CategoryManager();
            var category = manager.GetOneSpecificCategory(categoryId);

            return View(category);
        }

        [HttpPost]
        public ActionResult EditCategory(Category category)
        {
            var manager = new CategoryManager();
            manager.EditCategory(category);

            return RedirectToAction("AddCategory");
        }

       



    }
}