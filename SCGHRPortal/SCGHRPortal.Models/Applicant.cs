﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCGHRPortal.Models
{
    public class Applicant
    {

        public int ApplicantId { get; set; }
        [DisplayName("First Name")]
        public string ApplicantFirstName { get; set; }
        [DisplayName("Last Name")]
        public string ApplicantLastName { get; set; }
        [DisplayName("Adress")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool TermsAccepted { get; set; }


    }
}
