﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCGHRPortal.Models
{
    public class Policy
    {
        public int PolicyId { get; set; }
        [DisplayName("Policy Name")]
        public string PolicyName { get; set; }
        public string Description { get; set; }
    }
}
