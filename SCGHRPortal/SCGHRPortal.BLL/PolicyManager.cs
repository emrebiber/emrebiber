﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Data.Repositories;
using SCGHRPortal.Models;

namespace SCGHRPortal.BLL
{
    public class PolicyManager
    {
        private static IPolicyRepository _policyRepository;

        public PolicyManager()
        {
            _policyRepository = new MockPolicyRepository();
        }

        public List<Policy> GetAllPolicies()
        {
             var policies = _policyRepository.GetAll();

            return policies;
        }

        public Policy GetOneSpecificPolicy(int policyId)
        {
            var policy = _policyRepository.Get(policyId);

            return policy;
        }

        public void AddPolicy(Policy policy)
        {
            _policyRepository.Add(policy);
        }

        public void EditPolicy(Policy policy)
        {
            _policyRepository.Edit(policy);

        }

        public void DeletePolicy(int policyId)
        {
            _policyRepository.Delete(policyId);
        }
    }
}
