﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Data.Repositories;
using SCGHRPortal.Models;

namespace SCGHRPortal.BLL
{
    public class CategoryManager
    {
        private static ICategoryRepository _categoryRepository;

        public CategoryManager()
        {
            _categoryRepository = new MockCategoryRepository();
        }

        public List<Category> GetAllCategories()
        {
            var categories = _categoryRepository.GetAll();

            return categories;
        }

        public Category GetOneSpecificCategory(int categoryId)
        {
            var category = _categoryRepository.Get(categoryId);

            return category;
        }

        public void AddCategory(Category category)
        {
            _categoryRepository.Add(category);
        }

        public void EditCategory(Category category)
        {
            _categoryRepository.Edit(category);

        }

        public void DeleteCategory(int categoryId)
        {
            _categoryRepository.Delete(categoryId);
        }
    }
}
