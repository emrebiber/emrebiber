﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCGHRPortal.Data.Interfaces;
using SCGHRPortal.Data.Repositories;
using SCGHRPortal.Models;

namespace SCGHRPortal.BLL
{
    public class ApplicantManager
    {
        private  IApplicantRepository _applicantRepository;

        public ApplicantManager()
        {
            _applicantRepository = new MockApplicantRepository();
        }

        public List<Applicant>ListAllApplicants()
        {
            var applicants = _applicantRepository.GetAll();

            return applicants;
        }

        public Applicant GetOneSpecificApplicant(int applicantId)
        {
            var applicant = _applicantRepository.Get(applicantId);

            return applicant;
        }

        public void AddApplicant(Applicant applicant)
        {
            _applicantRepository.Add(applicant);
        }

        public void EditApplicant(Applicant applicant)
        {
            _applicantRepository.Edit(applicant);
        }

        public void DeleteApplicant(int applicanId)
        {
            _applicantRepository.Delete(applicanId);
        }
    }
}
