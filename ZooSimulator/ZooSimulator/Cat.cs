﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulator
{
    public class Cat : Animal
    {
        public Cat()
        {
            Name = "I'm a Cat";
            Energy = 1;
            Hunger = 20;
            EatingType = FoodType.HumanMadeFood;

        }

        public override int Feed()
        {
            return Hunger -= 2;
        }

        public override void Sleep()
        {
            Energy += 20;
        }
    }
}
