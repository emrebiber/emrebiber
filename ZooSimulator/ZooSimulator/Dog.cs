﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulator
{
    public class Dog : Animal
    {

        public Dog()
        {
            Name = "I'm a Dog";
            Energy = 5;
            Hunger = 10;
            EatingType = FoodType.HumanMadeFood;
        }

        public override int Feed()
        {
            return Hunger -= 5;
        }

        public override void Sleep()
        {
            Energy += 5;
            Hunger +=2;
        }

        public static void Fetch()
        {
            Console.WriteLine("Throw the ball. I will bring it for you..");
        }


    }
}
