﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulator
{
    public class Animal
    {
        public string Name { get; set; }
        public int Energy { get; set; }
        public int Hunger { get; set; }
        public FoodType EatingType { get; set; }

        public enum FoodType
        {
            Grass,
            Meat,
            HumanMadeFood
        }

        public virtual int Hunt()
        {
            return Energy--;
        }

        public virtual void Sleep()
        {
            Energy += 10;
            Hunger += 5;
        }

        public virtual int Feed()
        {
            return Hunger -= 1;
        }

        public static void MakeNoise()
        {
            Console.WriteLine("This is comes from Animal Class..");
        }

        public static void Eat()
        {
            Console.WriteLine("Animals love to eat");
        }

      

        
    }
}
