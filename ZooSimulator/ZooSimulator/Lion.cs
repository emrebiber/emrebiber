﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZooSimulator
{
    public class Lion : Animal
    {

        public Lion() 
        {
            Name = "I'm a Lion";
            Energy = 50;
            Hunger = 40;
            EatingType = FoodType.Meat;
        }

        public override int Feed()
        {
            return Hunger -= 10;
        }

        
    }
}
