﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZooSimulator
{
    class Program
    {
        static void Main(string[] args)
        {

            Dog dog = new Dog();
            
            Console.WriteLine(dog.Name);
            Console.WriteLine("My Hunger level is: {0} ",dog.Hunger);
            Console.WriteLine("My Energy level is: {0} ", dog.Energy);
            dog.Feed();
            Console.WriteLine("Now my Hunger level is: {0}", dog.Hunger);
            dog.Sleep();
            Console.WriteLine("After I sleep my Energy level {0} and my Hunger level {1}", dog.Energy,dog.Hunger);

            Console.WriteLine("\n_______________________________________________________________________________\n\n");

            Cat cat = new Cat();

            Console.WriteLine(cat.Name);
            Console.WriteLine("My Hunger level is: {0} ", cat.Hunger);
            Console.WriteLine("My Energy level is: {0} ", cat.Energy);
            cat.Feed();
            Console.WriteLine("Now my Hunger level is: {0}", cat.Hunger);
            cat.Sleep();
            Console.WriteLine("After I sleep my Energy level {0} and my Hunger level {1}", cat.Energy, cat.Hunger);

            Console.WriteLine("\n_______________________________________________________________________________\n\n");

            Lion lion = new Lion();

            Console.WriteLine(lion.Name);
            Console.WriteLine("My Hunger level is: {0} ", lion.Hunger);
            Console.WriteLine("My Energy level is: {0} ", lion.Energy);
            lion.Feed();
            Console.WriteLine("Now my Hunger level is: {0}", lion.Hunger);
            lion.Sleep();
            Console.WriteLine("After I sleep my Energy level {0} and my Hunger level {1}", lion.Energy, lion.Hunger);
            

            Console.ReadLine();
        }
    }
}
