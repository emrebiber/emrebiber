﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BubbleSort
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] numbers = {3, 7, 2, 4, 7, 12};
            int temp;

            for (int i = 1; i < numbers.Length; i++)
            {
                for (int j = 0; j < numbers.Length - 1; j++)
                {
                    if (numbers[j] > numbers[j + 1])
                    {
                        temp = numbers[j];
                        numbers[j] = numbers[j + 1];
                        numbers[j + 1] = temp;
                    }

                }
            }

            foreach (var sortedVersion in numbers)
            {
                Console.WriteLine("Sorted array is: {0}", sortedVersion);
            }

            

            Console.ReadLine();




        }
    }
}
