﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;

namespace BattleShip.UI.WorkFlow
{
    public class Player
    {
        public string PlayerName { get; set; }
        public Board PlayerBoard { get; set; }

        public Player()
        {
            PlayerBoard = new Board();
        }
    }
}
