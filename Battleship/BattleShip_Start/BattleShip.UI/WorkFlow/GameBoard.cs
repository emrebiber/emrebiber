﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI.WorkFlow
{
    public class GameBoard
    {


        public static void ShowShotHistory(Player player)
        {

            // i = col, j = row

            string[] topArray = new string[]
            {"    ", " A ", " B ", " C ", " D ", " E ", " F ", " G ", " H ", " I ", " J "};

            Console.Write("{0}, you are attacking\n\n", player.PlayerName);

            for (int i = 0; i < 10; i++)
            {
                Console.Write(topArray[i]);
            }
            Console.Write("\n______________________________");

            for (int j = 0; j <= 10; j++)
            {
                if (j < 10)
                {
                    Console.Write(" \n {0} | ", j);
                }
                for (int i = 0; i <= 10; i++)
                {
                    Coordinate coordinate = new Coordinate(i, j);
                    if (player.PlayerBoard.ShotHistory.ContainsKey(coordinate))
                    {
                        switch (player.PlayerBoard.ShotHistory[coordinate])
                        {
                            case ShotHistory.Hit:
                                Console.Write("H");
                                break;
                            case ShotHistory.Miss:
                                Console.Write("M");
                                break;
                        }
                    }
                    else
                        Console.Write("-");
                }
            }
        }

        public static void ShowShips(Player player)
        {
            // i = col, j = row

            string[] topArray = new string[]
            {"    ", " A ", " B ", " C ", " D ", " E ", " F ", " G ", " H ", " I ", " J "};

            Console.Write("{0}'s ships\n\n\n", player.PlayerName.ToUpper());

            for (int i = 0; i < 10; i++)
            {
                Console.Write(topArray[i]);
            }
            Console.Write("\n______________________________");

            for (int j = 0; j < 10; j++)
            {
                if (j < 10)
                    Console.Write(" \n {0} | ", j);

                for (int i = 0; i < 10; i++)
                {
                    Coordinate coordinate = new Coordinate(i, j);
                    if (player.PlayerBoard.ShotHistory.ContainsKey(coordinate))
                    {
                        switch (player.PlayerBoard.ShotHistory[coordinate])
                        {
                            case ShotHistory.Hit:
                                Console.Write("H");
                                break;
                            case ShotHistory.Miss:
                                Console.Write("M");
                                break;
                        }
                    }
                }
            }
            Console.WriteLine("\n");
        }
    }
}