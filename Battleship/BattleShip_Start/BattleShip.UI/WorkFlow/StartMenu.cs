﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;

namespace BattleShip.UI.WorkFlow
{
    public class StartMenu
    {

       

        public string GetPlayerName(string userName)
        {
            Console.WriteLine("{0} Enter your name: ", userName);
            userName = Console.ReadLine();

            return userName;
        }

        public static void PlayerTurn(Player p1, Player p2)
        {
            bool flagForVictory = false;
            Player currentPlayer = p1;
            Player nextPlayer = p2;

            while (!flagForVictory)
            {
                GameBoard.ShowShotHistory(nextPlayer);
                GameBoard.ShowShips(currentPlayer);
                Coordinate coordinate;
                bool flagForCoordinate = false;
                FireShotResponse response = new FireShotResponse();
                Console.WriteLine("Player {0}'s turn" , currentPlayer.PlayerName);

                do
                {
                    Console.WriteLine("{0} Enter coordinate to shoot", currentPlayer.PlayerName);
                    string userInput = Console.ReadLine();
                    coordinate = ConvertUserInputToCoordinate(userInput);
                    flagForCoordinate = CheckForAvaiblableCoordinate(coordinate);

                    if (!flagForCoordinate)
                    {
                        Console.WriteLine("NOT valid. Try Again!");
                    }
                    else
                    {
                        response = nextPlayer.PlayerBoard.FireShot(coordinate);

                        switch (response.ShotStatus)
                        {
                            case ShotStatus.Hit:
                                Console.WriteLine("It's a HIT!!!");
                                break;
                            case ShotStatus.Miss:
                                Console.WriteLine("It's a MISS!!!");
                                break;
                            case ShotStatus.HitAndSunk:
                                Console.WriteLine("You SANK the {0} kind of ship.", response.ShipImpacted);
                                break;
                            case ShotStatus.Duplicate:
                                Console.WriteLine("You tried that coordinate. Try again!");
                                break;
                            case ShotStatus.Victory:
                                Console.WriteLine("Player {0}, You WON!!!", currentPlayer.PlayerName);
                                break;
                            case ShotStatus.Invalid:
                                Console.WriteLine("Invalid coordinate.Try again!");
                                break;

                            /* we might need to add condition that if shot is duplicated
                            * user should have another chance. response.shotstatus == status.duplicate
                            */

                        }
                    }
                 }while (!flagForCoordinate);

                if (response.ShotStatus == ShotStatus.Victory)
                {
                    flagForVictory = true;
                }
                else
                {
                    flagForVictory = false;
                    Player temp = currentPlayer;
                    currentPlayer = nextPlayer;
                    nextPlayer = temp;
                }
            }
        }
        public static Coordinate ConvertUserInputToCoordinate(string userInput)
        {
            int x = 0;
            int y = 0;
            if (userInput != "")
            {
                x = Convert.ToChar(userInput.Substring(0, 1).ToUpper());
                int.TryParse(userInput.Substring(1, userInput.Length - 1), out y);
                x = x - 64;

            }

            Coordinate coordinate = new Coordinate(x, y);
            return coordinate;
        }

        public static bool CheckForAvaiblableCoordinate(Coordinate coordinate)
        {
            if ((coordinate.XCoordinate > 0 && coordinate.XCoordinate < 11) &&
                (coordinate.YCoordinate > 0 && coordinate.XCoordinate < 11))
                return true;

            return false;
        }
    }
}