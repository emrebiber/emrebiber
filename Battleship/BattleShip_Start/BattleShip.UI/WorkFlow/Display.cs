﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI.WorkFlow
{
    public class Display
    {
        public static void HitMessageToUser()
        {
           // Console.Clear();
            Console.WriteLine("It's a HIT!!!");
        }

        public static void MissMessageToUser()
        {
            // Console.Clear();
            Console.WriteLine("It' a MISS!!!");
        }

        public static void SinkShipMessageToUser()
        {
            Console.WriteLine("You SINKED a ship");
        }

        public static void VictoryMessageToUser()
        {
            Console.WriteLine("Congrats!! You WON!!");
        }

        public static void ChangeConsoleWindowSize()
        {
            int originalWidth, width, originalHeight, height;

            originalHeight = Console.WindowHeight;
            originalWidth = Console.WindowWidth;

            height = originalHeight * 2;
            width = originalWidth * 2;

            Console.SetWindowSize(width, height);

            



        }
        

    }
}
