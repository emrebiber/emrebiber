﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;


namespace BattleShip.UI.WorkFlow
{
    public class SetupShips
    {

        public static ShipType[] shipArray =
        {
            ShipType.Destroyer,
            ShipType.Battleship,
            ShipType.Carrier,
            ShipType.Cruiser,
            ShipType.Submarine
        };

        public static void PlayerPlaceShip(Player player)
        {
            Coordinate coordinate = new Coordinate(0, 0);
            bool validCoordinate = false;
            string userInput = "";

            PlaceShipRequest placeShipRequest = new PlaceShipRequest();
            string.Format("{0} Please place your ships!", player.PlayerName);

            for (int i = 0; i < shipArray.Length; i++)
            {
                ShipPlacement response;

                do
                {
                    Console.Clear();
                    GameBoard.ShowShips(player);
                    placeShipRequest.ShipType = shipArray[i];
                    do
                    {
                        Console.WriteLine("Enter coordinate for: {0}", shipArray[i]);
                        userInput = Console.ReadLine();

                        coordinate = StartMenu.ConvertUserInputToCoordinate(userInput);
                        validCoordinate = StartMenu.CheckForAvaiblableCoordinate(coordinate);

                    } while (!validCoordinate);

                    placeShipRequest.Coordinate = StartMenu.ConvertUserInputToCoordinate(userInput);


                    string direction;

                    do
                    {
                        Console.WriteLine("Enter direction to place: A=Left S=Down D=Right W=Up   ");
                        direction = Console.ReadLine().ToUpper();
                    } while (!(direction == "A" || direction == "S" || direction == "D" || direction == "W"));

                    switch (direction)
                    {
                        case "A":
                            placeShipRequest.Direction = ShipDirection.Left;
                            break;
                        case "S":
                            placeShipRequest.Direction = ShipDirection.Down;
                            break;
                        case "D":
                            placeShipRequest.Direction = ShipDirection.Right;
                            break;
                        case "W":
                            placeShipRequest.Direction = ShipDirection.Up;
                            break;
                    }

                    response = player.PlayerBoard.PlaceShip(placeShipRequest);

                    if (response != ShipPlacement.Ok)
                    {
                        Console.WriteLine("Placement was: {0}", response);
                        Console.ReadKey();
                    }
                } while (response != ShipPlacement.Ok);
            }

            Console.WriteLine("All ships are placed...");

        }
    }
}