﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.UI.WorkFlow;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "BattleShip!!!";
            Display.ChangeConsoleWindowSize();


            StartMenu menu = new StartMenu();

            Player p1 = new Player();
            Player p2 = new Player();

            p1.PlayerName = menu.GetPlayerName("Player1");
            SetupShips.PlayerPlaceShip(p1);

            p2.PlayerName = menu.GetPlayerName("Player2");
            SetupShips.PlayerPlaceShip(p2);

            Console.WriteLine("Press any key to start game...");
            Console.ReadKey();

            StartMenu.PlayerTurn(p1,p2);



            

            Console.ReadLine();


        }
    }
}
