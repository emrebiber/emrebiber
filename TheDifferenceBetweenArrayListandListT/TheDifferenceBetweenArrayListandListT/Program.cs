﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace TheDifferenceBetweenArrayListandListT
{
    class Program
    {
        static void Main(string[] args)
        {
           
            /*
            ArrayList arrayList = new ArrayList();

            arrayList.Add(12);
            arrayList.Add(35);
            arrayList.Add(5);
            arrayList.Add("Emre");

             
            int sumOfElements = 0;

            foreach (object elements in arrayList)
            {
                sumOfElements += (int)elements;
            }

            Console.WriteLine("The sum of the elements are: {0}", sumOfElements);
           */


          
            List<int> myList = new List<int>();
            myList.Add(12);
            myList.Add(35);
            myList.Add(5);
           // myList.Add("Emre");

            int sumOfElements = 0;

            foreach (int elements in myList)
            {
                sumOfElements += elements;
            }

            Console.WriteLine("The sum of elements of generic list: {0}", sumOfElements);

             



           

            Console.ReadLine();

        }
    }
}
