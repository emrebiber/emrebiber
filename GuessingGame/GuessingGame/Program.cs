﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;

            Random r = new Random();
            theAnswer = r.Next(1, 21);

            do
            {
                Console.Write("Enter your Guess: ");
                playerInput = Console.ReadLine();

                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (playerGuess == theAnswer)
                    {
                        Console.WriteLine("You Got It!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        if (playerGuess > theAnswer)
                            Console.WriteLine("Too High!");
                        else
                            Console.WriteLine("Too Low!");
                    }
                }
                else
                {
                    Console.WriteLine("That wasn't a valid number!");
                }
            } while (!isNumberGuessed);

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }
    }
}
