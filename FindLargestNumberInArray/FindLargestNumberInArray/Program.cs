﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace FindLargestNumberInArray
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] arr = new[] {125, 50, 25, 4, 326};
            int[] arr = new int[5];

            Console.WriteLine("Please enter five numbers: ");
            
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }
           
            int max = arr[0];
            int min = arr[0];

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > max)
                    max = arr[i];
            }

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                    min = arr[i];
            }

            Console.WriteLine("Largest number in the array is: {0}", max);
            Console.WriteLine("Smallest number in the array is: {0}", min);

            Console.ReadLine();
        }

        public static void PromptUserForInput(int[] array)
        {
            
            
        }

    }
}
