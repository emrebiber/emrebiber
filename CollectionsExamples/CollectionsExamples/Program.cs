﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsExamples
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowArrayList();
            //ShowHashTable();
            //ShowStack();
            //ShowQueue();

            //ShowGenericList();
            ShowGenericDictionary();

            Console.ReadLine();
        }

        static void ShowGenericDictionary()
        {
            Dictionary<int, Person> people = new Dictionary<int, Person>();

            Person p1 = new Person() {FirstName = "Homer", LastName = "Simpson", Age = 42, PersonId = 1};
            Person p2 = new Person() {FirstName = "Marge", LastName = "Simpson", Age = 40, PersonId = 2};
            Person p3 = new Person() {FirstName = "Lisa", LastName = "Simpson", Age = 12, PersonId = 3};
            Person p4= new Person() { FirstName = "Bart", LastName = "Simpson", Age = 9, PersonId = 4 };

            people.Add(p1.PersonId, p1);
            people.Add(p2.PersonId, p2);
            people.Add(p3.PersonId, p3);
            people.Add(p4.PersonId, p4);

            people.Remove(p1.PersonId);

            foreach (var key in people.Keys)
            {
                PrintPerson(people[key]);
            }

        }

        static void PrintPerson(Person p)
        {
            Console.WriteLine($"{p.PersonId} {p.FirstName} {p.LastName} {p.Age}");
        }

        static void ShowGenericList()
        {
            List<Person> people = new List<Person>()
            {
                new Person() {FirstName= "Jim", LastName = "Smith", Age = 30},
                new Person() {FirstName = "Sam", LastName = "Brown", Age = 35, PersonId = 4}
            };
            people.Add(new Person()
            {
                FirstName = "Dave", LastName = "Balzer",
                Age= 21, PersonId = 1
            });
            Person p = new Person();
            p.FirstName = "Fred";
            p.LastName = "Flinstone";
            p.Age = 2500;
            p.PersonId = 2;
            people.Add(p);

            foreach (var person in people)
            {
                Console.WriteLine("{0}, {1} is {2} years old.", person.LastName, 
                    person.FirstName, person.Age);
            }
        }

        static void ShowQueue()
        {
            Queue myQueue = new Queue();

            myQueue.Enqueue("Hello");
            myQueue.Enqueue("World");
            myQueue.Enqueue("!");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(myQueue.Dequeue());
            }
        }

        static void ShowStack()
        {
            Stack myStack = new Stack();
            myStack.Push("Hello");
            myStack.Push("World");
            myStack.Push("!");

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine(myStack.Pop());
            }
        }

        static void ShowHashTable()
        {
            Hashtable applicationMap = new Hashtable();

            applicationMap.Add("txt", "notepad.exe");
            applicationMap.Add("bmp", "paint.exe");
            applicationMap.Add("jpg", "paint.exe");
            applicationMap.Add("docx", "word.exe");

            Console.WriteLine("Enter an extension: ");
            string extension = Console.ReadLine();

            Console.WriteLine("We would open extension {0} with {1}", 
                extension, applicationMap[extension]);

            Console.WriteLine("\nEnter a new extension: ");
            extension = Console.ReadLine();

            Console.WriteLine("Enter a new app: ");
            string application = Console.ReadLine();

            if (applicationMap.ContainsKey(extension))
            {
                Console.WriteLine("That key already exists!");
            }
            else
            {
                applicationMap.Add(extension, application);
            }
            
            Console.WriteLine("\nAll Values\n________________________");
            foreach (var key in applicationMap.Keys)
            {
                Console.WriteLine("{0} opens {1}", key, applicationMap[key]);
            }
        }

        static void ShowArrayList()
        {
            ArrayList intList = new ArrayList();

            intList.Add(1);
            intList.Add(5);
            intList.Add(10);
            intList.Add(4);

            int sum = 0;

            foreach (object o in intList)
            {
                sum += (int)o;
            }

            Console.WriteLine("The some of the arraylist is {0}", sum);
            Console.ReadLine();
        }
    }
}
