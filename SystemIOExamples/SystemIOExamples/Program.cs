﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemIOExamples
{
    class Program
    {
        static void Main(string[] args)
        {
           // SimpleSystemIO();
            StreamReaderWriterExample();

            Console.ReadLine();

        }

        static void StreamReaderWriterExample()
        {
            using (StreamWriter writer = File.CreateText("reminders.txt"))
            {
                writer.WriteLine("Don't forget my wife's birthday!!!!");
                writer.WriteLine("Buy kid's school supplies..");
                writer.WriteLine("Watch Netflix");
                for (int i = 0; i < 10; i++)
                {
                    writer.Write(i + " ");
                }

                writer.Write(writer.NewLine);
            }

            Console.WriteLine("Create file and wrote some tasks...");

            using (StreamReader sr = File.OpenText("reminders.txt"))
            {
                string input = null;
                while ((input = sr.ReadLine()) != null)
                {
                    Console.WriteLine(input);
                }
            }
        }

        static void SimpleSystemIO()
        {
            Console.WriteLine("Simple IO with the File Type");
            Console.WriteLine();
            try
            {
                string[] myTasks = {"Fix bathroom sink", "Play No Man's Sky", "Call my wife", "Mow the lawn", "Go Barber"};
                string path = "";
                for (int i = 0; i < 100; i++)
                {
                    path = string.Format(@".\tasks{0}.txt", i);
                    File.WriteAllLines(path, myTasks);
                }

                File.WriteAllLines(@".\tasks.txt", myTasks);

                foreach (var task in File.ReadLines(@".\tasks.txt"))
                {
                    Console.WriteLine("TODO: {0}", task);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
