﻿$(document)
    .ready(function() {
        $('#btnShowAddContact')
            .on('click',
                function () {
                    $('#name').val('');
                    $('#phoneNumber').val('');
                    $('#addContactModal').modal('show');
                });

        $('#btnSaveContact')
            .on('click',
                function() {
                    var contact = {};
                    contact.Name = $('#name').val();
                    contact.PhoneNumber = $('#phoneNumber').val();

                    $.post(uri, contact)
                        .done(function() {
                            loadContacts();
                            $('#addContactModal').modal('hide');
                        })
                        .fail(function(jqXhr, status, err) {
                            alert(status + " - " + err);
                        });
                });
    });