﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml.Linq;
using ContactsWithWebAPI.Models;

namespace ContactsWithWebAPI.Controllers
{
    public class ContactsController : ApiController
    {
        public List<Contact> Get()
        {
            var repo = new FakeContactRepository();
            return repo.GetAll();
        }

        public Contact Get(int id)
        {
            var repo = new FakeContactRepository();
            return repo.GetById(id);
        }

        public HttpResponseMessage Post(Contact contact)
        {
            var repo = new FakeContactRepository();
            repo.Add(contact);
            var response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }



    }
}
