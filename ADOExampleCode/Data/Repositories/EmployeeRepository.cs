﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Config;
using Data.Models;

namespace Data.Repositories
{
    public class EmployeeRepository
    {
        public List<Employee> GetAll()
        {
            List<Employee> employees = new List<Employee>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT e1.EmployeeID, e1.LastName, e1.FirstName, e1.Title, " +
                                  "e1.BirthDate, e1.ReportsTo, (e2.FirstName + ' ' + e2.LastName) as ManagerName " +
                                  "FROM Employees e1 " +
                                  "LEFT JOIN Employees e2 ON e1.ReportsTo = e2.EmployeeID";
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employees.Add(PopulateFromDataReader(dr));
                    }
                }
            }
            return employees;
        }

        public void AddEmployee(Employee e)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "insert into Employees (FirstName, LastName, ReportsTo) " +
                                  "values (@FirstName, @LastName, @ReportsTo)";
                cmd.Connection = cn;

                cmd.Parameters.AddWithValue("@FirstName", e.FristName);
                cmd.Parameters.AddWithValue("@LastName", e.LastName);
                cmd.Parameters.AddWithValue("@ReportsTo", e.ReportsTo);

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public Employee GetEmployeeStoredProc(int id)
        {
            Employee employee = new Employee();
            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "GetEmployeeByID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@EmployeeID", id);

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employee = PopulateFromDataReader(dr);
                    }
                }
            }

            return employee;
        }

        public Employee GetEmployee(int id)
        {
            Employee employee = new Employee();
            using (var cn = new SqlConnection(Settings.ConnectionString))
            {
                var cmd = new SqlCommand();
                cmd.CommandText = "SELECT e1.EmployeeID, e1.LastName, e1.FirstName, e1.Title, " +
                                  "e1.BirthDate, e1.ReportsTo, (e2.FirstName + ' ' + e2.LastName) as ManagerName " +
                                  "FROM Employees e1 " +
                                  "LEFT JOIN Employees e2 ON e1.ReportsTo = e2.EmployeeID " +
                                  "WHERE e1.EmployeeID = @EmployeeID";
                cmd.Connection = cn;
                cmd.Parameters.AddWithValue("@EmployeeID", id);

                cn.Open();

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        employee = PopulateFromDataReader(dr);
                    }
                }
            }

            return employee;
        }

        private Employee PopulateFromDataReader(SqlDataReader dr)
        {
            Employee employee = new Employee();

            employee.EmployeeId = (int)dr["EmployeeID"];
            employee.LastName = dr["LastName"].ToString();
            employee.FristName = dr["FirstName"].ToString();
            employee.Title = dr["Title"].ToString();

            if (dr["BirthDate"] != DBNull.Value)
                employee.DateOfBirth = (DateTime)dr["BirthDate"];

            if (dr["ReportsTo"] != DBNull.Value)
            {
                employee.ReportsTo = (int) dr["ReportsTo"];
                employee.ManagerName = dr["ManagerName"].ToString();
            }

            return employee;
        }
    }
}
