﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using Data.Repositories;

namespace ADOExampleCode
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowAllEmployees();
            //LoadSingleEmployee();
            //LoadSingleEmployeeStoredProc();

            AddNewEmployee();
            Console.ReadLine();
        }

        private static void AddNewEmployee()
        {
            Employee e = new Employee();
            e.FristName = "Dave";
            e.LastName = "Balzer";
            e.ReportsTo = 4;

            EmployeeRepository repo = new EmployeeRepository();
            repo.AddEmployee(e);

            Console.WriteLine("Done");
        }

        private static void LoadSingleEmployeeStoredProc()
        {
            EmployeeRepository repo = new EmployeeRepository();
            Employee e = repo.GetEmployeeStoredProc(4);

            Console.WriteLine($"{e.EmployeeId:D2} {e.LastName}, {e.FristName} {e.Title} {e.DateOfBirth:d} {e.ReportsTo} {e.ManagerName}");
        }

        private static void LoadSingleEmployee()
        {
            EmployeeRepository repo = new EmployeeRepository();
            Employee e = repo.GetEmployee(4999);

            Console.WriteLine($"{e.EmployeeId:D2} {e.LastName}, {e.FristName} {e.Title} {e.DateOfBirth:d} {e.ReportsTo} {e.ManagerName}");
        }

        private static void ShowAllEmployees()
        {
            EmployeeRepository repo = new EmployeeRepository();

            Console.WriteLine("All Employees");
            Console.WriteLine("------------------------");
            foreach (Employee e in repo.GetAll())
            {
                Console.WriteLine($"{e.EmployeeId:D2} {e.LastName}, {e.FristName} {e.Title} {e.DateOfBirth:d} {e.ReportsTo} {e.ManagerName}");
            }

        }
    }
}
