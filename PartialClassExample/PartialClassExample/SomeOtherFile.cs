﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialClassExample
{
    public partial class Person
    {
        public int Age { get; set; }
        public string MiddleName { get; set; }
    }
}
