﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialClassExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();

            p.FirstName = "Dave";
            p.LastName = "Balzer";
            p.Age = 21;
            p.MiddleName = "Awesome";
        }
    }
}
