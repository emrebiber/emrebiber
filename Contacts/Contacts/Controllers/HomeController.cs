﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Contacts.Data;
using Contacts.Models;
namespace Contacts.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repo = new FakeContactRepository();
            var model = repo.GetAll();
            return View(model);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddContact()
        {
            var contact = new Contact();
            contact.Name = Request.Form["Name"];
            contact.PhoneNumber = Request.Form["PhoneNumber"];
            var repo = new FakeContactRepository();
            repo.Add(contact);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var repo = new FakeContactRepository();
            var contact = repo.GetById(id);
            return View(contact);
        }

        [HttpPost]
        public ActionResult EditContact()
        {
            var contact = new Contact();
            contact.ContactId = int.Parse(Request.Form["ContactId"]);
            contact.Name = Request.Form["Name"];
            contact.PhoneNumber = Request.Form["PhoneNumber"];
            var repo = new FakeContactRepository();
            repo.Edit(contact);
            return RedirectToAction("Index");
        }
    }
}