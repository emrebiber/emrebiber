﻿var myApp = angular.module('friendsApp', ['ngRoute']);

myApp.factory('friendsFactory',
    function($http) {
        var webApiProvider = {};

        var url = '/api/Friends/';

        webApiProvider.getFriends = function() {
            return $http.get(url);
        };

        webApiProvider.saveFriend = function(friend) {
            return $http.post(url, friend);
        };

        return webApiProvider;
    });

myApp.config(function($routeProvider) {
    $routeProvider
        .when('/Routes',
        {
            controller: 'FriendsController',
            templateUrl: '/AngularViews/FriendsList.html'
        })
        .when('/AddFriend',
        {
            controller: 'AddFriendController',
            templateUrl: '/AngularViews/AddFriend.html'
        })
        .otherwise({ redirectTo: '/Routes' });
});

myApp.controller('FriendsController',
    function($scope, friendsFactory) {
        friendsFactory.getFriends()
            .success(function(data) {
                $scope.friends = data;
            })
            .error(function(data, status) {
                alert('Oh No!  Something went wrong: ' + status);
            });
    });

myApp.controller('AddFriendController',
    function($scope, $location, friendsFactory) {
        $scope.friend = {};

        $scope.save = function() {
            friendsFactory.saveFriend($scope.friend)
                .success(function() {
                    $location.path('/Routes');
                })
                .error(function(data, status) {
                    alert('Another Error: ' + status);
                });
        }
    });