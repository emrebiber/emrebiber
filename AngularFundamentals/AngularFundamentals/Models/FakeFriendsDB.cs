﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularFundamentals.Models
{
    public class FakeFriendsDB
    {
        private static List<Friend> _friends = new List<Friend>();

        static FakeFriendsDB()
        {
            _friends.AddRange(new []
            {
                new Friend() {Name="Jennie", Phone="867-5309", Age=35},
                new Friend() {Name="Mary", Phone="111-2222", Age=30},
                new Friend() {Name="Mike", Phone="333-4444", Age=20},
                new Friend() {Name="Jim", Phone="555-6666", Age=24}
            });
        }

        public List<Friend> GetAll()
        {
            return _friends;
        }

        public void Add(Friend friend)
        {
            _friends.Add(friend);
        }
    }
}