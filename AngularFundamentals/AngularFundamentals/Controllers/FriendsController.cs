﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularFundamentals.Models;

namespace AngularFundamentals.Controllers
{
    public class FriendsController : ApiController
    {
        public List<Friend> Get()
        {
            var db = new FakeFriendsDB();
            return db.GetAll();
        }

        public HttpResponseMessage Post(Friend friend)
        {
            var db = new FakeFriendsDB();
            db.Add(friend);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
