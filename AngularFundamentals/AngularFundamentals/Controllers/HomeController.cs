﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularFundamentals.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Directives()
        {
            return View();
        }

        public ActionResult Repeat()
        {
            return View();
        }

        public ActionResult Filters()
        {
            return View();
        }

        public ActionResult Routes()
        {
            return View();
        }
    }
}