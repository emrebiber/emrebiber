﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Factorizor
    {
        static void Main(string[] args)
        {
            int number;
            int sum = 1;
            string input;
            bool prime = true;

            Console.Write("What number would you like to factor? ");
            input = Console.ReadLine(); ;

            number = int.Parse(input);
            Console.WriteLine("\nThe factors of {0} are:\n\n1\n", input);

        Restart:
            for (int i = 2; i <= number; i++)
            {
                if (number % i == 0 && number !=i)
                {
                    prime = false;
                    Console.WriteLine("{0}\n", i);
                    sum += i;
                    number /= i;
                    //i = 2;
                    goto Restart;
                }
                else if (number == i)
                {
                    Console.WriteLine("{0}\n", i);
                }
            }

            if (sum == number)
            {
                Console.WriteLine("{0} is a perfect number", input);
            }
            else
            {
                Console.WriteLine("{0} is not a perfect number", input);
            }
            if (prime)
            {
                Console.WriteLine("{0} is a prime number", input);
            }
            else
            {
                Console.WriteLine("{0} is not a prime number", input);
            }
            Console.Read();
        }
    }
}
