﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQSamples
{
    class StudentCourses
    {
        public int StudentID { get; set; }
        public string CourseName { get; set; }
    }
}
