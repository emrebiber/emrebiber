﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LINQSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            //ShowAnonymousTypes();
            //ShowLinqJoins();
            ShowLinqGrouping();

            Console.ReadLine();
        }

        static void ShowLinqGrouping()
        {
            List<Student> students = new List<Student>()
            {
                new Student() { LastName = "Sandler", Major = "Computer Science"},
                new Student() { LastName = "Martin", Major = "History"},
                new Student() { LastName = "Murphy", Major = "English"},
                new Student() { LastName = "Pacino", Major = "Computer Science"},
                new Student() { LastName = "Candy", Major = "History"}
            };

            var result = from s in students
                group s by s.Major;

            foreach (var group in result)
            {
                Console.WriteLine(group.Key);
                foreach (var student in group)
                {
                    Console.WriteLine("\t{0}", student.LastName);
                }
            }




        }

        static void ShowLinqJoins()
        {
            List<Student> students = new List<Student>()
            {
                new Student() {LastName = "Balzer", StudentID = 1},
                new Student() {LastName = "Toner", StudentID = 2}
            };

            List<StudentCourses> courses = new List<StudentCourses>()
            {
                new StudentCourses() {StudentID = 1, CourseName = "C# Fundamentals"},
                new StudentCourses() {StudentID = 1, CourseName = "Asp.Net Fundamentals"},
                new StudentCourses() {StudentID = 2 ,CourseName = "Java Fundamentals"},
                new StudentCourses() {StudentID = 2, CourseName = "CSS and HTML"},
                new StudentCourses() {StudentID = 1, CourseName = "Sql Server Mastery"}
            };

            var results = from student in students
                join course in courses
                    on student.StudentID equals course.StudentID
                select new {student.LastName, course.CourseName};

            foreach (var r in results)
            {
                Console.WriteLine("{0, -10} -  {1}", r.LastName, r.CourseName);
            }
        }

        static void ShowAnonymousTypes()
        {
            Console.WriteLine("************** Anonymous Types *************");
            List<Person> people = new List<Person>()
            {
                new Person() {FirstName = "Homer", LastName = "Simpson", Age = 43, Gender = 'M'},
                new Person() {FirstName = "Marge", LastName = "Simpson", Age = 40, Gender = 'F'},
                new Person() {FirstName = "Lisa", LastName = "Simpson", Age = 12, Gender = 'F'},
                new Person() {FirstName = "Bart", LastName = "Simpson", Age = 9, Gender = 'M'},
                new Person() {FirstName = "Maggie", LastName = "Simpson", Age = 1, Gender = 'F'}
            };

            var ladies = from p in people
                where p.Gender == 'F'
                select new {Name = p.LastName + ", " + p.FirstName, p.Age};
                     //select p;

                     Console.WriteLine("The type of ladies is {0}", ladies.ToString());
            foreach (var l in ladies)
            {
                Console.WriteLine($"{l.Name} - {l.Age}");
            }

        }
    }
}
