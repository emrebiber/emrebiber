﻿using System;
using DataLayer;
using HelloWorld.SomeOtherDirectory;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // This is a single line comment
            DavesClass.WriteName();

            Console.WriteLine(DatabaseMethods.MyMethod());

            /*
             This is a multi line comment
             I can keep writing
             this is fun!!!  Woo Hoo
             */
            Console.ReadLine();
        }
    }
}
