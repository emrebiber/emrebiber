﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld.SomeOtherDirectory
{
    static class DavesClass
    {
        /// <summary>
        /// This is a special method that writes Daves Name
        /// </summary>
        public static void WriteName()
        {
            Console.WriteLine("Dave!");
        }
    }
}
