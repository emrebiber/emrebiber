﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentManagementSystem.Helpers;
using StudentManagementSystem.Workflows;

namespace StudentManagementSystem
{
    public class MainMenu
    {
        public static void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("SCG Flooring Program");
            Console.WriteLine(ConsoleIO._seperatorBar);
            Console.WriteLine("1. Display Orders");
            Console.WriteLine("2. Add Order");
            Console.WriteLine("3. Edit Order");
            Console.WriteLine("4. Remove Order");
            Console.WriteLine();
            Console.WriteLine("Q - Quit");
            Console.WriteLine();
            Console.WriteLine(ConsoleIO._seperatorBar);
            Console.WriteLine();
        }



        private static bool ProcessChoice()
        {
            string input = Console.ReadLine();

            switch (input)
            {
                case "1":
                    ListStudentWorkflow listWorkflow = new ListStudentWorkflow();
                    listWorkflow.Execute();
                    break;
                case "2":
                   AddStudentWorkflow addWorkflow = new AddStudentWorkflow();
                    addWorkflow.Execute();
                    break;
                case "3":
                   RemoveStudentWorkflow removeWorkflow = new RemoveStudentWorkflow();
                    removeWorkflow.Execute();
                    break;
                case "4":
                   EditStudentWorkflow editWorkflow = new EditStudentWorkflow();
                    editWorkflow.Execute();
                    break;
                case "Q":
                    return false;
                default:
                    Console.WriteLine("That is not a valid choice. Press any key to continue");
                    Console.ReadKey();
                    break;
            }

            return true;
        }

        public static void Show()
        {
            bool continueRunning = true;

            while (continueRunning)
            {
               DisplayMenu();
               continueRunning = ProcessChoice();
            }
          


        }
    }
}
