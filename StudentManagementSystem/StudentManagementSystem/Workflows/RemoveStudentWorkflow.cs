﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentManagementSystem.Data;
using StudentManagementSystem.Helpers;
using StudentManagementSystem.Models;

namespace StudentManagementSystem.Workflows
{
    public class RemoveStudentWorkflow
    {
        public void Execute()
        {
            Console.Clear();
            Console.WriteLine("Remove Student");
            Console.Write(ConsoleIO._seperatorBar);
            

            StudentRepository repo = new StudentRepository(Settings.FilePath);
            List<Student> students = repo.List();

            ConsoleIO.PrintPickListOfStudents(students);
            Console.WriteLine();
            Console.WriteLine(ConsoleIO._seperatorBar);

            int index = ConsoleIO.GetStudentIndexFromUser("Which student would you like to delete? ", students.Count());
            index--;

            string answer =
                ConsoleIO.GetYesNoAnswerFromUser(
                    $"Are you sure you want to remove {students[index].FirstName} {students[index].LastName} ");

            if (answer == "Y")
            {
                repo.Delete(index);
                Console.WriteLine("Student Removed!");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Removed cancelled.");
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }
    }
}