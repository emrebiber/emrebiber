﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("************** Fun With Strings *****************");
            //BasicStringFunctionality();
            //StringConcatenation();
            //EscapeCharacters();
            //VerbatimStrings();
            StringBuilderExample();

            Console.ReadLine();
        }

        static void StringBuilderExample()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Computer Languages\n");
            sb.Append("C#\n");
            sb.Append("Java\n");
            sb.Append("Ruby\n");
            sb.Append("Python\n");

            Console.WriteLine(sb.ToString());
        }

        static void VerbatimStrings()
        {
            string myLongString = @"This is a very 
                        very 
                                very
                                        long string!";
            Console.WriteLine(myLongString);
            Console.WriteLine(@"C:\MyApp\bin\Debug");
            Console.WriteLine(@"I said""this is really cool""");
        }

        static void EscapeCharacters()
        {
            //Console.WriteLine("My {0} value is {0}", "Some Value");
            Console.WriteLine("C:\\MyApp\\bin\\debug");
            Console.WriteLine("Model\tColor\tSpeed\tName\a");
            Console.WriteLine("Almost\n\n\n\nFinished!");
        }

        static void StringConcatenation()
        {
            string s1 = "This is the ";
            string s2 = "first day of the cohort!";
            //Console.WriteLine("{0}{1}", s1, s2);
            string s3 = s1 + s2;
            Console.WriteLine(s3);
            string s4 = string.Concat(s1, s2);
            Console.WriteLine(s4);
             s1 = s1 + s2;
            Console.WriteLine(s1);
        }

        static void BasicStringFunctionality()
        {
            string firstName = "Freddy";
            Console.WriteLine("Value of firstName: {0}", firstName);
            Console.WriteLine("firstName has {0} characters.", firstName.Length);
            Console.WriteLine("firstName in uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("firstName in lowercase: {0}", firstName.ToLower());
            Console.WriteLine("firstName contains the letter y?: {0}", firstName.Contains("y"));
            Console.WriteLine("firstName after replace: {0}", firstName.Replace("dy", ""));
        }
    }
}
