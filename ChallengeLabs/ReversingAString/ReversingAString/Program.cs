﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReversingAString
{
    class Program
    {
        static void Main(string[] args)
        {
            //ReverseStringWithArrayReverse();
            ReverseStringWithForLoop();

            Console.ReadLine();
        }

        static void ReverseStringWithArrayReverse()
        {
            Console.Write("Enter string to reverse: ");
            string userInput = Console.ReadLine();

            char[] arr = userInput.ToCharArray();
            Array.Reverse(arr);
            string reversedString = new string(arr);


            Console.Write("Your reversed string is: {0} ", reversedString);
        }

        static void ReverseStringWithForLoop()
        {
            Console.Write("Enter string to reverse: ");
            string userInput = Console.ReadLine();
            string reversedString = "";

            for (int i = userInput.Length - 1; i >= 0; i--)
            {
                reversedString += userInput[i];
            }

            Console.Write("Reverse string is: {0}", reversedString);

        }
    }
}
