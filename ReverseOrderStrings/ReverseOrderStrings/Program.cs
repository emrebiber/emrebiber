﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReverseOrderStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter string to reverse: ");
            string userInput = Console.ReadLine();

            char[] arr = userInput.ToCharArray();
            Array.Reverse(arr);
            string reversedString = new string(arr);
            

            Console.Write("Your reversed string is: {0} ", reversedString);
            Console.ReadLine();
        }
    }
}
