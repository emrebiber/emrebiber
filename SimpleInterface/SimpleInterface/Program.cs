﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            GermanShephard bigDog = new GermanShephard();
            WeinerDoodle smallDog = new WeinerDoodle();

            GiveTreat(bigDog);
            GiveTreat(smallDog);

            TakeOutside(bigDog);
            TakeOutside(smallDog);

            Console.ReadLine();


        }

        static void GiveTreat(IDog dog)
        {
            dog.Bark();
        }


        static void TakeOutside(IDog dog)
        {
           Console.WriteLine(dog.GoForAWalk()); 
        }
        
    }
}
