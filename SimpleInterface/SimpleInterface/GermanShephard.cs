﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterface
{
    public class GermanShephard : IDog
    {
        public void Bark()
        {
            Console.WriteLine("WOOF!");
        }

        public string GoForAWalk()
        {
            return "Go for a long walk!";
        }
    }
}
