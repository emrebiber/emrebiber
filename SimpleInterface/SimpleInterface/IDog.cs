﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleInterface
{
    public interface IDog
    {
        void Bark();
        string GoForAWalk();
    }
}
