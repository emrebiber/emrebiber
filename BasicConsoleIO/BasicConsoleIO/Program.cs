﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicConsoleIO
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**************** Basic I/O  ******************");
            //GetUserData();
            //DemonstrateAlignment();
            FormatNumericalData();

            Console.ReadLine();
        }

        static void FormatNumericalData()
        {
            Console.WriteLine("The value of 99999 in various formats:");
            Console.WriteLine("c format: {0:c}", 99999);
            Console.WriteLine("d9 format: {0:d9}", 99999);
            Console.WriteLine("f3 format: {0:f3}", 99999);
        }

        static void DemonstrateAlignment()
        {
            //Left Alignment
            string lineOutputFormat = "{0, 15} {1} {2}";
            Console.WriteLine(lineOutputFormat, "John Doe", "OH", "44113");
            Console.WriteLine(lineOutputFormat, "Jane Doe", "NY", "12065");
            Console.WriteLine(lineOutputFormat, "Cheech Marin", "CA", "90210");
        }

        static void GetUserData()
        {
            // Get Name and Age
            Console.WriteLine("Please Enter your name: ");
            string userName = Console.ReadLine();

            Console.WriteLine("Please enter your age: ");
            string userAge = Console.ReadLine();

            ConsoleColor prevColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Blue;

            Console.WriteLine("Hello {0}! You are {1} years old. Thanks {0}", userName, userAge);
            Console.WriteLine($"Hello {userName}!  You are {userAge} years old!");

            Console.ForegroundColor = prevColor;

            Console.WriteLine("Done");
        }
    }
}
