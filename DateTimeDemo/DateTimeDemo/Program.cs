﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeDemo
{
    class Program
    {
        static void Main(string[] args)
        {
           // DifferenceOfDates();

            DayOfWeekAndYear();

            Console.ReadLine();
        }

        static void CreateDateTimeObject()
        {
            //current date
            DateTime currentDateTime = new DateTime();
            DateTime utcNow = DateTime.UtcNow;
            DateTime onlyDate = DateTime.Today;

            DateTime aDateTime = new DateTime(); // default gives January 1st, 0001
            DateTime specificDateTime = new DateTime(2008, 6, 15, 21, 15, 7);

            DateTime userDate;
            while (true)
            {
                Console.Write("Enter a Date: ");
                string input = Console.ReadLine();
               
                if (DateTime.TryParse(input,out userDate))
                {
                    break;
                }

                Console.WriteLine("This is not a valid date");
            }
          




        }

        static void ManipulatingDateValues()
        {
            DateTime now  = DateTime.Now;

            DateTime dueDate = now.AddDays(30).AddHours(5); // adds 30 days and 5 hour to now.

            // adds 30 days and 5 hour to now.
            TimeSpan ts = new TimeSpan(30, 5, 0, 0);
            dueDate = dueDate.Add(ts);


            DateTime pastDateTime = now.AddDays(-5);
        }

        static void DifferenceOfDates()
        {
            // will always select the next new years days
            DateTime newYears = new DateTime(DateTime.Today.Year +1,1,1);


            DateTime now = DateTime.Today;

            TimeSpan timeUntilNewYears = newYears.Subtract(now);

            Console.WriteLine("It is {0} days until New Years", timeUntilNewYears.Days);






        }

        static void DayOfWeekAndYear()
        {
            DateTime nextIndepedence = new DateTime(DateTime.Today.Year + 1, 7, 4);

            switch (nextIndepedence.DayOfWeek)
            {
                    case DayOfWeek.Saturday:
                    case DayOfWeek.Sunday:  
                        Console.WriteLine("{0:d} is on the weekend", nextIndepedence);
                        break;
                    default:
                        Console.WriteLine("{0:d} is on a weekday", nextIndepedence);
                        break;

            }


        }
    }



}

