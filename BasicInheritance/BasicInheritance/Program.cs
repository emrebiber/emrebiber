﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*** Basic Inheritance ****");
            Car myCar = new Car();
            MiniVan van = new MiniVan(90, "Hello World!");

            Console.WriteLine("myCar has max speed of {0}", myCar._maxSpeed);
            myCar.Speed = 40;
            Console.WriteLine("myCar current speed is {0}", myCar.Speed);
            myCar.Speed = 80;
            Console.WriteLine("myCar current speed is {0}", myCar.Speed);

            Console.WriteLine("van has max speed of {0}", van._maxSpeed);
            van.Speed = 40;
            Console.WriteLine("van current speed is {0}", van.Speed);
            van.Speed = 80;
            Console.WriteLine("van current speed is {0}", van.Speed);

            Console.ReadLine();
        }
    }
}
