﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    public class MiniVan : Car
    {

        public MiniVan(int maxSpeed, string somevalue) : base(maxSpeed)
        {
            Console.WriteLine(somevalue);
        }
    }
}
