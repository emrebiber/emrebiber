﻿namespace EnumTrafficLightDemo
{
    public enum TrafficLightColor
    {
        Green,
        Yellow,
        Red
    }
}
